# Crossbow - the minimalist feed aggregator

Crossbow is a simple but powerful RSS/Atom feed
aggregator

 - Designed to be invoked from cron(8) to fetch updates
   periodically.

 - It can easily convert feeds to email.

 - Entries can be handled with an external program,
   allowing for example, to automatically download
   and parse content, without user interaction.

 - Minimal set of dependencies.

 - Documented thoroughly.


## Resources

 - Home page: <https://dacav.org/projects/crossbow/>

 - Official repo: <https://dacav.org/git/crossbow.git>

 - Mirror repo (gitlab): <https://gitlab.com/dacav/crossbow>

 - Latest release: <https://dacav.org/projects/crossbow/releases/latest/>

 - Releases feed: <https://dacav.org/projects/crossbow/feeds/releases.xml>


## How to install:

 1. Configure

        cmake [opts] -S . -B ./build

    Supported build options:

    - `-D STRICT=ON`: enable strict build (`-Werror`, etc...)
    - `-D UT_SAN=ON`: enable sanitizers for unit tests.
    - `-D LIB_SAN=ON`: enable sanitizers for the library

 2. Build

        cmake --build ./build

 3. Run unit tests

        ctest --test-dir ./build

 4. Install:

        cmake --install ./build

Please refer to the CMake documentation to learn about additional options.

### Pitfalls

The CMake configuration relies on versioning information which is only available
to a cloned Git repository, or when building from a dist tarball (generated by
the `contrib/dist-tarball` script.

If versioning information is not available, the package configuration step will
fail with the following error:

> Could not determine version, see project README for more info.

This is the case if the tarball was built with `git-archive(1)`, or
downloaded from a release page on GitLab.
Please download a release tarball from the release page instead.
