# TODO list

## Enable memory sanitizers under OpenBSD

See also http://man.openbsd.org/malloc#S

## Consider dropping (or handling text encoding with) `text_short`

src/text.h
Text is modified to obtain pretty printing.
In case of multi-byte characters, the `text_short` is going to be butchered.
