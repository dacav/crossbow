Contributing to Crossbow
========================

Comments, ideas and patches are welcome:

- Send suggestions and patches (see git-send-email(1))
  via email:
  dacav at fastmail.com
- Open a merge request on gitlab:
  <https://gitlab.com/dacav/crossbow/-/merge_requests>
- Open issues on gitlab:
  <https://gitlab.com/dacav/crossbow/-/issues>
