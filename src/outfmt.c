#include <assert.h>
#include <err.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/wait.h>
#include <unistd.h>

#include "outfmt.h"
#include "outfmt_parser.h"
#include "text.h"
#include "util.h"

enum {
    /* TODO: these assignments are arbitrary, and they
     * should be reviewed.
     *
     * Clever way to print struct size:
     *  char (*__kaboom)[sizeof( struct foo )] = 1;
     * (credits: https://stackoverflow.com/a/35261673)
     *
     * Current sizes:
     *  sizeof struct subproc => 3088
     *  sizeof struct print => 2056    <- can be more.
     *  sizeof struct ofmt => 3192
     */
    max_iov_len = 3 * 32,
    max_pholders = 32,
    max_spec_len = SHRT_MAX,
    max_segments = 3 * 32,
    max_argv_len = max_segments + 1, /* upper bound */
    max_eval_buflen = 1024,
};

struct subproc_segment {
    enum ofp_atom atom;
    union {
        ofmt_resolver_callback resolver;
        struct {
            short begin, len;
        } buf_idx;
    };
    short arg_idx;
    bool arg_boundary : 1;
    bool arg_atomic : 1;
};

struct subproc {
    struct subproc_segment segments[max_segments];
    const char *argv[max_argv_len];
    short segwrite;
    bool pipe_enabled;
};

struct print_pholder {
    ofmt_resolver_callback resolver;
    short iovidx;
};

struct print {
    struct iovec iov[max_iov_len];
    struct print_pholder pholders[max_pholders];
    int iovwrite;
    int phwrite;
};

struct ofmt {
    enum ofmt_mode mode;
    void *opaque;
    void (*opaque_free)(void *);
    struct radix *resolvers_map;
    ofmt_resolver_callback pipe_resolver;

    struct ofp_setup ofp_setup;
    struct ofmt_err error;

    char *buffer;
    int buflen;
    int bufwrite;

    union {
        struct subproc subproc;
        struct print print;
    };
};

static int fail(struct ofmt *ofmt, enum ofmt_fail why)
{
    assert(why != ofmt_fail_bad_state);
    ofmt->error.reason = why;
    if (why == ofmt_fail_system)
        ofmt->error.errno_val = errno;
    return -1;
}

static int get_resolver(struct ofmt *ofmt,
                        const struct ofp_token *tok,
                        ofmt_resolver_callback *res)
{
    if (tok->atom != ofp_atom_pholder)
        return fail(ofmt, ofmt_fail_bad_state);

    if (tok->len < 1 || tok->begin[0] != '%') /* broken parser? */
        return fail(ofmt, ofmt_fail_bad_state);

    if (tok->len < 2)
        return fail(ofmt, ofmt_fail_invalid_pholder);

    int n;
    void *item;

    /* The provided placeholder token starts with '%', but the resolver will
     * not expect a leading '%'.  For this reason we pass it with an offset,
     * and we sum +1 to the resulting length. */

    n = radix_lookup(ofmt->resolvers_map, tok->begin + 1, tok->len - 1, &item);
    if (n == -1)
        return fail(ofmt, ofmt_fail_undefined_resolver);

    *res = item;
    return n + 1;
}

static int print_save_pholder(struct ofmt *ofmt, const struct ofp_token *tok)
{
    struct print *print;
    ofmt_resolver_callback resolver;
    int used;

    if (tok->len < 1) /* broken parser? */
        return fail(ofmt, ofmt_fail_bad_state);

    print = &ofmt->print;

    if (print->iovwrite >= max_iov_len)
        return fail(ofmt, ofmt_fail_too_many_args);

    if (print->phwrite >= max_pholders)
        return fail(ofmt, ofmt_fail_too_many_pholders);

    used = get_resolver(ofmt, tok, &resolver);
    if (used != -1) {
        print->pholders[print->phwrite++] = (struct print_pholder){
            .resolver = resolver,
            .iovidx = print->iovwrite,
        };
        print->iov[print->iovwrite++] = (struct iovec){};
    }

    return used;
}

static size_t copy_escaped(char *dst, const char *src, size_t len)
{
    bool escaped = false;
    int j = 0;

    for (int i = 0; i < len; ++i) {
        char c = src[i];

        if (escaped) {
            escaped = false;
            switch (c) {
            case 'n':
                c = '\n';
                break;
            case 'r':
                c = '\r';
                break;
            case 't':
                c = '\t';
                break;
            case ':':
                continue; /* \: is a "zero-width break". */
            }
        } else if (c == '\\') {
            escaped = true;
            continue;
        }

        dst[j++] = c;
    }

    return j;
}

static int store(struct ofmt *ofmt, const char *begin, size_t len)
{
    if (ofmt->buflen - ofmt->bufwrite < len) {
        /* it should be enough by construction, if it isn't it means the
         * algorithm is buggy. */
        return fail(ofmt, ofmt_fail_bad_state);
    }

    size_t copied = copy_escaped(&ofmt->buffer[ofmt->bufwrite], begin, len);
    ofmt->bufwrite += copied;

    return 0;
}

static void record_fail(void *o, enum ofp_fail reason)
{
    struct ofmt *ofmt = o;

    /* we returned -1 during *_emit, so we already failed */
    if (reason == ofp_fail_emitting)
        return;

    fail(ofmt, ofmt_fail_parse);
    ofmt->error.ofp_reason = reason;
}

static inline struct subproc_segment *next_segment(struct subproc *subproc)
{
    if (subproc->segwrite < max_segments)
        return &subproc->segments[subproc->segwrite++];
    return NULL;
}

static inline struct subproc_segment *cur_segment(struct subproc *subproc)
{
    if (subproc->segwrite == 0)
        return NULL;
    return &subproc->segments[subproc->segwrite - 1];
}

static int subproc_merge_verbatim(struct ofmt *ofmt,
                                  struct subproc_segment *s,
                                  const struct ofp_token *tok)
{
    int w;

    w = ofmt->bufwrite;
    if (store(ofmt, tok->begin, tok->len) == -1)
        return -1;

    s->buf_idx.len += ofmt->bufwrite - w;
    return 0;
}

static int subproc_save_verbatim(struct ofmt *ofmt, const struct ofp_token *tok)
{
    struct subproc_segment *s;
    int w;

    s = cur_segment(&ofmt->subproc);
    if (s)
        switch (s->atom) {
        case ofp_atom_whitespace: /* never stored */
            return fail(ofmt, ofmt_fail_bad_state);
        case ofp_atom_verbatim:
            if (!s->arg_boundary)
                return subproc_merge_verbatim(ofmt, s, tok);
        case ofp_atom_pholder:
            break;
        }

    s = next_segment(&ofmt->subproc);
    if (!s)
        return fail(ofmt, ofmt_fail_too_many_args);

    w = ofmt->bufwrite;
    if (store(ofmt, tok->begin, tok->len) == -1) {
        ofmt->subproc.segwrite--; /* give up claimed segment */
        return -1;
    }

    *s = (struct subproc_segment){
        .atom = ofp_atom_verbatim,
        .buf_idx.begin = w,
        .buf_idx.len = ofmt->bufwrite - w,
    };
    return 0;
}

static int subproc_save_pholder(struct ofmt *ofmt, const struct ofp_token *tok)
{
    struct subproc_segment *s;
    ofmt_resolver_callback resolver;
    int used;

    if (tok->len < 1) /* broken parser? */
        return fail(ofmt, ofmt_fail_bad_state);

    used = get_resolver(ofmt, tok, &resolver);
    if (used == -1)
        return -1;

    s = next_segment(&ofmt->subproc);
    if (!s)
        return fail(ofmt, ofmt_fail_too_many_args);

    *s = (struct subproc_segment){
        .atom = ofp_atom_pholder,
        .resolver = resolver,
    };
    return used;
}

static int subproc_save_whitespace(struct ofmt *ofmt)
{
    struct subproc_segment *s;

    s = cur_segment(&ofmt->subproc);
    if (!s)
        return 0; /* leading spaces are ignored */

    if (s->arg_boundary)
        return 0; /* it was already marked */

    switch (s->atom) {
    case ofp_atom_whitespace: /* never stored */
        return fail(ofmt, ofmt_fail_bad_state);
    case ofp_atom_verbatim:
        if (store(ofmt, &(char){0}, 1) == -1)
            return -1;
        /* fallthrough */
    case ofp_atom_pholder:
        s->arg_boundary = true;
        break;
    }

    return 0;
}

static int subproc_emit(void *o, const struct ofp_token *tok)
{
    struct ofmt *ofmt = o;

    switch (tok->atom) {
    case ofp_atom_pholder:
        return subproc_save_pholder(ofmt, tok);

    case ofp_atom_verbatim:
        return subproc_save_verbatim(ofmt, tok);

    case ofp_atom_whitespace:
        return subproc_save_whitespace(ofmt);
    }

    return 0;
}

static int print_save_verbatim(struct ofmt *ofmt, const struct ofp_token *tok)
{
    int start;
    struct print *print;

    print = &ofmt->print;
    if (print->iovwrite >= max_iov_len)
        return fail(ofmt, ofmt_fail_too_many_args);

    start = ofmt->bufwrite;
    if (store(ofmt, tok->begin, tok->len) == -1)
        return -1;

    /* TODO: check if optimization is feasible: two contiguous pointers
     * might be merged. */
    print->iov[print->iovwrite++] = (struct iovec){
        .iov_base = &ofmt->buffer[start],
        .iov_len = ofmt->bufwrite - start,
    };

    return 0;
}

static int subproc_post_scan(struct ofmt *ofmt)
{
    struct subproc *subproc;
    int arg_idx;
    bool new_arg;

    /* always pretend like a final whitespace was entered.  This will mark
     * the last segment as arg_boundary, and add trailing nul character if
     * needed. */
    if (subproc_save_whitespace(ofmt) == -1)
        return -1;

    subproc = &ofmt->subproc;
    arg_idx = 0;
    new_arg = true;
    for (int i = 0; i < subproc->segwrite; ++i) {
        struct subproc_segment *s;

        s = &subproc->segments[i];
        s->arg_idx = arg_idx;

        if (new_arg) {
            if (s->atom == ofp_atom_verbatim && s->arg_boundary) {
                subproc->argv[arg_idx] = ofmt->buffer + s->buf_idx.begin;
                s->arg_atomic = true;
            }
        }

        new_arg = s->arg_boundary;
        if (new_arg)
            arg_idx++;
    }
    subproc->argv[arg_idx] = NULL;

    return 0;
}

static int print_emit(void *o, const struct ofp_token *tok)
{
    struct ofmt *ofmt = o;

    switch (tok->atom) {
    case ofp_atom_pholder:
        return print_save_pholder(ofmt, tok);

    case ofp_atom_verbatim:
    case ofp_atom_whitespace:
        return print_save_verbatim(ofmt, tok);
    }

    return 0;
}

static int ensure_buffer(struct ofmt *ofmt, size_t speclen)
{
    size_t needed;
    char *new_buffer;

    /* ofmt_mode_exec:
     *  Fill up the buffer with nul-terminated chunks which will populate
     *  the execve(2) array.  Since each chunk is terminated by a space
     *  character we use (speclen + 1) characters: the +1 accounts for the
     *  corner case in which we have only one verbatim string which must be
     *  nul terminated.
     *
     * ofmt_mode_print:
     *  No need to nul-terminate strings, so (speclen) bytes will be
     *  definitely enough.
     *
     * Using (speclen + 1) always covers all needs.
     */
    needed = speclen + 1;

    if (ofmt->buflen >= needed)
        return 0;

    if (needed > max_spec_len)
        return fail(ofmt, ofmt_fail_too_long_spec);

    new_buffer = realloc(ofmt->buffer, needed);
    if (!new_buffer)
        return fail(ofmt, ofmt_fail_system);

    ofmt->buffer = new_buffer;
    ofmt->buflen = needed;
    return 0;
}

static int print_eval(struct ofmt *ofmt,
                      const struct ofmt_evaluate_params *params)
{
    struct print *print = &ofmt->print;

    for (int i = 0; i < print->phwrite; ++i) {
        const struct print_pholder *ph = &print->pholders[i];
        const char *value = ph->resolver(params->item, ofmt->opaque);

        print->iov[ph->iovidx] = (struct iovec){
            .iov_base = (void *)value,
            .iov_len = value ? strlen(value) : 0,
        };
    }

    int fd = STDOUT_FILENO;
    if (params->opt_print_fd)
        fd = *(params->opt_print_fd);

    if (writev(fd, print->iov, print->iovwrite) == -1)
        return fail(ofmt, ofmt_fail_system);

    return 0;
}

static int dry_run(const struct launcher_run *lch)
{
    char buffer[60];

    warnx("dry run, would execute: %s",
          text_join_argv(buffer, sizeof(buffer), lch->argv));

    if (lch->chdir_to)
        warnx(" chdir: %s", lch->chdir_to);

    if (lch->input.bytes)
        warnx(" pipe in: %s",
              text_short(
                  buffer, sizeof(buffer), lch->input.bytes, lch->input.len));

    return 0; /* of course */
}

static int launch(struct ofmt *ofmt, const struct launcher_run *lch)
{
    int status;

    if (launcher_execvp(lch, &status) != 0)
        return fail(ofmt, ofmt_fail_system);

    if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
        return 0;

    fail(ofmt, ofmt_fail_subproc);
    ofmt->error.subproc.argv0 = lch->argv[0];
    ofmt->error.subproc.exit_status = status;
    return -1;
}

static int subproc_eval(struct ofmt *ofmt,
                        const struct ofmt_evaluate_params *params)
{
    struct subproc *subproc = &ofmt->subproc;
    char const **argv = subproc->argv;

    char evaluation_buffer[max_eval_buflen];
    int w = 0;
    int current_arg = -1;

    for (int i = 0; i < subproc->segwrite; ++i) {
        struct subproc_segment *s;
        size_t value_len;
        const char *value = NULL;

        s = &subproc->segments[i];

        if (s->arg_atomic)
            continue; /* subproc->argv[s->arg_idx] points at verbatim */

        if (current_arg != s->arg_idx) {
            current_arg = s->arg_idx;
            argv[current_arg] = evaluation_buffer + w;
        }

        switch (s->atom) {
        case ofp_atom_pholder:
            value = s->resolver(params->item, ofmt->opaque);
            value_len = value ? strlen(value) : 0;
            break;

        case ofp_atom_verbatim:
            value = ofmt->buffer + s->buf_idx.begin;
            value_len = s->buf_idx.len;
            break;

        case ofp_atom_whitespace:
            return fail(ofmt, ofmt_fail_bad_state);

        default:
            panic_info("invalid atom %d", s->atom);
        }

        if (value_len + !!s->arg_boundary > sizeof evaluation_buffer - w)
            return fail(ofmt, ofmt_fail_too_long_evaluation);
        memcpy(evaluation_buffer + w, value, value_len);
        w += value_len;
        if (s->arg_boundary)
            evaluation_buffer[w++] = '\0';
    }

    const char *input_stdin = NULL;
    if (subproc->pipe_enabled) {
        if (!ofmt->pipe_resolver)
            panic(); // unregistered pipe resolver

        input_stdin = ofmt->pipe_resolver(params->item, ofmt->opaque);
    }

    const struct launcher_run run = {
        .argv = argv,
        .chdir_to = params->opt_subproc_chdir,
        .input.bytes = (void *)input_stdin,
        .input.len = input_stdin ? strlen(input_stdin) : 0,
    };

    return params->dry_run ? dry_run(&run) : launch(ofmt, &run);
}

struct ofmt *ofmt_new(void)
{
    struct ofmt *ofmt;
    struct radix *resolvers_map;

    ofmt = malloc(sizeof(struct ofmt));
    if (!ofmt) {
        warn("malloc");
        return NULL;
    }

    resolvers_map = radix_new();
    if (!resolvers_map) {
        free(ofmt);
        return NULL;
    }

    *ofmt = (struct ofmt){
        .opaque_free = free,
        .resolvers_map = resolvers_map,
        .ofp_setup.opaque = ofmt,
        .ofp_setup.fail = record_fail,
    };

    return ofmt;
}

static void free_opaque(struct ofmt *ofmt)
{
    if (ofmt->opaque && ofmt->opaque_free)
        ofmt->opaque_free(ofmt->opaque);
}

void ofmt_set_opaque(struct ofmt *ofmt,
                     void *opaque,
                     void (*opaque_free)(void *))
{
    free_opaque(ofmt);
    ofmt->opaque = opaque;
    ofmt->opaque_free = opaque_free;
}

void *ofmt_get_opaque(const struct ofmt *ofmt)
{
    return ofmt->opaque;
}

int ofmt_set_resolver(struct ofmt *ofmt,
                      const char *key,
                      ofmt_resolver_callback resolver)
{
    if (radix_register(ofmt->resolvers_map, key, resolver) == -1)
        return fail(ofmt,
                    errno == EINVAL ? ofmt_fail_bad_setup : ofmt_fail_system);

    return 0;
}

int ofmt_set_pipe_resolver(struct ofmt *ofmt, ofmt_resolver_callback resolver)
{
    ofmt->pipe_resolver = resolver;
    return 0;
}

static void reset(struct ofmt *ofmt, enum ofmt_mode mode)
{
    ofmt->error = (struct ofmt_err){};
    ofmt->bufwrite = 0;

    switch (mode) {
    case ofmt_mode_print:
        ofmt->print.iovwrite = 0;
        ofmt->print.phwrite = 0;
        ofmt->ofp_setup.emit = print_emit;
        ofmt->mode = ofmt_mode_print;
        break;
    case ofmt_mode_pipe:
        ofmt->subproc.pipe_enabled = true;
        /* fallthrough */
    case ofmt_mode_exec:
        ofmt->subproc.segwrite = 0;
        ofmt->ofp_setup.emit = subproc_emit;
        ofmt->mode = ofmt_mode_exec;
        break;
    default:
        abort();
    }
}

int ofmt_compile(struct ofmt *ofmt,
                 enum ofmt_mode mode,
                 const char *spec,
                 size_t speclen)
{
    reset(ofmt, mode);

    if (ensure_buffer(ofmt, speclen) == -1)
        return -1;

    if (ofp_scan(&ofmt->ofp_setup, spec, speclen) == -1)
        return -1;

    switch (mode) {
    case ofmt_mode_pipe:
    case ofmt_mode_exec:
        if (subproc_post_scan(ofmt) == -1)
            return -1;
    case ofmt_mode_print:
        break;
    }
    return 0;
}

int ofmt_evaluate(struct ofmt *ofmt, const struct ofmt_evaluate_params *params)
{
    switch (ofmt->mode) {
    case ofmt_mode_exec:
        return subproc_eval(ofmt, params);
    case ofmt_mode_print:
        return print_eval(ofmt, params);
    default:
        abort();
    }
}

const char *ofmt_fail_str(enum ofmt_fail f)
{
    static const char *const repr[] = {
        [ofmt_fail_none] = "ofmt_fail_none",
        [ofmt_fail_bad_setup] = "ofmt_fail_bad_setup",
        [ofmt_fail_bad_state] = "ofmt_fail_bad_state",
        [ofmt_fail_invalid_pholder] = "ofmt_fail_invalid_pholder",
        [ofmt_fail_parse] = "ofmt_fail_parse",
        [ofmt_fail_subproc] = "ofmt_fail_subproc",
        [ofmt_fail_system] = "ofmt_fail_system",
        [ofmt_fail_too_long_evaluation] = "ofmt_fail_too_long_evaluation",
        [ofmt_fail_too_long_spec] = "ofmt_fail_too_long_spec",
        [ofmt_fail_too_many_args] = "ofmt_fail_too_many_args",
        [ofmt_fail_too_many_pholders] = "ofmt_fail_too_many_pholders",
        [ofmt_fail_undefined_resolver] = "ofmt_fail_undefined_resolver",
    };
    return repr[f];
}

const struct ofmt_err *ofmt_get_error(const struct ofmt *ofmt)
{
    return &ofmt->error;
}

void ofmt_print_error(const struct ofmt *ofmt)
{
    switch (ofmt->error.reason) {
    case ofmt_fail_parse:
        warnx("output format parse error: %s",
              ofp_fail_str(ofmt->error.ofp_reason));
        break;
    case ofmt_fail_system:
        warn("output format system error");
        break;
    case ofmt_fail_subproc:
        if (WIFEXITED(ofmt->error.subproc.exit_status))
            warnx("%s: exit %d",
                  ofmt->error.subproc.argv0,
                  WEXITSTATUS(ofmt->error.subproc.exit_status));
        else if (WIFSIGNALED(ofmt->error.subproc.exit_status))
            warnx("%s: terminated by signal %d",
                  ofmt->error.subproc.argv0,
                  WTERMSIG(ofmt->error.subproc.exit_status));
        else
            warnx("%s: stopped/resumed", ofmt->error.subproc.argv0);
        break;
    default:
        warnx("output format error: %s", ofmt_fail_str(ofmt->error.reason));
        break;
    }
}

void ofmt_del(struct ofmt *ofmt)
{
    if (!ofmt)
        return;

    int errno_s = errno;
    radix_del(ofmt->resolvers_map);
    free_opaque(ofmt);
    free(ofmt->buffer);
    free(ofmt);
    errno = errno_s;
}
