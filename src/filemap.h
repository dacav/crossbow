#pragma once

#include <stddef.h>

struct filemap {
    void *memory;
    size_t size;
};

int filemap_load_fd(struct filemap *, int fd);

int filemap_load_path(struct filemap *, const char *fname);

void filemap_unload(struct filemap *);
