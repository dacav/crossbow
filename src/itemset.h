#pragma once

#include <stdbool.h>
#include <stdint.h>

struct itemset;

typedef enum {
    itemset_eok = 0,   /* success */
    itemset_edup = -1, /* duplicated item */
    itemset_emax = -2, /* max set size reached */
    itemset_esys = -3, /* system error */
} itemset_error;

struct itemset *itemset_new(void);

bool itemset_exists(const struct itemset *, const uint8_t *data, uint16_t len);

itemset_error itemset_add(struct itemset *, const uint8_t *data, uint16_t len);

bool itemset_iter(const struct itemset *,
                  const void **aux,
                  const uint8_t **data,
                  uint16_t *data_len);

uint16_t itemset_get_size(const struct itemset *);

void itemset_del(struct itemset *);
