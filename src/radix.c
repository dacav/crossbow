#include <err.h>
#include <errno.h>
#include <stdlib.h>

#include "radix.h"

struct node {
    struct node *next;
    struct node *sub;
    void *data;
    char symbol;
};

struct radix {
    struct node *root;
};

int radix_lookup(const struct radix *r,
                 const char *path,
                 int pathlen,
                 void **out)
{
    const struct node *c = r->root;
    int depth = 0;
    void *found = NULL;

    if (pathlen == 0) {
        errno = EINVAL;
        return -1;
    }

    for (int i = 0; i < pathlen; ++i) {

        while (c && c->symbol < path[i])
            c = c->next;

        if (!c || c->symbol > path[i])
            goto exit;

        found = c->data;
        depth = i + 1;
        c = c->sub;
    }

exit:
    if (!found)
        return -1;
    *out = found;
    return depth;
}

int radix_register(struct radix *r, const char *path, void *item)
{
    struct node *c = r->root;
    struct node **back = &r->root;

    for (int i = 0; path[i]; ++i) {

        while (c && c->symbol < path[i]) {
            back = &c->next;
            c = c->next;
        }

        if (!c || c->symbol > path[i]) {
            struct node *n = malloc(sizeof(struct node));
            if (!n)
                return -1;

            *n = (struct node){
                .symbol = path[i],
            };

            *back = n;
            n->next = c;
            c = n;
        }

        if (path[i + 1] == '\0') {
            c->data = item;
            return 0;
        }

        back = &c->sub;
        c = c->sub;
    }

    errno = EINVAL;
    return -1;
}

static void node_free(struct node *n)
{
    if (!n)
        return;

    node_free(n->next);
    node_free(n->sub);
    free(n);
}

struct radix *radix_new(void)
{
    struct radix *r;

    r = malloc(sizeof(struct radix));
    if (r)
        *r = (struct radix){};
    else
        warn("malloc");
    return r;
}

void radix_del(struct radix *r)
{
    if (!r)
        return;

    node_free(r->root);
    free(r);
}
