#pragma once

#include <stdbool.h>
#include <stddef.h>

struct rc;

struct rc_global {
    unsigned short jobs;
    const char *persist_dir;
};

enum rc_handler {
    rc_hdl_print,
    rc_hdl_exec,
    rc_hdl_pipe,
};

struct rc_feed {
    const char *name;
    const char *url;
    struct {
        const char *spec;
        size_t len;
    } ofmt;
    const char *chdir;
    enum rc_handler handler;
};

struct rc *rc_new(void);

int rc_parse_buffer(struct rc *, const char *str, size_t len);

int rc_load(struct rc *);

const struct rc_global *rc_get_global(const struct rc *);

const struct rc_feed *rc_get_feed(const struct rc *, unsigned idx);

unsigned rc_n_feeds(const struct rc *);

void rc_dump(const struct rc *);

void rc_free(struct rc *);
