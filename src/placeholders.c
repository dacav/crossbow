#include "placeholders.h"
#include "util.h"

#include <err.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <eccio.h>

// Size required to represent the maximum integer of a given type.
// See https://stackoverflow.com/a/44024876
#define MAXINT_BUFSIZE(x) ((size_t)(CHAR_BIT * sizeof(x) * 302 / 1000) + 1)

struct state {
    struct placeholder_extra extra;
    struct {
        char incremental_id[7];
    } repr_bufs;
};

struct state;

static char *snpf(char buffer[], size_t buflen, const char *fmt, ...)
{
    va_list ap;
    int n;

    va_start(ap, fmt);
    n = vsnprintf(buffer, buflen, fmt, ap);
    va_end(ap);

    if (n >= buflen)
        warnx("placeholder truncated");
    return buffer;
}

static const char *resolve_id_is_guid(const void *item, void *opaque)
{
    (void)opaque;
    return ((const EccioEntry *)item)->id.is_guid ? "1" : "0";
}

#define MAKE_FIELD_RESOLVER(resolver_name, field)                              \
    static const char *resolver_name(const void *item, void *opaque)           \
    {                                                                          \
        (void)opaque;                                                          \
        return ((const EccioEntry *)item)->field;                              \
    }

#define MAKE_EXTRA_RESOLVER(resolver_name, field)                              \
    static const char *resolver_name(const void *item, void *opaque)           \
    {                                                                          \
        const struct state *state = opaque;                                    \
        return state->extra.field;                                             \
    }

// clang-format off
MAKE_FIELD_RESOLVER(resolve_author_email,       author.email);
MAKE_FIELD_RESOLVER(resolve_author_name,        author.name);
MAKE_FIELD_RESOLVER(resolve_author_uri,         author.uri);
MAKE_FIELD_RESOLVER(resolve_comments,           comments);
MAKE_FIELD_RESOLVER(resolve_content,            content);
MAKE_FIELD_RESOLVER(resolve_id_value,           id.value);
MAKE_FIELD_RESOLVER(resolve_pub_date,           pub_date);
MAKE_FIELD_RESOLVER(resolve_summary,            summary);
MAKE_FIELD_RESOLVER(resolve_title,              title);

MAKE_EXTRA_RESOLVER(resolve_feed_identifier,    feed_identifier);
MAKE_EXTRA_RESOLVER(resolve_feed_title,         feed_title);
// clang-format on

static const char *resolve_incremental_id(const void *item, void *opaque)
{
    struct state *state = opaque;

    return snpf(state->repr_bufs.incremental_id,
                sizeof state->repr_bufs.incremental_id,
                "%06u",
                state->extra.entry.incremental_id);
}

static const char *resolve_link(const void *item, void *opaque)
{
    const EccioEntry *entry = item;

    (void)opaque;
    // TODO: add outfmt syntax to specify an index!
    return eccio_entry_get_link(entry, 0);
}

static const char *resolve_category(const void *item, void *opaque)
{
    const EccioEntry *entry = item;

    (void)opaque;
    // TODO: add outfmt syntax to specify an index!
    return eccio_entry_get_category(entry, 0);
}

int placeholders_setup(struct ofmt *ofmt)
{
    struct state *state;

    state = calloc(1, sizeof(struct state));
    if (!state) {
        warn("malloc");
        return -1;
    }
    ofmt_set_opaque(ofmt, state, free);

#define ADD_RESOLVER(resolver_name, placeholder)                               \
    do {                                                                       \
        if (ofmt_set_resolver(ofmt, placeholder, resolver_name) == -1)         \
            goto fail;                                                         \
    } while (0)

    // clang-format off
    ADD_RESOLVER(resolve_author_email,      "am");
    ADD_RESOLVER(resolve_author_name,       "a");
    ADD_RESOLVER(resolve_author_uri,        "au");
    ADD_RESOLVER(resolve_category,          "ca");
    ADD_RESOLVER(resolve_comments,          "co");
    ADD_RESOLVER(resolve_content,           "c");
    ADD_RESOLVER(resolve_feed_identifier,   "fi");
    ADD_RESOLVER(resolve_feed_title,        "ft");
    ADD_RESOLVER(resolve_id_is_guid,        "gp");
    ADD_RESOLVER(resolve_id_value,          "g");
    ADD_RESOLVER(resolve_incremental_id,    "n");
    ADD_RESOLVER(resolve_link,              "l");
    ADD_RESOLVER(resolve_pub_date,          "pd");
    ADD_RESOLVER(resolve_summary,           "s");
    ADD_RESOLVER(resolve_title,             "t");
    // clang-format on
#undef ADD_RESOLVER

    if (ofmt_set_pipe_resolver(ofmt, resolve_content) == -1)
        goto fail;

    return 0;

fail:
    warnx("placeholders_setup failed");
    ofmt_print_error(ofmt);
    return -1;
}

void placeholders_set_extra(const struct ofmt *ofmt,
                            const struct placeholder_extra *extra)
{
    struct state *state = ofmt_get_opaque(ofmt);
    if (!state)
        panic(); // placeholders_setup must be called first!

    state->extra = *extra;
}
