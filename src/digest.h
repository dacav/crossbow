#pragma once

#include <stddef.h>
#include <stdint.h>

struct digest;

enum {
    digest_buflen = 20,
};

struct digest *digest_new(void);

int digest_add(struct digest *, const uint8_t *bytes, size_t len);

int digest_add_cstr(struct digest *, const char *s);

int digest_finish(struct digest *, uint8_t *bytes, size_t len);

int digest_reset(struct digest *d);

void digest_del(struct digest *);
