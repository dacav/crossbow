#pragma once

struct persist_dir;

struct persist_dir *persist_dir_new(int basedir);

struct persist_file *persist_dir_iter(struct persist_dir *, void **aux);

int persist_dir_fd(const struct persist_dir *);

void persist_dir_del(struct persist_dir *);
