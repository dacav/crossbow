#include "download.h"

#include <err.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <curl/curl.h>
#include <utlist.h>

#include "config.h"
#include "logging.h"
#include "util.h"

struct job {
    char errbuf[CURL_ERROR_SIZE];
    download_callback callback;
    void *opaque;
    CURL *request;
    int output_file;
    struct job *prev, *next;
};

struct download {
    CURLM *curlm;
    struct job *jobs;
    bool global_init;
};

static CURLM *setup_curl_multi(unsigned maxjobs)
{
    CURLM *curlm = NULL;
    CURLMcode err;

    curlm = curl_multi_init();
    if (!curlm) {
        warnx("curl_multi_init failed");
        return NULL;
    }

    err =
        curl_multi_setopt(curlm, CURLMOPT_MAX_TOTAL_CONNECTIONS, (long)maxjobs);
    if (err) {
        warnx("curl_multi_setopt CURLMOPT_MAXCONNECTS: %s",
              curl_multi_strerror(err));
        goto fail;
    }

    return curlm;

fail:
    curl_multi_cleanup(curlm);
    return NULL;
}

static int open_output_file(struct job *job)
{
    int fd;
    char filename[] = ".crossbow_XXXXXX";

    fd = mkostemps(filename, 0, O_CLOEXEC);
    if (fd == -1) {
        warn("mkstemp");
        return -1;
    }

    if (unlink(filename))
        warn("unlink"); /* Not critical, just warn and continue. */

    job->output_file = fd;
    return 0;
}

static size_t write_cb(void *ptr, size_t size, size_t nmemb, void *opaque)
{
    struct job *job = opaque;
    size_t wrote = 0, to_write = size * nmemb;

    if (job->output_file == -1 && open_output_file(job))
        return 0; /* note: write_cb has the same interface as fwrite(3) */

    while (to_write) {
        ssize_t w;

        w = write(job->output_file, (uint8_t *)ptr + wrote, to_write - wrote);
        if (w <= 0) {
            warn("write -> %zd", w);
            return 0;
        }

        wrote += w;
        to_write -= w;
    }

    return size * nmemb;
}

static CURL *setup_curl_easy(struct download *dl,
                             const char *url,
                             struct job *job)
{
    CURL *request;
    CURLcode err_setopt = 0;
    CURLMcode err_multi;

    request = curl_easy_init();
    if (!request) {
        warnx("curl_easy_init failed");
        return NULL;
    }

    err_setopt = curl_easy_setopt(request, CURLOPT_URL, url);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_ERRORBUFFER, job->errbuf);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(
        request, CURLOPT_USERAGENT, PROJECT_NAME "/" PROJECT_VERSION_FULL);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(
        request, CURLOPT_PROTOCOLS_STR, "file,gopher,http,https");
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_WRITEFUNCTION, write_cb);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_WRITEDATA, job);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_FAILONERROR, 1L);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_FOLLOWLOCATION, 1L);
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_DEFAULT_PROTOCOL, "https");
    if (err_setopt)
        goto fail;

    err_setopt = curl_easy_setopt(request, CURLOPT_PRIVATE, job);
    if (err_setopt)
        goto fail;

    err_multi = curl_multi_add_handle(dl->curlm, request);
    if (err_multi) {
        warnx("curl_multi_add_handle: %s", curl_multi_strerror(err_multi));
        goto fail;
    }

    return request;

fail:
    if (err_setopt)
        warnx("curl_easy_setopt: %s", curl_easy_strerror(err_setopt));
    curl_easy_cleanup(request);
    return NULL;
}

struct download *download_new(bool global_init, unsigned maxjobs)
{
    struct download *dl;

    dl = malloc(sizeof(*dl));
    if (!dl) {
        warn("malloc");
        return NULL;
    }
    *dl = (struct download){};

    if (global_init) {
        CURLcode err;

        err = curl_global_init(CURL_GLOBAL_ALL);
        if (err) {
            warnx("curl_global_init: %s", curl_easy_strerror(err));
            goto fail;
        }
        dl->global_init = true;
    }

    dl->curlm = setup_curl_multi(maxjobs);
    if (!dl->curlm)
        goto fail;

    return dl;

fail:
    download_del(dl);
    return NULL;
}

static void job_delete(struct download *dl, struct job *job)
{
    if (!job)
        return;

    if (job->prev || job->next)
        DL_DELETE(dl->jobs, job);

    if (job->request) {
        CURLMcode err;

        err = curl_multi_remove_handle(dl->curlm, job->request);
        if (err)
            warnx("curl_multi_remove_handle: %s", curl_multi_strerror(err));

        curl_easy_cleanup(job->request);
    }

    if (job->output_file != -1) {
        if (close(job->output_file))
            warn("close");
    }
    free(job);
}

int download_schedule(struct download *dl,
                      const char *url,
                      download_callback cb,
                      void *opaque)
{
    struct job *job;

    job = malloc(sizeof(*job));
    if (!job) {
        warn("malloc");
        return -1;
    }

    *job = (struct job){
        .callback = cb,
        .opaque = opaque,
        .request = setup_curl_easy(dl, url, job),
        .output_file = -1,
    };

    if (!job->request)
        goto fail;

    DL_APPEND(dl->jobs, job);
    return 0;

fail:
    job_delete(dl, job);
    return -1;
}

static enum download_status request_failed(const struct job *job)
{
    CURLcode err;
    long code;
    char *scheme;

    err = curl_easy_getinfo(job->request, CURLINFO_SCHEME, &scheme);
    if (err) {
        warnx("curl_easy_getinfo CURLINFO_SCHEME: %s", curl_easy_strerror(err));
        return dl_sys_failure;
    }
    if (!scheme) {
        warnx("curl_easy_getinfo CURLINFO_SCHEME: unexpectedly NULL result");
        return dl_sys_failure;
    }

    switch (scheme[0]) {
    case 'f':
    case 'F':
        if (!strcasecmp("file", scheme))
            break;
        panic_info("unexpected scheme %s", scheme);

    case 'g':
    case 'G':
        /* Gopher is not very good at error handling.  There's no
         * generalized way of detecting errors.  E.g. gophernicus will
         * report erros with some user-friendly text.  Assuming
         * there's no error.
         * If adding more schemes starting with 'g', then remove the comment
         * from the following code:
         */
        if (!strcasecmp("gopher", scheme))
            break;
        panic_info("unexpected scheme %s", scheme);

    case 'h':
    case 'H':
        if (!strcasecmp("http", scheme) || !strcasecmp("https", scheme)) {
            err =
                curl_easy_getinfo(job->request, CURLINFO_RESPONSE_CODE, &code);
            if (err) {
                warnx("curl_easy_getinfo CURLINFO_RESPONSE_CODE: %s",
                      curl_easy_strerror(err));
                return dl_sys_failure;
            }
            return (code < 200 || code >= 300) ? dl_fetch_failure : dl_complete;
        }
        panic_info("unexpected scheme %s", scheme);
    }

    /* Not knowing any better, we assume a failure if the file was never
     * assigned so far by a write operation. */
    return job->output_file == -1 ? dl_fetch_failure : dl_complete;
}

static int report_failure(struct job *job, enum download_status type)
{
    const char *errmsg = NULL;

    switch (type) {
    case dl_complete:
        /* This never happens by design, but handling all cases is required by
         * the compiler, and generally a good practice. */
        panic(); // report_failure called with dl_complete.

    case dl_aborted:
        errmsg = "aborted";
        break;
    case dl_fetch_failure:
        /* errbuf might be empty, e.g. when using file:/// and the file does
         * not exist.  Using a generic message, it is better than nothing. */
        errmsg = job->errbuf[0] ? job->errbuf : "not found";
        break;
    case dl_sys_failure:
        /* More specific info is already reported via warn(3) or warnx(3). */
        errmsg = "system failure";
    }

    return job->callback(job->opaque, type, errmsg, -1);
}

static int deliver(struct job *job)
{
    int output_file;

    output_file = job->output_file;
    job->output_file = -1;

    if (fdatasync(output_file)) {
        warn("fdatasync");
        goto fail;
    }
    if (lseek(output_file, 0, SEEK_SET)) {
        warn("lseek");
        goto fail;
    }

    return job->callback(job->opaque, dl_complete, "success", output_file);

fail:
    if (close(output_file))
        warn("close");
    return report_failure(job, dl_sys_failure);
}

static int flush_ready(struct download *dl)
{
    int msgq_ign;
    CURLMsg *msg;

    while (msg = curl_multi_info_read(dl->curlm, &msgq_ign), msg) {
        struct job *job;
        int cancel;
        CURLcode e;

        if (msg->msg != CURLMSG_DONE)
            continue;

        e = curl_easy_getinfo(msg->easy_handle, CURLINFO_PRIVATE, &job);
        if (e) {
            /* This should not happen, but if it does, the job delivery
             * and deletion is going to be postponed until all downloads
             * are complete.
             */
            warnx("curl_easy_getinfo CURLINFO_PRIVATE: %s",
                  curl_easy_strerror(e));
            continue;
        }

        if (msg->data.result != CURLE_OK)
            cancel = report_failure(job, dl_fetch_failure);
        else
            switch (request_failed(job)) {
            case dl_sys_failure:
                cancel = report_failure(job, dl_sys_failure);
                break;

            case dl_complete:
                cancel = deliver(job);
                break;

            case dl_fetch_failure:
                cancel = report_failure(job, dl_fetch_failure);
                break;

            case dl_aborted:
                panic(); // unexpected request_failed value
            }

        job_delete(dl, job);
        if (cancel)
            return -1;
    }

    return 0;
}

int download_perform(struct download *dl)
{
    int still_running;

    for (;;) {
        CURLMcode mc;

        mc = curl_multi_perform(dl->curlm, &still_running);
        if (mc) {
            warnx("curl_multi_perform failure: %s", curl_multi_strerror(mc));
            return -1;
        }

        if (flush_ready(dl)) {
            notice("further transfers aborted");
            break; /* canceled by delivery callback */
        }

        if (!still_running) {
            debug("no more running downloads");
            break;
        }

        mc = curl_multi_poll(dl->curlm, NULL, 0, 128, NULL);
        if (mc) {
            warnx("curl_multi_poll failure: %s", curl_multi_strerror(mc));
            return -1;
        }
    }

    return 0;
}

void download_abort(struct download *dl)
{
    unsigned int interrupted = 0;
    struct job *job, *tmp;

    if (!dl->jobs)
        return;

    DL_FOREACH_SAFE(dl->jobs, job, tmp)
    {
        /* Deliver failure for all aborted downloads. */
        (void)report_failure(job, dl_aborted);
        job_delete(dl, job);
        ++interrupted;
    }

    if (interrupted)
        notice("%d download jobs were interrupted", interrupted);
}

void download_del(struct download *dl)
{
    if (!dl)
        return;

    download_abort(dl);
    curl_multi_cleanup(dl->curlm);
    if (dl->global_init)
        curl_global_cleanup();

    free(dl);
}
