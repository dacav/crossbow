#pragma once

struct radix;

struct radix *radix_new(void);

int radix_register(struct radix *, const char *path, void *item);

int radix_lookup(const struct radix *,
                 const char *path,
                 int pathlen,
                 void **found);

void radix_del(struct radix *);
