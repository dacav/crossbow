#include "text.h"

#include <ctype.h>
#include <stdint.h>

enum {
    dots_len = 3,
};

static const char *end_with_dots(char *dst, size_t dst_size)
{
    switch (dst_size) {
    default:
        dst[dst_size - 4] = '.';
    case 3:
        dst[dst_size - 3] = '.';
    case 2:
        dst[dst_size - 2] = '.';
    case 1:
        dst[dst_size - 1] = '\0';
    case 0:
        return dst;
    }
}

const char *text_short(char *dst,
                       size_t dst_size,
                       const char *text,
                       size_t text_len)
{
    size_t j = 0;
    enum {
        normal,
        skip_empty
    } phase = normal;

#define APPEND(c)                                                              \
    do {                                                                       \
        if (j < dst_size - 1)                                                  \
            dst[j++] = c;                                                      \
        else                                                                   \
            return end_with_dots(dst, dst_size);                               \
    } while (0)

    if (!dst || !dst_size || !text || !text_len) {
        static const char nul;
        return &nul;
    }

    for (size_t i = 0; i < text_len && text[i]; ++i) {
        char c = text[i];

        switch (phase) {
        case normal:
            if (c == '\n' || c == '\r') {
                APPEND(c);
                APPEND(' ');
                APPEND(' ');
                phase = skip_empty;
                continue;
            }

            if (!isprint((unsigned char)c)) {
                APPEND('.');
                continue;
            }
            break;

        case skip_empty:
            if (isspace((unsigned char)c))
                continue;
            phase = normal;
        }

        APPEND(c);
    }
#undef APPEND

    dst[j] = 0;
    return dst;
}

const char *text_join_argv(char *dst, size_t dst_size, const char *const *argv)
{
    size_t j = 0;

#define APPEND(c)                                                              \
    do {                                                                       \
        if (j < dst_size - 1)                                                  \
            dst[j++] = c;                                                      \
        else                                                                   \
            return end_with_dots(dst, dst_size);                               \
    } while (0)

    for (size_t i = 0; argv[i]; ++i) {
        if (i)
            APPEND(' ');

        for (size_t k = 0; argv[i][k]; ++k) {
            char c = argv[i][k];
            switch (c) {
            case ' ':
            case '\\':
                APPEND('\\');
            default:
                break;
            }
            APPEND(c);
        }
    }
#undef APPEND

    dst[j] = 0;
    return dst;
}
