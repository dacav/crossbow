#pragma once

#include <stddef.h>

enum ofp_atom {
    ofp_atom_pholder,
    ofp_atom_verbatim,
    ofp_atom_whitespace,
};

struct ofp_token {
    const char *begin;
    size_t len;
    enum ofp_atom atom;
};

const char *ofp_atom_str(enum ofp_atom);

enum ofp_fail {
    ofp_fail_none = 0,

    ofp_fail_bad_spec,
    ofp_fail_bad_state,
    ofp_fail_emitting,
    ofp_fail_invalid_pholder,
    ofp_fail_trail_escape,
};

const char *ofp_fail_str(enum ofp_fail);

struct ofp_setup {
    void *opaque;
    int (*emit)(void *, const struct ofp_token *);
    void (*fail)(void *, enum ofp_fail);
};

int ofp_scan(const struct ofp_setup *, const char *spec, size_t speclen);
