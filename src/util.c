#include <stdint.h>

#include "util.h"

const char *to_hex(char *dst,
                   size_t dst_len,
                   const void *src_obj,
                   size_t src_len)
{
    const char digits[] = "0123456789abcdef";
    const uint8_t *src = src_obj;
    size_t j = 0;

    if (dst_len < 1)
        return "";

    for (size_t i = 0; i < src_len - 1 && j + 2 < dst_len; i++) {
        dst[j++] = digits[(src[i] & 0xf0) >> 4];
        dst[j++] = digits[(src[i] & 0x0f) >> 0];
    }
    dst[j] = 0;

    return dst;
}
