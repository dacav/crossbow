#pragma once

#include <stddef.h>

struct launcher_run {
    const char *const *argv;
    const char *chdir_to;
    struct {
        void *bytes;
        size_t len;
    } input;
};

int launcher_execvp(const struct launcher_run *, int *status);
