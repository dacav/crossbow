#include "persist_dir.h"

#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <uthash.h>

#include "persist_file.h"

struct persist_dir {
    int basedir;
    unsigned n_files;
    struct persist_file **files;
};

enum {
    min_files = 16,
};

static int resize_files(struct persist_dir *pd, unsigned new_n_files)
{
    struct persist_file **new_files;

    if (new_n_files == pd->n_files)
        return 0;

    new_files = reallocarray(pd->files, new_n_files, sizeof(void *));
    if (!new_files) {
        warn("reallocarray");
        return -1;
    }

    pd->files = new_files;
    pd->n_files = new_n_files;
    return 0;
}

static int append(struct persist_dir *pd,
                  struct persist_file *pf,
                  unsigned *n_files)
{
    if (*n_files >= pd->n_files) {
        unsigned new_n_files;

        new_n_files = pd->n_files ? 2 * pd->n_files : min_files;
        if (resize_files(pd, new_n_files))
            return -1;
    }

    pd->files[(*n_files)++] = pf;
    return 0;
}

static int scan(struct persist_dir *pd)
{
    int dirfd, e = -1;
    DIR *dir = NULL;
    struct dirent *dent;
    unsigned n_files = 0;

    dirfd = dup(pd->basedir);
    if (dirfd == -1) {
        warn("dup");
        goto fail;
    }

    dir = fdopendir(dirfd);
    if (!dir) {
        warn("fdopendir");
        goto fail;
    }
    dirfd = -1;

    while (errno = 0, dent = readdir(dir)) {
        struct stat sb;
        struct persist_file *pf;

        if (fstatat(pd->basedir, dent->d_name, &sb, 0) == -1) {
            warn("fstatat(..., %s) failed", dent->d_name);
            goto fail;
        }

        if (!S_ISREG(sb.st_mode)) {
            if (dent->d_name[0] != '.')
                warnx("ignoring file %s", dent->d_name);
            continue;
        }

        pf = persist_file_new(pd, dent->d_name);
        if (!pf)
            goto fail;

        if (append(pd, pf, &n_files)) {
            persist_file_del(pf);
            goto fail;
        }
    }

    if (resize_files(pd, n_files))
        goto fail;

    e = 0;
fail:
    if (dirfd != -1 && close(dirfd))
        warn("close(%d)", dirfd);
    if (dir)
        closedir(dir);
    return e;
}

struct persist_dir *persist_dir_new(int basedir)
{
    struct persist_dir *pd;

    pd = malloc(sizeof(*pd));
    if (pd)
        *pd = (struct persist_dir){
            .basedir = basedir,
        };
    else
        warn("malloc");

    return pd;
}

struct persist_file *persist_dir_iter(struct persist_dir *pd, void **aux)
{
    struct persist_file **it;

    it = *aux;
    if (it == NULL) {
        if (!pd->files && scan(pd))
            return NULL;

        it = pd->files;
    }

    if (it - pd->files >= pd->n_files) {
        /* End of iteration */
        *aux = NULL;
        return NULL;
    }

    *aux = it + 1;
    return *it;
}

int persist_dir_fd(const struct persist_dir *pd)
{
    return pd->basedir;
}

void persist_dir_del(struct persist_dir *pd)
{
    if (!pd)
        return;

    if (close(pd->basedir))
        warn("close(%d)", pd->basedir);

    for (unsigned i = 0; i < pd->n_files; ++i)
        persist_file_del(pd->files[i]);
    free(pd->files);

    free(pd);
}
