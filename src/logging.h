#pragma once

extern unsigned g_verbosity_level;

#define debug_enabled (g_verbosity_level > 2)
#define debug(...)                                                             \
    do {                                                                       \
        if (debug_enabled)                                                     \
            warnx(__VA_ARGS__);                                                \
    } while (0)

#define info_enabled (g_verbosity_level > 1)
#define info(...)                                                              \
    do {                                                                       \
        if (info_enabled)                                                      \
            warnx(__VA_ARGS__);                                                \
    } while (0)

#define notice_enabled (g_verbosity_level > 0)
#define notice(...)                                                            \
    do {                                                                       \
        if (notice_enabled)                                                    \
            warnx(__VA_ARGS__);                                                \
    } while (0)
