#include <err.h>
#include <stdlib.h>

#include <uthash.h>

#include "itemset.h"

enum {
    max_size = 10000,
};

struct item {
    uint8_t *data;
    uint16_t data_len;
    UT_hash_handle hh;
};

struct itemset {
    struct item *set;
    uint16_t size;
};

struct itemset *itemset_new(void)
{
    struct itemset *is;

    is = malloc(sizeof(*is));
    if (is)
        *is = (struct itemset){};
    else
        warn("malloc");
    return is;
}

struct item *find(const struct itemset *is, const uint8_t *data, uint16_t len)
{
    struct item *item;

    HASH_FIND(hh, is->set, data, len, item);
    return item;
}

bool itemset_exists(const struct itemset *is, const uint8_t *data, uint16_t len)
{
    return find(is, data, len) != NULL;
}

itemset_error itemset_add(struct itemset *is,
                          const uint8_t *data,
                          uint16_t data_len)
{
    struct item *item;

    item = find(is, data, data_len);
    if (item)
        return itemset_edup;

    if (is->size >= max_size)
        return itemset_emax;

    item = malloc(sizeof(*item));
    if (!item) {
        warn("malloc");
        return itemset_esys;
    }

    *item = (struct item){
        .data = malloc(data_len),
        .data_len = data_len,
    };

    if (!item->data) {
        warn("malloc");
        free(item);
        return itemset_esys;
    }
    memcpy(item->data, data, data_len);

    HASH_ADD_KEYPTR(hh, is->set, item->data, item->data_len, item);
    is->size++;
    return itemset_eok;
}

bool itemset_iter(const struct itemset *is,
                  const void **aux,
                  const uint8_t **data,
                  uint16_t *data_len)
{
    const struct item *item = *aux;

    if (item)
        item = item->hh.next;
    else
        item = is->set;
    *aux = item;

    if (!item)
        return false;

    *data = item->data;
    *data_len = item->data_len;
    return true;
}

uint16_t itemset_get_size(const struct itemset *is)
{
    return is->size;
}

void itemset_del(struct itemset *is)
{
    struct item *item, *tmp;

    if (!is)
        return;

    HASH_ITER(hh, is->set, item, tmp)
    {
        HASH_DELETE(hh, is->set, item);
        free(item->data);
        free(item);
    }

    free(is);
}
