#include <err.h>
#include <stdbool.h>
#include <string.h>
#include <sysexits.h>

#include <openssl/evp.h>

#include "digest.h"
#include "util.h"

#define DIGEST_NAME "sha1"

struct digest {
    EVP_MD_CTX *mdctx;
};

struct digest *digest_new(void)
{
    struct digest *d;

    d = malloc(sizeof *d);
    if (!d) {
        warn("malloc");
        return NULL;
    }

    d->mdctx = EVP_MD_CTX_new();
    if (!d->mdctx) {
        warnx("EVP_MD_CTX_new");
        goto fail;
    }

    if (digest_reset(d))
        goto fail;

    return d;

fail:
    digest_del(d);
    return NULL;
}

int digest_add(struct digest *d, const uint8_t *bytes, size_t len)
{
    if (!EVP_DigestUpdate(d->mdctx, bytes, len)) {
        warnx("EVP_DigestUpdate");
        return -1;
    }

    return 0;
}

int digest_add_cstr(struct digest *d, const char *s)
{
    if (s)
        return digest_add(d, (const uint8_t *)s, strlen(s));
    else
        return 0;
}

int digest_finish(struct digest *d, uint8_t *bytes, size_t len)
{
    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;

    if (len < digest_buflen)
        return -1;

    if (!EVP_DigestFinal(d->mdctx, md_value, &md_len)) {
        warnx("EVP_DigestFinal");
        return -1;
    }

    if (md_len != digest_buflen)
        panic_info("Digest " DIGEST_NAME ", md_len %u digest_buflen %d",
                   md_len,
                   digest_buflen);

    memcpy(bytes, md_value, digest_buflen);
    return 0;
}

int digest_reset(struct digest *d)
{
    if (!EVP_DigestInit(d->mdctx, EVP_sha1())) {
        warnx("EVP_DigestInit");
        return -1;
    }
    return 0;
}

void digest_del(struct digest *d)
{
    if (!d)
        return;

    if (d->mdctx)
        EVP_MD_CTX_free(d->mdctx);
    free(d);
}
