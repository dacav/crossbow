#pragma once

#include "digest.h"
#include "outfmt.h"

int placeholders_setup(struct ofmt *);

/* This struct collects additional information that gets copied in the
 * opaque structure within a ofmt object.  This information is
 * ultimately available to the placeholder resolution functions
 * defined in 'placeholders.c'. */
struct placeholder_extra {
    const char *feed_title;
    const char *feed_identifier;
    struct {
        unsigned incremental_id;
        uint8_t uid[digest_buflen];
        char uid_str[digest_buflen * 2 + 1];
    } entry;
};

void placeholders_set_extra(const struct ofmt *,
                            const struct placeholder_extra *);
