#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <eccio.h>
#include <uthash.h>

#include "config.h"
#include "digest.h"
#include "download.h"
#include "filemap.h"
#include "itemset.h"
#include "logging.h"
#include "outfmt.h"
#include "persist_dir.h"
#include "persist_file.h"
#include "placeholders.h"
#include "rc.h"
#include "text.h"
#include "util.h"

struct opts {
    const char *uid;
    bool dry_mark : 1;
    bool dry_exec : 1;
    bool catch_up : 1;
};

struct feed_info {
    const char *name;
    const struct rc_feed *config;
    struct persist_file *persist_file;
    bool own_persist;
    bool ofmt_loaded;
    bool enabled;
    struct ctx *global_ctx;

    UT_hash_handle hh;
};

struct feed_map {
    struct feed_info *slots;
    struct feed_info *map;
    unsigned size;
};

struct ctx {
    struct rc *rc;
    struct download *dload;
    struct ofmt *ofmt;
    const struct opts opts;
    struct persist_dir *persist_dir;
    struct digest *digest;
    EccioParser *feed_parser;
    unsigned failures;
};

enum process_status {
    ps_success,
    ps_fail_critical,
    ps_fail_feed,
    ps_fail_item,
};

static void parser_error_handler(void *opaq, const EccioErrInfo *einfo)
{
    const char *feed_name, *message;
    int errcode;

    eccio_err_info_get(einfo, ECCIO_ERRKEY_FEEDNAME, &feed_name);
    eccio_err_info_get(einfo, ECCIO_ERRKEY_MESSAGE, &message);
    eccio_err_info_get(einfo, ECCIO_ERRKEY_ERRCODE, &errcode);

    warnx("Parsing feed %s: %s (errcode %d)", feed_name, message, errcode);
}

static void feed_map_free(struct feed_map *feed_map)
{
    struct feed_info *feed_info, *tmp;

    HASH_ITER(hh, feed_map->map, feed_info, tmp)
    {
        if (feed_info->own_persist)
            persist_file_del(feed_info->persist_file);
        HASH_DELETE(hh, feed_map->map, feed_info);
    }
    free(feed_map->slots);
}

static int feed_map_init(struct feed_map *feed_map, struct ctx *ctx)
{
    /* Each configured feed is associated with a struct feed_info object,
     * indexed into feed_map by feed name. */

    struct feed_info *slots;
    unsigned size;
    bool any_enabled = false;

    size = rc_n_feeds(ctx->rc);
    if (size == 0) {
        notice("no feeds defined");
        *feed_map = (struct feed_map){};
        return 0;
    }

    slots = reallocarray(NULL, size, sizeof(*slots));
    if (!slots) {
        warn("reallocarray");
        return -1;
    }

    *feed_map = (struct feed_map){
        .slots = slots,
        .size = size,
    };

    for (unsigned i = 0; i < size; i++) {
        const struct rc_feed *rc_feed;
        struct feed_info *feed_info;
        size_t namelen;

        rc_feed = rc_get_feed(ctx->rc, i);
        namelen = strlen(rc_feed->name);
        HASH_FIND(hh, feed_map->map, rc_feed->name, namelen, feed_info);

        if (feed_info) {
            warnx("Duplicated feed name in configuration: %s", rc_feed->name);
            goto fail;
        }

        feed_info = slots++;
        *feed_info = (struct feed_info){
            .name = rc_feed->name,
            .config = rc_feed,
            .enabled =
                ctx->opts.uid ? !strcmp(rc_feed->name, ctx->opts.uid) : true,
            .global_ctx = ctx,
        };

        any_enabled |= feed_info->enabled;

        HASH_ADD_KEYPTR(hh, feed_map->map, feed_info->name, namelen, feed_info);
    }

    if (!any_enabled) {
        warnx("no feed named %s", ctx->opts.uid);
        goto fail;
    }

    return 0;

fail:
    feed_map_free(feed_map);
    return -1;
}

static int feed_info_set_persist(struct feed_info *feed_info)
{
    struct persist_file *persist_file;

    persist_file =
        persist_file_new(feed_info->global_ctx->persist_dir, feed_info->name);
    if (!persist_file)
        return -1;

    if (persist_file_load(persist_file))
        goto fail;

    if (!persist_file_is_writeable(persist_file))
        goto fail;

    /* The persist_file object we create is not owned by the persist_dir
     * object, thus we have to free it later. */
    feed_info->own_persist = true;
    feed_info->persist_file = persist_file;
    return 0;

fail:
    persist_file_del(persist_file);
    return -1;
}

static void usage(const char *progname)
{
    fprintf(stderr, "Usage: %s [-cDdhqVv] [[-i] identifier]\n", progname);
}

static struct opts read_opts(int argc, char **argv)
{
    int opt;

    struct opts result = {};

    while (opt = getopt(argc, argv, "cDdhi:qVv"), opt != -1)
        switch (opt) {
        case 'c':
            result.catch_up = true;
            break;

        case 'D':
            result.dry_exec = true;
            break;

        case 'd':
            result.dry_mark = true;
            break;

        case 'h':
            usage(argv[0]);
            exit(EXIT_SUCCESS);

        case 'i':
            result.uid = optarg;
            break;

        case 'q':
            g_verbosity_level = 0;
            break;

        case 'V':
            puts(PROJECT_NAME " " PROJECT_VERSION_FULL);
            exit(EXIT_SUCCESS);

        case 'v':
            g_verbosity_level++;
            break;

        default:
            usage(argv[0]);
            exit(EXIT_FAILURE);
        }

    if (!result.uid && optind < argc)
        result.uid = argv[optind++];

    for (int i = optind; i < argc; ++i)
        warnx("Ignoring argument: %s", argv[i]);

    return result;
}

static struct ofmt *setup_ofmt(void)
{
    struct ofmt *ofmt;

    ofmt = ofmt_new();
    if (!ofmt)
        return NULL;

    if (placeholders_setup(ofmt)) {
        ofmt_del(ofmt);
        return NULL;
    }

    return ofmt;
}

static struct persist_dir *setup_persist(const struct rc *rc)
{
    const char *persist_dir_path;
    int basedir;
    struct persist_dir *persist_dir;

    persist_dir_path = rc_get_global(rc)->persist_dir;

    if (mkdir(persist_dir_path, 0755) == -1 && errno != EEXIST)
        warn("mkdir(%s, 0)", persist_dir_path);

    basedir = open(persist_dir_path, O_DIRECTORY | O_RDONLY | O_CLOEXEC);
    if (basedir == -1) {
        warn("open(%s, O_DIRECTORY | O_RDONLY)", persist_dir_path);
        return NULL;
    }

    persist_dir = persist_dir_new(basedir);
    if (persist_dir == NULL && close(basedir) == -1)
        warn("close");
    return persist_dir;
}

static int sync_persist_dir(const struct ctx *ctx, struct feed_map *feed_map)
{
    /* Loop over the persisted feeds on the filesystem:
     * - Index the persist_file object if the corresponding feed exists in
     *   the feed map (that is, the feed is configured in the rc file;.
     * - Unlink the file if the feed map does not contain it (that is, it was
     *   removed from the configuration file).
     */

    void *aux;
    struct persist_file *persist_file;

    aux = NULL;
    debug("synchronize persist_dir");
    while (persist_file = persist_dir_iter(ctx->persist_dir, &aux),
           persist_file) {
        const char *pfname;
        unsigned pfnamelen;
        struct feed_info *feed_info;

        pfname = persist_file_name(persist_file);
        pfnamelen = strlen(pfname);

        HASH_FIND(hh, feed_map->map, pfname, pfnamelen, feed_info);
        debug(" persist_file %s, configured %s, enabled %s",
              pfname,
              feed_info ? "yes" : "no",
              feed_info ? (feed_info->enabled ? "yes" : "no") : "n/a");

        if (feed_info && !feed_info->enabled)
            continue;

        if (persist_file_load(persist_file))
            continue;

        if (!feed_info) {
            debug(" unlinking persist_file %s: not in configuration", pfname);
            if (persist_file_unlink(persist_file))
                warnx("Failed to remove persist_file %s", pfname);
            continue;
        }

        if (!persist_file_is_writeable(persist_file)) {
            warnx("skipping feed %s: cannot handle persist_file %s",
                  feed_info->name,
                  pfname);
            HASH_DELETE(hh, feed_map->map, feed_info);
            continue;
        }

        feed_info->persist_file = persist_file;
    }

    return 0;
}

static EccioParser *get_parser(const struct feed_info *feed_info)
{
    return feed_info->global_ctx->feed_parser;
}

static enum process_status parse_feed(const struct feed_info *feed_info,
                                      int feed_fd)
{
    EccioErr e;
    EccioParser *ep;

    ep = get_parser(feed_info);
    eccio_parser_setopt(ep, ECCIO_OPT_FEEDNAME, feed_info->name);
    e = eccio_parser_load_fd(ep, feed_fd);
    if (e == ECCIO_ERR_OK)
        return ps_success;

    warnx("failed to parse %s (url: %s): %s",
          feed_info->name,
          feed_info->config->url,
          eccio_err_tostr(e));
    return ps_fail_feed;
}

static enum process_status load_outfmt(struct feed_info *feed_info)
{
    static const enum ofmt_mode ofmt_mode_of[] = {
        [rc_hdl_pipe] = ofmt_mode_pipe,
        [rc_hdl_print] = ofmt_mode_print,
        [rc_hdl_exec] = ofmt_mode_exec,
    };
    const struct rc_feed *config = feed_info->config;
    struct ofmt *ofmt = feed_info->global_ctx->ofmt;

    if (ofmt_compile(ofmt,
                     ofmt_mode_of[config->handler],
                     config->ofmt.spec,
                     config->ofmt.len) == 0) {
        feed_info->ofmt_loaded = true;
        return ps_success;
    }

    warnx("failed to interpret the output format of %s", feed_info->name);
    if (info_enabled)
        ofmt_print_error(ofmt);

    if (ofmt_get_error(ofmt)->reason == ofmt_fail_system)
        return ps_fail_critical;

    return ps_fail_feed;
}

static enum process_status process_item_print(
    const struct feed_info *feed_info,
    const EccioEntry *entry,
    const struct placeholder_extra *extra_data)
{
    char buffer[256];
    unsigned int i;
    const char *v;

    printf("ITEM: %u\n", extra_data->entry.incremental_id);
    printf(" uid:          %.*s\n",
           (int)sizeof(extra_data->entry.uid_str),
           extra_data->entry.uid_str);

#define print_if(fmt, val)                                                     \
    do {                                                                       \
        if (val)                                                               \
            printf(fmt, val);                                                  \
    } while (0)

    print_if(" title:        %s\n", entry->title);
    for (i = 0; (v = eccio_entry_get_link(entry, i)); i++)
        printf(" link          %s\n", v);

    print_if(" author.name:  %s\n", entry->author.email);
    print_if(" author.email: %s\n", entry->author.email);
    print_if(" author.uri:   %s\n", entry->author.uri);
    print_if(" comments:     %s\n", entry->comments);
    print_if(" id.value:     %s\n", entry->id.value);
    printf(" id.is_guid:   %s\n", entry->id.is_guid ? "yes" : "no");
    print_if(" pub_date:     %s\n", entry->pub_date);

    for (i = 0; (v = eccio_entry_get_category(entry, i)); i++)
        printf(" category:        %s\n", v);

    print_if(" summary: %s\n",
             text_short(buffer, sizeof(buffer), entry->summary, text_whole));
    print_if(" content: %s\n",
             text_short(buffer, sizeof(buffer), entry->content, text_whole));
#undef print_if

    return ps_success;
}

static enum process_status process_item(
    struct feed_info *feed_info,
    const EccioEntry *entry,
    const struct placeholder_extra *extra_data)
{
    const struct rc_feed *config = feed_info->config;

    if (config->handler == rc_hdl_print && config->ofmt.spec == NULL)
        return process_item_print(feed_info, entry, extra_data);

    if (!feed_info->ofmt_loaded) {
        enum process_status pstatus;

        pstatus = load_outfmt(feed_info);
        if (pstatus != ps_success)
            return pstatus;
    }

    struct ctx *ctx = feed_info->global_ctx;
    placeholders_set_extra(ctx->ofmt, extra_data);

    struct ofmt_evaluate_params params = {
        .item = entry,
        .dry_run = ctx->opts.dry_exec,
        .opt_subproc_chdir = feed_info->config->chdir,
    };
    if (ofmt_evaluate(ctx->ofmt, &params) == 0)
        return ps_success;

    warnx("failed to evaluate the output format of %s", feed_info->name);
    if (info_enabled)
        ofmt_print_error(ctx->ofmt);

    return ps_fail_item;
}

static int item_digest(struct digest *digest,
                       const EccioEntry *entry,
                       struct placeholder_extra *phextra)
{
    int e = -1;
    const char *value;

    if (digest_add_cstr(digest, entry->title))
        goto cleanup;

    if (digest_add_cstr(digest, entry->id.value))
        goto cleanup;

    for (unsigned int i = 0; (value = eccio_entry_get_link(entry, i)); i++)
        if (digest_add_cstr(digest, value))
            goto cleanup;

    /* TODO: maybe more fields?
     * Or allow the user to select the fields that should participate
     * in the digest (by configuration, for individual feeds).
     */

    if (digest_add_cstr(digest, entry->pub_date))
        goto cleanup;

    if (digest_finish(digest, phextra->entry.uid, sizeof phextra->entry.uid))
        goto cleanup;

    to_hex(phextra->entry.uid_str,
           sizeof phextra->entry.uid_str,
           phextra->entry.uid,
           sizeof phextra->entry.uid);

    e = 0;
cleanup:
    digest_reset(digest);
    return e;
}

static void debug_print_old_items(const struct itemset *old_items)
{
    const void *aux = NULL;
    const uint8_t *data;
    uint16_t data_len;
    char *repr = NULL;
    size_t repr_len = 0;

    if (!debug_enabled)
        return;

    warnx(" loaded from persist_file: %hu items", itemset_get_size(old_items));
    while (itemset_iter(old_items, &aux, &data, &data_len)) {
        size_t repr_len_needed = data_len * 2 + 1;

        if (repr_len < repr_len_needed) {
            char *new_repr;

            new_repr = realloc(repr, repr_len_needed);
            if (!new_repr) {
                warnx("malloc");
                goto exit;
            }

            repr = new_repr;
            repr_len = repr_len_needed;
        }

        debug("   item guid \"%s\"", to_hex(repr, repr_len, data, data_len));
    }

exit:
    free(repr);
}

static enum process_status process_feed(struct feed_info *feed_info,
                                        int feed_fd)
{
    enum process_status pstatus;
    struct placeholder_extra phextra;
    const EccioEntry *entry;
    bool catch_up = feed_info->global_ctx->opts.catch_up;
    struct digest *digest = feed_info->global_ctx->digest;
    struct itemset *old_items = NULL, *new_items = NULL;
    unsigned old_news = 0, new_news = 0;
    EccioParser *parser = get_parser(feed_info);

    info("processing %s", feed_info->name);

    new_items = itemset_new();
    if (!new_items) {
        pstatus = ps_fail_critical;
        goto exit;
    }

    /* If old_items is NULL, this feed is processed for the first time. */
    old_items = persist_file_swap_items(feed_info->persist_file, NULL);

    if (old_items)
        debug_print_old_items(old_items);

    debug(" loading from feed file");
    pstatus = parse_feed(feed_info, feed_fd);
    if (pstatus != ps_success)
        goto exit;

    phextra = (struct placeholder_extra){
        .entry.incremental_id =
            persist_file_get_incrid(feed_info->persist_file),
        .feed_title = eccio_parser_get_feed(parser)->title,
        .feed_identifier = feed_info->name,
    };

    for (unsigned int i = 0; (entry = eccio_parser_get_entry(parser, i)); i++) {
        bool seen_before;

        if (item_digest(digest, entry, &phextra)) {
            warnx("Could not compute digest while handling %s",
                  feed_info->name);
            continue;
        }

        seen_before = old_items && itemset_exists(old_items,
                                                  phextra.entry.uid,
                                                  sizeof phextra.entry.uid);
        debug(" %c item guid \"%s\"",
              seen_before ? '=' : '+',
              phextra.entry.uid_str);

        if (seen_before)
            old_news++;
        else
            new_news++;

        if (!seen_before) {
            pstatus = catch_up ? ps_success
                               : process_item(feed_info, entry, &phextra);
            phextra.entry.incremental_id++;

            switch (pstatus) {
            case ps_success:
                break;
            case ps_fail_critical:
            case ps_fail_feed:
                goto exit;
            case ps_fail_item:
                feed_info->global_ctx->failures++;
                continue;
            }
        }

        switch (itemset_add(
            new_items, phextra.entry.uid, sizeof phextra.entry.uid)) {
        case itemset_edup:
            notice("Duplicated item in feed %s: %s",
                   feed_info->name,
                   phextra.entry.uid_str);
            break;
        case itemset_emax:
            notice("Too many entries in feed %s", feed_info->name);
            break;
        case itemset_eok:
            break;
        case itemset_esys:
            pstatus = ps_fail_critical;
            goto exit;
        }
    }

    info("feed %s: old_news=%u new_news=%u",
         feed_info->name,
         old_news,
         new_news);

    /* Transfer ownership of new_items to the persist_file */
    (void)persist_file_swap_items(feed_info->persist_file, new_items);
    new_items = NULL;

    debug(" incremental id is %u", phextra.entry.incremental_id);
    persist_file_set_incrid(feed_info->persist_file,
                            phextra.entry.incremental_id);

    bool skip_write = feed_info->global_ctx->opts.dry_mark;
    if (!skip_write && persist_file_write(feed_info->persist_file)) {
        pstatus = ps_fail_feed;
        goto exit;
    }

    pstatus = ps_success;
exit:
    itemset_del(old_items);
    itemset_del(new_items);

    return pstatus;
}

static int on_downloaded(void *opaque,
                         enum download_status dstatus,
                         const char *errmsg,
                         int feed_fd)
{
    struct feed_info *feed_info = opaque;
    enum process_status pstatus = ps_fail_feed;

    switch (dstatus) {
    case dl_complete:
        if (feed_fd == -1)
            panic(); // bad fd with dl_complete
        pstatus = process_feed(feed_info, feed_fd);
        break;

    case dl_sys_failure:
        pstatus = ps_fail_critical;
    case dl_fetch_failure:
        notice("failed to download %s: %s", feed_info->name, errmsg);
        break;

    case dl_aborted:
        notice("aborted download of %s (url: %s)",
               feed_info->name,
               feed_info->config->url);
        break;
    }

    if (pstatus == ps_fail_feed)
        feed_info->global_ctx->failures++;

    /* Only (err == -1) is going to interrupt all the downloads. */
    return pstatus == ps_fail_critical ? -1 : 0;
}

static int schedule_feed_download(struct feed_info *feed_info)
{
    debug("schedule retrieval of url %s for %s",
          feed_info->config->url,
          feed_info->name);
    return download_schedule(feed_info->global_ctx->dload,
                             feed_info->config->url,
                             on_downloaded,
                             feed_info);
}

static int run(struct ctx *ctx)
{
    int e = -1;
    struct feed_map feed_map;

    if (feed_map_init(&feed_map, ctx))
        return -1;

    if (sync_persist_dir(ctx, &feed_map))
        goto exit;

    for (unsigned i = 0; i < feed_map.size; ++i) {
        struct feed_info *feed_info = &feed_map.slots[i];

        if (!feed_info->enabled)
            continue;

        if (!feed_info->persist_file) {
            /* We do not have a persist_file yet.  This happens when we
             * handle a feed for the first time: the feed is listed in the
             * configuration, but the persist_dir did not yield a
             * persist_file for it.
             */
            if (feed_info_set_persist(feed_info)) {
                notice("skipping %s", feed_info->name);
                continue;
            }
        }
        info("scheduling fetch for %s", feed_info->name);

        if (schedule_feed_download(feed_info))
            goto exit;
    }

    if (download_perform(ctx->dload))
        goto exit;

    e = ctx->failures ? -1 : 0;
exit:
    download_abort(ctx->dload);
    feed_map_free(&feed_map);
    return e;
}

int main(int argc, char **argv)
{
    int e = -1;
    struct ctx ctx = {
        .opts = read_opts(argc, argv),
    };

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath wpath cpath inet dns proc exec", NULL) == -1)
        err(EXIT_FAILURE, "pledge");
#endif

    ctx.rc = rc_new();
    if (!ctx.rc)
        goto exit;

    if (rc_load(ctx.rc))
        goto exit;

    ctx.dload = download_new(true, rc_get_global(ctx.rc)->jobs);
    if (!ctx.dload)
        goto exit;

    ctx.ofmt = setup_ofmt();
    if (!ctx.ofmt)
        goto exit;

    ctx.persist_dir = setup_persist(ctx.rc);
    if (!ctx.persist_dir)
        goto exit;

    ctx.digest = digest_new();
    if (!ctx.digest)
        goto exit;

    ctx.feed_parser = eccio_parser_new();
    if (!ctx.feed_parser)
        goto exit;

    e = eccio_parser_setopt(
        ctx.feed_parser, ECCIO_OPT_ERRCALLBACK, parser_error_handler, &ctx);
    if (e) {
        warnx("eccio_parser_setopt: %s", eccio_err_tostr(e));
        goto exit;
    }

    e = run(&ctx);
exit:
    digest_del(ctx.digest);
    eccio_parser_del(ctx.feed_parser);
    persist_dir_del(ctx.persist_dir);
    ofmt_del(ctx.ofmt);
    download_del(ctx.dload);
    rc_free(ctx.rc);
    return e ? EXIT_FAILURE : EXIT_SUCCESS;
}
