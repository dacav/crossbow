#pragma once

#include <stdbool.h>

#include "persist_dir.h"

/* Abstract view of a persistence file.
 *
 * The persist_file type provides an abstraction around the storage of feed
 * information, such as the set of seen items (feed entries).  The abstraction
 * covers the file format and filesystem operations.
 *
 * Each persist_file object is bound to a file on the filesystem.
 *
 * Each persist_file object may own a set of items, that is an object of type
 * persist_items.
 */
struct persist_file;

/* Construct a persist_file object.
 *
 * The constructed object is going to be bound to file having name `filename`.
 * The file is going to be positioned under the directory referred by the `pd`
 * parameter.
 */
struct persist_file *persist_file_new(struct persist_dir *pd,
                                      const char *filename);

const char *persist_file_name(const struct persist_file *);

enum persist_file_status {
    pfs_unknown = 0,
    pfs_noent,        /* file does not exist yet */
    pfs_regular,      /* file exists */
    pfs_unrecognized, /* file exists, format not recognized */
    pfs_incompat,     /* file exists, recognized, different version */
    pfs_corrupt,      /* file exists, recognized, same version, corrupt */
};

bool persist_file_is_writeable(const struct persist_file *);

int persist_file_write(const struct persist_file *);

int persist_file_unlink(const struct persist_file *);

/* Load the tracked persistence file.
 *
 * The loaded file is referenced by the constructor parameters.
 *
 * Once loaded, the file will own a new set of items.
 * If the persist_file object already owned a set of items, such set is
 * destroyed first.
 *
 * The items can be obtained using persist_file_get_items or
 * persist_file_swap_items.
 */
int persist_file_load(struct persist_file *);

enum persist_file_status persist_file_status(const struct persist_file *);

unsigned persist_file_get_incrid(const struct persist_file *);

void persist_file_set_incrid(struct persist_file *, unsigned v);

/* Get a constant pointer to the inner set of items.
 *
 * The returned items are still owned by the persist_file object.
 */
struct itemset;
const struct itemset *persist_file_get_items(const struct persist_file *);

/* Swap the inner set of items with a new set.
 *
 * The returned items are no longer owned by the persist_file object.
 *
 * It is legal to pass a NULL as second parameter: if so, the persist_file
 * object will no longer own a set of items.
 */
struct itemset *persist_file_swap_items(struct persist_file *,
                                        struct itemset *);

/* Destroy a persist_file object.
 *
 * If the object owns a set of items, the set is destroyed.
 */
void persist_file_del(struct persist_file *);
