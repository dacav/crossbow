#include "filemap.h"

#include <err.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

static int filesize(int fd, size_t *s)
{
    struct stat sb;

    if (fstat(fd, &sb) == -1) {
        warn("fstat failed");
        return -1;
    }

    *s = sb.st_size;
    return 0;
}

int filemap_load_fd(struct filemap *fmap, int fd)
{
    size_t size;
    void *mapped;

    if (filesize(fd, &size) == -1)
        return -1;

    *fmap = (struct filemap){};
    if (size == 0)
        /* An empty file is not an error.  It is handled as there
         * was no file in the first place */
        goto success;

    mapped = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (mapped == MAP_FAILED) {
        warn("mmap failed");
        return -1;
    }

    *fmap = (struct filemap){
        .memory = mapped,
        .size = size,
    };

success:
    close(fd);
    return 0;
}

int filemap_load_path(struct filemap *fmap, const char *fname)
{
    int fd;

    fd = open(fname, O_RDONLY | O_CLOEXEC);
    if (fd == -1) {
        warn("open(%s, ...)", fname);
        return -1;
    }

    if (filemap_load_fd(fmap, fd)) {
        close(fd);
        return -1;
    }

    return 0;
}

void filemap_unload(struct filemap *fmap)
{
    if (fmap->memory == MAP_FAILED || !fmap->memory)
        return;

    munmap(fmap->memory, fmap->size);
    *fmap = (struct filemap){};
}
