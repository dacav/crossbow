#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "launcher.h"
#include "outfmt_parser.h"
#include "radix.h"

struct ofmt;

struct ofmt *ofmt_new(void);

void ofmt_set_opaque(struct ofmt *, void *opaque, void (*opaque_free)(void *));
void *ofmt_get_opaque(const struct ofmt *);

typedef const char *(*ofmt_resolver_callback)(const void *item, void *opaque);
int ofmt_set_resolver(struct ofmt *, const char *key, ofmt_resolver_callback);
int ofmt_set_pipe_resolver(struct ofmt *, ofmt_resolver_callback);

enum ofmt_mode {
    ofmt_mode_exec,
    ofmt_mode_pipe,
    ofmt_mode_print,
};

int ofmt_compile(struct ofmt *,
                 enum ofmt_mode,
                 const char *spec,
                 size_t speclen);

struct ofmt_evaluate_params {
    const void *item;
    const int *opt_print_fd;
    const char *opt_subproc_chdir;
    bool dry_run;
};

int ofmt_evaluate(struct ofmt *, const struct ofmt_evaluate_params *);

enum ofmt_fail {
    ofmt_fail_none = 0,

    ofmt_fail_bad_setup,
    ofmt_fail_bad_state,
    ofmt_fail_invalid_pholder,
    ofmt_fail_parse,
    ofmt_fail_subproc,
    ofmt_fail_system,
    ofmt_fail_too_long_evaluation,
    ofmt_fail_too_long_spec,
    ofmt_fail_too_many_args,
    ofmt_fail_too_many_pholders,
    ofmt_fail_undefined_resolver,
};

const char *ofmt_fail_str(enum ofmt_fail);

struct ofmt_err {
    enum ofmt_fail reason;
    union {
        enum ofp_fail ofp_reason; /* when reason == ofmt_fail_parse */
        int errno_val;            /* when reason == ofmt_fail_system */
        struct {                  /* when reason == ofmt_fail_subproc */
            const char *argv0;
            int exit_status;
        } subproc;
    };
};

const struct ofmt_err *ofmt_get_error(const struct ofmt *);

void ofmt_print_error(const struct ofmt *);

void ofmt_del(struct ofmt *);
