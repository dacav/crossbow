#pragma once

#include <stdbool.h>

struct download;

enum download_status {
    dl_complete,
    dl_aborted,
    dl_fetch_failure,
    dl_sys_failure,
};

/* download_callback - download callback.
 *
 * If download_status == dl_complete, then the feed_fd parameter is an open
 * file descriptor.
 * By reading it the callback can access the downloaded content.
 * The callback is responsible for closing the file descriptor.
 *
 * Expected to return 0 on success.  If the return value is not 0 all
 * downloading processes will be interrupted.
 */
typedef int (*download_callback)(void *opaque,
                                 enum download_status,
                                 const char *errmsg,
                                 int feed_fd);

struct download *download_new(bool global_init, unsigned maxjobs);

int download_schedule(struct download *,
                      const char *url,
                      download_callback,
                      void *opaque);

int download_perform(struct download *);

void download_abort(struct download *);

void download_del(struct download *);
