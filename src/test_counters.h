#pragma once

/* This module provides instrumentation to embed in source modules for
 * sake of testing.  The definitions are enabled only if the TESTING
 * preprocessor symbol is defined, which only happens when compiling with
 * unit tests.  Regular binaries are not affected.
 *
 * It is important to enable "Per-Object flags emulation", as specified by
 * the Automake manual in order to avoid to accidentally enable
 * instrumentation in the distributed binaries.
 */
#ifdef TESTING

void test_reset_counters();
const unsigned test_read_counter(const char *name);

#define test_counter(name)                                                     \
    do {                                                                       \
        unsigned *test_next_counter(const char *);                             \
        static unsigned *cnt = NULL;                                           \
        if (!cnt)                                                              \
            cnt = test_next_counter(#name);                                    \
        ++(*cnt);                                                              \
    } while (0)

#else /* ifdef TESTING */

#define test_counter(...)
#define test_reset_counters(...)

#endif /* ifdef TESTING */
