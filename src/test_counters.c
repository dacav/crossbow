#include "test_counters.h"

#include <err.h>
#include <string.h>
#include <sysexits.h>

enum {
    test_max_counters = 30,
};

struct test_counter {
    const char *name;
    unsigned value;
};

struct test_counter;

static struct test_counter test_counters[test_max_counters];
static unsigned next_counter;

unsigned *test_next_counter(const char *name)
{
    struct test_counter *out;

    if (next_counter >= test_max_counters)
        errx(1, "too many test counters");

    out = &test_counters[next_counter++];
    out->name = name;

    return &out->value; /* already zero, because it's in the BSS */
}

const unsigned test_read_counter(const char *name)
{
    for (int i = 0; i < next_counter; ++i)
        if (strcmp(test_counters[i].name, name) == 0)
            return test_counters[i].value;

    return 0;
}

void test_reset_counters(void)
{
    for (int i = 0; i < next_counter; ++i)
        test_counters[i].value = 0;
}
