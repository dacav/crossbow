#pragma once

#include <err.h>
#include <stdlib.h>
#include <sysexits.h>

#ifdef TESTING
#define PROJECT_VERSION_FULL "unit-test"
#else
#include "config.h"
#endif

#define panic()                                                                \
    do {                                                                       \
        errx(EX_SOFTWARE,                                                      \
             "FATAL: r:%s f:%s l:%d\n",                                        \
             PROJECT_VERSION_FULL,                                             \
             __func__,                                                         \
             __LINE__);                                                        \
    } while (0);

#define panic_info(...)                                                        \
    do {                                                                       \
        warnx("FATAL: " __VA_ARGS__);                                          \
        panic()                                                                \
    } while (0)

const char *to_hex(char *dst, size_t dst_len, const void *src, size_t src_len);
