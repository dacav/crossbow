#pragma once

#include <stddef.h>

#define text_whole ((size_t)-1)

/* text_short()
 *
 * @dst         The destination buffer
 * @dst_len     Length in bytes of the destination buffer
 * @text        Original text
 * @text_len    Maximum number of bytes to read from @text, or @text_whole if
 *              @text should be parsed until NUL is found.
 *
 * Given @text, construct a modified copy of it on the destination buffer @dst.
 *
 * FIXME: Broken unless encoding is ASCII!
 *
 * The modified copy has the following characteristics:
 * - Text is copied until either the NUL character is found, or @text_len bytes
 *   have been copied.
 * - CR and LF characters are replaced with two SPACE characters.
 * - Repeated whitespaces, as by isspace(3), are folded into a single SPACE
 *   character;
 * - Non-printable characters are replaced with '.'
 * - If @text is truncated, the last three characters are
 * - The resulting string is always terminated with a NUL character.
 *
 * This function returns a pointer to @dst, or to a static constant NUL
 * character (serving as empty string) in special corner cases.
 */
const char *text_short(char *dst,
                       size_t dst_len,
                       const char *text,
                       size_t text_len);

const char *text_join_argv(char *dst, size_t dst_size, const char *const *argv);
