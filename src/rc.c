#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "filemap.h"
#include "rc.h"
#include "test_counters.h"
#include "util.h"

#define RC_EXPECTED_COMPAT ((unsigned short)1)

struct rc {
    struct rc_global global;
    struct rc_feed *feeds;
    unsigned nfeeds;

    /* This field may hold a pointer to global.persist_dir.
     * We keep a copy so we can free it if we own the value.
     * If it is NULL, we don't own it
     */
    void *persist_dir_buffer;
};

struct feeds_array {
    struct rc_feed *feeds;
    unsigned room, next;
};

struct feed_state {
    struct rc_feed *cur_feed;
    struct {
        bool uses_command : 1;
        bool uses_format : 1;
        bool defines_handler : 1;
    } flags;
};

struct parse_data {
    unsigned short rc_compat;
    struct rc *rc;
    struct feeds_array feeds_array;
    struct feed_state feed_state;
};

struct token {
    const char *bytes;
    int len;
};

enum {
    uses_command,
    uses_format,
    defines_handler,
};

static const char *empty_value = "";

struct rc *rc_new(void)
{
    struct rc *rc;

    rc = malloc(sizeof(*rc));
    if (!rc) {
        warn("malloc");
        return NULL;
    }

    *rc = (struct rc){};
    return rc;
}

static void free_value(const char *value)
{
    if (value != NULL && value != empty_value)
        free((void *)value);
}

static void free_feed(struct rc_feed *feed)
{
    free_value(feed->name);
    free_value(feed->url);
    free_value(feed->ofmt.spec);
    free_value(feed->chdir);
}

static void free_feeds(struct rc_feed *feeds, unsigned n)
{
    if (!feeds)
        return;

    for (unsigned int i = 0; i < n; i++)
        free_feed(&feeds[i]);
    free(feeds);
}

static int reject_feed(const struct rc_feed *feed, const char *reason)
{
    warnx("invalid feed '%s': %s", feed->name, reason);
    return -1;
}

static void reset(struct rc *rc)
{
    free_feeds(rc->feeds, rc->nfeeds);
    free(rc->persist_dir_buffer);

    *rc = (struct rc){
        .global =
            {
                .jobs = 10L,
            },
    };
}

static int load_ushort(unsigned short *dst, const struct token *value)
{
    char *endptr;
    unsigned long result;

    errno = 0;
    result = strtoul(value->bytes, &endptr, 10);

    if (errno)
        warn("invalid short value: '%.*s'", value->len, value->bytes);
    else if (endptr - value->bytes != value->len)
        warnx("invalid short value: '%.*s'", value->len, value->bytes);
    else if (result > USHRT_MAX)
        warnx("Value too big: %lu, max is %hu",
              result,
              (unsigned short)USHRT_MAX);
    else {
        *dst = result;
        return 0;
    }

    return -1;
}

static int load_str(const char **dst, const struct token *value)
{
    free_value(*dst);

    if (value->len <= 0) {
        *dst = empty_value;
        return 0;
    }

    *dst = strndup(value->bytes, value->len);
    if (*dst)
        return 0;

    warn("strndup");
    return -1;
}

static int load_path(const char **path, const struct token *value)
{
    const char *home;
    char *cursor;
    size_t home_len;

    if (value->len < 2 || value->bytes[0] != '~' || value->bytes[1] != '/')
        return load_str(path, value);

    home = getenv("HOME");
    if (!home) {
        warnx("NOTE: no $HOME env available, cannot expand ~ in %.*s",
              value->len,
              value->bytes);
        return load_str(path, value);
    }

    /* Reserved space: strlen(home), minus one for the replacement of '~', plus
     * the length of the path, plus 1 for the null terminator.  So basically
     * just strlen(home) + value->len. */
    home_len = strlen(home);
    cursor = malloc(home_len + value->len);
    if (!cursor) {
        warn("malloc");
        return -1;
    }
    *path = cursor;
    memcpy(cursor, home, home_len);
    cursor += home_len;
    memcpy(cursor, value->bytes + 1, value->len - 1);
    cursor += value->len - 1;
    *cursor = '\0';
    return 0;
}

static int load_persist_dir(struct parse_data *pdata, const struct token *value)
{
    struct rc *rc = pdata->rc;
    const char *pdir = NULL;

    if (load_path(&pdir, value))
        return -1;

    rc->global.persist_dir = pdir;
    free(rc->persist_dir_buffer);
    rc->persist_dir_buffer = (pdir == empty_value) ? NULL : (char *)pdir;
    return 0;
}

static int load_ofmt(struct rc_feed *feed, const struct token *value)
{
    if (load_str(&feed->ofmt.spec, value))
        return -1;

    feed->ofmt.len = value->len;
    return 0;
}

#define matches(k, v) !strncmp((k)->bytes, (v), (k)->len)

static int load_handler(enum rc_handler *dst, const struct token *value)
{
    if (value->len <= 0)
        goto reject;

    switch (value->bytes[0]) {
    case 'e':
        if (matches(value, "exec")) {
            *dst = rc_hdl_exec;
            return 0;
        }
        break;
    case 'p':
        if (matches(value, "pipe")) {
            *dst = rc_hdl_pipe;
            return 0;
        }
        if (matches(value, "print")) {
            *dst = rc_hdl_print;
            return 0;
        }
        break;
    }

reject:
    warnx("invalid handler: '%.*s'", value->len, value->bytes);
    return -1;
}

static int finalize_feed(struct feed_state *fstate)
{
    struct rc_feed *feed;

    feed = fstate->cur_feed;
    if (!feed)
        return 0;
    test_counter(count_finalized_feeds);

    if (fstate->flags.uses_format && fstate->flags.uses_command) {
        test_counter(fail_both_command_and_format);
        return reject_feed(feed, "using both 'format' and 'command'");
    }

    if (!fstate->flags.defines_handler && fstate->flags.uses_command)
        feed->handler = rc_hdl_exec;

    if (strchr(feed->name, '/')) {
        test_counter(bad_feed_name);
        return reject_feed(feed, "invalid feed name, contains '/'");
    }

    if (!feed->url) {
        test_counter(missing_url);
        return reject_feed(feed, "no url specified");
    }

    if (!feed->ofmt.spec)
        switch (feed->handler) {
        case rc_hdl_pipe:
            test_counter(missing_ofmt_spec_pipe);
            return reject_feed(
                feed, "output mode 'pipe' requires 'command' to be defined");
        case rc_hdl_exec:
            test_counter(missing_ofmt_spec_exec);
            return reject_feed(
                feed, "output mode 'exec' requires 'command' to be defined");
        case rc_hdl_print:
            /* Optional, falls back to a hard-wired output format. */
            break;
        }

    if (feed->chdir && feed->handler == rc_hdl_print)
        warnx("in feed '%s', output mode 'print' ignores provided 'chdir'",
              feed->name);

    return 0;
}

static int new_feed(struct parse_data *pdata, const struct token *value)
{
    struct feeds_array *array;
    struct rc_feed *feed;

    if (finalize_feed(&pdata->feed_state))
        return -1;

    array = &pdata->feeds_array;
    if (array->next >= array->room) {
        unsigned new_room;
        struct rc_feed *new_feeds;

        new_room = 2 * (array->room ?: 2);
        new_feeds =
            reallocarray(array->feeds, new_room, sizeof(struct rc_feed));

        if (!new_feeds) {
            warn("reallocarray");
            return -1;
        }

        array->feeds = new_feeds;
        array->room = new_room;
    }
    feed = &array->feeds[array->next++];

    *feed = (struct rc_feed){
        .name = strndup(value->bytes, value->len),
    };

    pdata->feed_state = (struct feed_state){
        .cur_feed = feed,
    };
    return 0;
}

static int fail_unmatched(const char *kind, const struct token *key)
{
    warnx("unmatched %s configuration key: '%.*s'", kind, key->len, key->bytes);
    return -1;
}

static int load_global_opt(struct parse_data *pdata,
                           const struct token *key,
                           const struct token *value)
{
    switch (key->bytes[0]) {
    case 'c':
        if (matches(key, "compat"))
            return load_ushort(&pdata->rc_compat, value);
        break;
    case 'j':
        if (matches(key, "jobs"))
            return load_ushort(&pdata->rc->global.jobs, value);
        break;
    case 'p':
        if (matches(key, "persist_dir"))
            return load_persist_dir(pdata, value);
        break;
    }

    test_counter(fail_unmatched_global_opt);
    return fail_unmatched("global", key);
}

static int verify_rc_compat(const struct parse_data *pdata)
{
    if (pdata->rc_compat == RC_EXPECTED_COMPAT)
        return 0;

    warnx("Incompatible config file (expected 'compat %hu'). "
          "See crossbow.conf(5)",
          RC_EXPECTED_COMPAT);
    return -1;
}

static int mark(struct parse_data *pdata, int flag)
{
    switch (flag) {
    case uses_command:
        pdata->feed_state.flags.uses_command = true;
        break;
    case uses_format:
        pdata->feed_state.flags.uses_format = true;
        break;
    case defines_handler:
        pdata->feed_state.flags.defines_handler = true;
        break;
    default:
        panic_info("bad flag %d", flag);
    }
    return 0;
};

static int load_feed_opt(struct parse_data *pdata,
                         const struct token *key,
                         const struct token *value)
{
    struct rc_feed *feed = pdata->feed_state.cur_feed;

    switch (key->bytes[0]) {
    case 'c':
        if (matches(key, "chdir"))
            return load_path(&feed->chdir, value);
        if (matches(key, "command"))
            return load_ofmt(feed, value) || mark(pdata, uses_command);
        break;
    case 'f':
        if (matches(key, "format"))
            return load_ofmt(feed, value) || mark(pdata, uses_format);
        break;
    case 'h':
        if (matches(key, "handler"))
            return load_handler(&feed->handler, value) ||
                   mark(pdata, defines_handler);
        break;
    case 'u':
        if (matches(key, "url"))
            return load_str(&feed->url, value);
        break;
    }

    test_counter(fail_unmatched_feed_opt);
    return fail_unmatched("feed", key);
}

static int register_pair(struct parse_data *pdata,
                         const struct token *key,
                         const struct token *value)
{
    if (matches(key, "feed"))
        return new_feed(pdata, value);

    if (pdata->feed_state.cur_feed)
        return load_feed_opt(pdata, key, value);
    else
        return load_global_opt(pdata, key, value);
}

static int parse_config(struct rc *rc, const char *config, size_t len)
{
    enum state {
        st_void,
        st_comment,
        st_key,
        st_break,
        st_value,
    };

    enum state state = st_void;
    struct token key = {0}, value;
    struct parse_data pdata = {
        .rc = rc,
    };

    for (size_t i = 0; i < len; i++) {
        switch (state) {
        case st_void:
            if (isspace((unsigned char)config[i]))
                continue;

            if (config[i] == '#')
                state = st_comment;
            else {
                state = st_key;
                key.bytes = &config[i];
            }
            continue;

        case st_comment:
            if (config[i] != '\n')
                continue;
            state = st_void;
            break;

        case st_key:
            if (!isspace((unsigned char)config[i]))
                continue;
            state = st_break;
            key.len = &config[i] - key.bytes;
            continue;

        case st_break:
            state = st_value;
            value.bytes = &config[i];
            continue;

        case st_value:
            if (config[i] != '\n')
                continue;
            value.len = &config[i] - value.bytes;
            if (register_pair(&pdata, &key, &value))
                goto fail;
            state = st_void;
        }
    }

    if (verify_rc_compat(&pdata) != 0)
        goto fail;

    if (state == st_value && register_pair(&pdata, &key, &value))
        goto fail;

    if (finalize_feed(&pdata.feed_state))
        goto fail;

    rc->feeds = pdata.feeds_array.feeds;
    rc->nfeeds = pdata.feeds_array.next;
    return 0;

fail:
    free_feeds(pdata.feeds_array.feeds, pdata.feeds_array.next);
    return -1;
}

int rc_parse_buffer(struct rc *rc, const char *str, size_t len)
{
    reset(rc);

    if (parse_config(rc, str, len))
        return -1;

    return 0;
}

static int open_rcfile(const char *home)
{
    char path[PATH_MAX];
    int fd;
    const char *xdg_config_home;

    snprintf(path, sizeof(path), "%s/.crossbow.conf", home);
    fd = open(path, O_RDONLY | O_CLOEXEC);
    if (fd != -1)
        return fd;

    if (errno != ENOENT) {
        warn("open(%s, O_RDONLY | O_CLOEXEC)", path);
        return -1;
    }

    xdg_config_home = getenv("XDG_CONFIG_HOME");
    if (xdg_config_home && *xdg_config_home)
        snprintf(path, sizeof(path), "%s/crossbow.conf", xdg_config_home);
    else
        snprintf(path, sizeof(path), "%s/.config/crossbow.conf", home);

    fd = open(path, O_RDONLY | O_CLOEXEC);
    if (fd != -1)
        return fd;

    if (errno == ENOENT)
        warnx("no configuration file available");
    else
        warn("open(%s, O_RDONLY | O_CLOEXEC)", path);
    return -1;
}

static char *default_persist_dir(const char *home)
{
    char path[PATH_MAX];
    char *out;

    /* Could probably use the non-standard asprintf(3) */
    snprintf(path, sizeof(path), "%s/.crossbow", home);
    out = strdup(path);
    if (!out)
        warn("strdup");
    return out;
}

static int load_defaults(struct rc *rc, const char *home)
{
    char *pdir;

    pdir = default_persist_dir(home);
    if (!pdir)
        return -1;

    assert(!rc->persist_dir_buffer);
    rc->global.persist_dir = pdir;
    rc->persist_dir_buffer = pdir;
    return 0;
}

int rc_load(struct rc *rc)
{
    int rcfile, e = -1;
    const char *home;
    struct filemap filemap = {};

    home = getenv("HOME");
    if (!home || !*home) {
        warnx("undefined env: $HOME");
        return -1;
    }

    rcfile = open_rcfile(home);
    if (rcfile == -1)
        goto fail;

    if (filemap_load_fd(&filemap, rcfile))
        goto fail;
    rcfile = -1; /* ownership moved */

    if (rc_parse_buffer(rc, filemap.memory, filemap.size))
        goto fail;

    if (!rc->global.persist_dir) {
        if (load_defaults(rc, home))
            goto fail;
    }

    e = 0;
fail:
    if (rcfile != -1 && close(rcfile))
        warn("close rcfile");
    filemap_unload(&filemap);
    return e;
}

const struct rc_global *rc_get_global(const struct rc *rc)
{
    return &rc->global;
}

const struct rc_feed *rc_get_feed(const struct rc *rc, unsigned idx)
{
    if (idx >= rc->nfeeds)
        return NULL;

    return &rc->feeds[idx];
}

unsigned rc_n_feeds(const struct rc *rc)
{
    return rc->nfeeds;
}

void rc_dump(const struct rc *rc)
{
    warnx("jobs %d", rc->global.jobs);
    warnx("persist_dir %s", rc->global.persist_dir);

    for (unsigned i = 0; i < rc->nfeeds; ++i) {
        warnx(" %u.name %s", i, rc->feeds[i].name);
        warnx(" %u.handler %d", i, rc->feeds[i].handler);
    }
}

void rc_free(struct rc *rc)
{
    if (!rc)
        return;

    reset(rc);
    free(rc);
}
