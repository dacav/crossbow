#include "persist_file.h"

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sysexits.h>
#include <unistd.h>

#include "filemap.h"
#include "itemset.h"
#include "logging.h"
#include "persist_dir.h"
#include "util.h"

#include <utlist.h>

/* file format:
 *
 * 1. magic (compatible with old format).
 *      "crossbow" + magic_revision
 * 2. size of array (byte-aligned unsigned short).
 * 3. sequence of length+data items
 */

typedef uint8_t revision_t;
typedef unsigned short aushort_t __attribute__((aligned(1)));

struct persist_path {
    char *buffer;
};

struct persist_file {
    struct persist_dir *pd;
    struct persist_path path;

    enum persist_file_status file_status;
    struct itemset *items;
    unsigned incrid;
};

enum parse_status {
    ps_fatal = -1,
    ps_stop = 0,
    ps_continue = 1,
};

#define magic_nr "crossbow"
enum {
    magic_revision = 1,
    magic_nr_len = sizeof(magic_nr) - 1,
};

static const struct {
    const char *status_name;
    bool can_write;
} status_info[] = {
    [pfs_unknown] =
        {
            .status_name = "unknown",
            .can_write = false,
        },
    [pfs_noent] =
        {
            .status_name = "noent",
            .can_write = true,
        },
    [pfs_regular] =
        {
            .status_name = "regular",
            .can_write = true,
        },
    [pfs_unrecognized] =
        {
            .status_name = "unrecognized",
            .can_write = false,
        },
    [pfs_incompat] =
        {
            .status_name = "incompat",
            .can_write = false,
        },
    [pfs_corrupt] =
        {
            .status_name = "corrupt",
            .can_write = true,
        },
};

static struct persist_path persist_path_new(const char *filename)
{
    size_t filename_len;
    char *buffer;

    filename_len = strlen(filename) + 1;
    buffer = malloc(1 + filename_len);
    if (buffer) {
        buffer[0] = '.';
        memcpy(buffer + 1, filename, filename_len);
    } else
        warn("malloc");

    return (struct persist_path){
        .buffer = buffer,
    };
}

static bool persist_path_valid(const struct persist_path *pp)
{
    return pp->buffer != NULL;
}

static void persist_path_free(struct persist_path *pp)
{
    free(pp->buffer);
}

static const char *persist_path_main(const struct persist_path *pp)
{
    return pp->buffer + 1;
}

static const char *persist_path_pivot(const struct persist_path *pp)
{
    return pp->buffer;
}

static bool has_room_for(const struct filemap *fmap,
                         off_t r_off,
                         size_t required)
{
    return fmap->size - r_off >= required;
}

static enum parse_status stop(struct persist_file *pf,
                              enum persist_file_status status)
{
    pf->file_status = status;
    return ps_stop;
}

static enum parse_status parse_magic(struct persist_file *pf,
                                     const struct filemap *fmap,
                                     off_t *r_off)
{
    const revision_t *rptr;

    if (!has_room_for(fmap, *r_off, magic_nr_len + sizeof(revision_t)))
        return stop(pf, pfs_unrecognized);

    if (strncmp(magic_nr, fmap->memory, magic_nr_len))
        return stop(pf, pfs_unrecognized);

    rptr = (const revision_t *)((const uint8_t *)fmap->memory + *r_off +
                                magic_nr_len);

    if (*rptr != magic_revision)
        return stop(pf, pfs_incompat);

    *r_off += magic_nr_len + sizeof(revision_t);
    return ps_continue;
}

static int parse_ushort(const struct filemap *fmap,
                        off_t *r_off,
                        unsigned short *out)
{
    const aushort_t *length;

    if (!has_room_for(fmap, *r_off, sizeof(*out)))
        return -1;

    length = (const aushort_t *)((const uint8_t *)fmap->memory + *r_off);
    *out = *length;
    *r_off += sizeof(*length);
    return 0;
}

static enum parse_status parse(struct persist_file *pf,
                               const struct filemap *fmap)
{
    off_t r_off;
    enum parse_status status;
    unsigned short items_count, incrid;

    r_off = 0;
    status = parse_magic(pf, fmap, &r_off);
    if (status != ps_continue)
        return status;

    if (parse_ushort(fmap, &r_off, &incrid))
        return stop(pf, pfs_corrupt);

    if (parse_ushort(fmap, &r_off, &items_count))
        return stop(pf, pfs_corrupt);

    if (items_count == 0)
        return stop(pf, pfs_regular);

    pf->items = itemset_new();
    if (!pf->items)
        return ps_fatal;

    while (r_off < fmap->size && items_count > 0) {
        unsigned short item_len;

        if (parse_ushort(fmap, &r_off, &item_len))
            return stop(pf, pfs_corrupt);

        if (!has_room_for(fmap, r_off, item_len))
            return stop(pf, pfs_corrupt);

        if (itemset_add(pf->items, (uint8_t *)fmap->memory + r_off, item_len))
            return ps_fatal;

        items_count--;
        r_off += item_len;
    }

    pf->incrid = incrid;
    return stop(pf, items_count ? pfs_corrupt : pfs_regular);
}

static int load(struct persist_file *pf)
{
    /* returns -1 on system error, but a return value of 0 does not imply
     * success.  The file_status field must be checked for status.
     */
    int fd, e = -1;
    struct filemap fmap;

    fd = openat(persist_dir_fd(pf->pd),
                persist_path_main(&pf->path),
                O_RDONLY | O_CLOEXEC);
    if (fd == -1) {
        if (errno == ENOENT) {
            pf->file_status = pfs_noent;
            return 0;
        }
        warn("openat (%s)", persist_path_main(&pf->path));
        return -1;
    }

    if (filemap_load_fd(&fmap, fd))
        goto cleanup;
    fd = -1;

    if (parse(pf, &fmap) == ps_fatal)
        goto cleanup;

    e = 0;
cleanup:
    if (fd != -1 && close(fd))
        warn("close(%d)", fd);
    return e;
}

// XXX filename might be evil, if the user says so.
struct persist_file *persist_file_new(struct persist_dir *pd,
                                      const char *filename)
{
    struct persist_file *pf;

    pf = malloc(sizeof(*pf));
    if (!pf) {
        warn("malloc");
        return NULL;
    }

    *pf = (struct persist_file){
        .pd = pd,
        .path = persist_path_new(filename),
    };

    if (!persist_path_valid(&pf->path)) {
        warn("malloc");
        goto fail;
    }

    return pf;

fail:
    persist_file_del(pf);
    return NULL;
}

enum persist_file_status persist_file_status(const struct persist_file *pf)
{
    return pf->file_status;
}

static int write_iovec(const struct persist_file *pf,
                       int fd,
                       unsigned iovec_len,
                       const struct iovec *iovec)
{
    int e;

    e = writev(fd, iovec, iovec_len);
    if (e == -1) {
        warn("writev (%s)", persist_path_pivot(&pf->path));
        return -1;
    }
    return 0;
}

static int write_magic(const struct persist_file *pf, int fd)
{
    return write_iovec(
        pf,
        fd,
        2,
        (struct iovec[]){
            {.iov_base = magic_nr, .iov_len = magic_nr_len},
            {.iov_base = &(uint8_t){magic_revision}, .iov_len = 1},
        });
}

static int write_item(const struct persist_file *pf,
                      int fd,
                      const uint8_t *data,
                      uint16_t data_len)
{
    return write_iovec(
        pf,
        fd,
        2,
        (struct iovec[]){
            {.iov_base = &(aushort_t){data_len}, .iov_len = sizeof(aushort_t)},
            {
                .iov_base = (void *)data,
                .iov_len = data_len,
            },
        });
}

static int write_incrid(const struct persist_file *pf, int fd)
{
    return write_iovec(pf,
                       fd,
                       1,
                       &(struct iovec){.iov_base = &(aushort_t){pf->incrid},
                                       .iov_len = sizeof(aushort_t)});
}

static int write_items_count(const struct persist_file *pf, int fd)
{
    aushort_t n;
    struct iovec n_items = {
        .iov_base = &n,
        .iov_len = sizeof(aushort_t),
    };

    n = pf->items ? itemset_get_size(pf->items) : 0;
    if (write_iovec(pf, fd, 1, &n_items) == -1)
        return -1;

    return 0;
}

static int write_items(const struct persist_file *pf, int fd)
{
    const void *aux = NULL;
    const uint8_t *data;
    uint16_t data_len;

    if (write_items_count(pf, fd))
        return -1;

    while (itemset_iter(pf->items, &aux, &data, &data_len))
        if (write_item(pf, fd, data, data_len))
            return -1;

    return 0;
}

bool persist_file_is_writeable(const struct persist_file *pf)
{
    bool can_write = status_info[pf->file_status].can_write;
    if (!can_write)
        notice("persist file %s is not writeable: %s",
               persist_path_main(&pf->path),
               status_info[pf->file_status].status_name);

    return can_write;
}

int persist_file_write(const struct persist_file *pf)
{
    int e, fd, dirfd;
    const char *main, *pivot;

    if (!persist_file_is_writeable(pf))
        return -1;

    dirfd = persist_dir_fd(pf->pd);
    pivot = persist_path_pivot(&pf->path);

    fd = openat(dirfd, pivot, O_WRONLY | O_CREAT | O_CLOEXEC, S_IRWXU);
    if (fd == -1) {
        warn("openat (%s)", pivot);
        return -1;
    }

    e = -1;
    if (write_magic(pf, fd))
        goto exit;

    if (write_incrid(pf, fd))
        goto exit;

    if (write_items(pf, fd))
        goto exit;

    main = persist_path_main(&pf->path);
    if (renameat(dirfd, pivot, dirfd, main)) {
        warn("renameat (%s -> %s)", pivot, main);
        return -1;
    }

    e = 0;
exit:
    close(fd);
    return e;
}

int persist_file_unlink(const struct persist_file *pf)
{
    if (!status_info[pf->file_status].can_write) {
        warnx("persist file '%s': cannot unlink (%s)",
              persist_path_main(&pf->path),
              status_info[pf->file_status].status_name);
        return -1;
    }

    if (unlinkat(persist_dir_fd(pf->pd), persist_path_main(&pf->path), 0)) {
        warn("unlinkat %s", persist_path_main(&pf->path));
        return -1;
    }

    return 0;
}

int persist_file_load(struct persist_file *pf)
{
    if (pf->items) {
        itemset_del(pf->items);
        pf->items = NULL;
    }

    return load(pf);
}

unsigned persist_file_get_incrid(const struct persist_file *pf)
{
    return pf->incrid;
}

void persist_file_set_incrid(struct persist_file *pf, unsigned iid)
{
    pf->incrid = iid;
}

const char *persist_file_name(const struct persist_file *pf)
{
    return persist_path_main(&pf->path);
}

const struct itemset *persist_file_get_items(const struct persist_file *pf)
{
    return pf->items;
}

struct itemset *persist_file_swap_items(struct persist_file *pf,
                                        struct itemset *items)
{
    struct itemset *out = pf->items;
    pf->items = items;
    return out;
}

void persist_file_del(struct persist_file *pf)
{
    if (!pf)
        return;

    itemset_del(pf->items);
    persist_path_free(&pf->path);
    free(pf);
}
