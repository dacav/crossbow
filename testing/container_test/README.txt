NAME

   run - container-based black-box testing system for Crossbow

SYNOPSIS

   build
   run

DESCRIPTION

   Run a black-box test suite in a container.

   The container image is built by the "build" script.
   Once the container is built, the "run" script can be used to verify the
   currently checked out source tree.

   The verification is achieved by compiling and installing Crossbow within a
   container.  The test suite is then executed.
