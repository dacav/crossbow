#!/bin/sh
#
# This test ensures that every documented behaviour checks out.

set -e

atexit() {
	local x="$?"

	[ -z "$tempdir" ] || rm -rf "$tempdir"
	exit "$x"
}
trap atexit EXIT
trap exit INT

tempdir="$(mktemp -d)"
export HOME="$tempdir"

# Set up auxiliary scripts

mkdir "$tempdir/bin"
export PATH="$tempdir/bin:$PATH"

mkscript() {
	: "${1:?}"
	cat >"$tempdir/bin/$1"
	chmod +x "$tempdir/bin/$1"
}

mkscript fails_first <<-'EOF'
	#!/bin/sh
	set -e
	if grep -q item1.description; then
		echo >&2 'fails_first: failure'
		echo FAIL
		exit 1
	fi
	echo >&2 'fails_first: success'
	echo SUCCESS
EOF

mkscript save_entry <<-'EOF'
	#!/bin/sh
	set -e
	test_name="${1:?missing test name}"
	entry_id="${2:?missing entry id}"

	exec >"./save_entry.$test_name.$entry_id"

	# Pour the whole content in the output file, then adjust for the missing
	# new-line at the end of the piped content.
	cat && printf \\n
EOF


test_processing_failure() {
	# Ensure that failing to process a feed entry is not fatal to the following
	# entries.

	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1

		feed good
			url file://${PWD}/assets/feed.rss
			handler print
			format <%fi> %g, %t\\n

		feed bad_outfmt
			url file://${PWD}/assets/feed.rss
			handler print
			format <%fi> %w, %t\\n

		feed fails_first
			url file://${PWD}/assets/feed.rss
			handler pipe
			command fails_first
	EOF

	crossbow -vv good >"$tempdir/stdout"
	diff -u "$tempdir/stdout" - <<-EOF
		<good> item1.guid, item1.title
		<good> item2.guid, item2.title
	EOF

	if crossbow -vv bad_outfmt; then exit 1; fi

	if crossbow -vv fails_first >"$tempdir/stdout"; then exit 1; fi

	diff -u "$tempdir/stdout" - <<-EOF
		FAIL
		SUCCESS
	EOF
}

test_dry_exec() {
	# Test the -D flag.  It implements a dry run for sub-processes,
	# so that they're not executed.
	local test_name=test_dry_exec

	mkdir -p "$tempdir/$test_name"
	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1

		feed dry_exec
			url file://${PWD}/assets/feed.rss
			handler pipe
			chdir $tempdir/$test_name
			command save_entry dry_exec %n

		feed regular_exec
			url file://${PWD}/assets/feed.rss
			handler pipe
			chdir $tempdir/$test_name
			command save_entry regular_exec %n
	EOF

	crossbow regular_exec
	crossbow -D dry_exec

	[ -e "$tempdir/$test_name/save_entry.regular_exec.000000" ]
	diff -u "$tempdir/$test_name/save_entry.regular_exec.000000" - <<-EOF
	item1.description
	EOF

	[ -e "$tempdir/$test_name/save_entry.regular_exec.000001" ]
	diff -u "$tempdir/$test_name/save_entry.regular_exec.000001" - <<-EOF
	item2.description
	EOF

	[ ! -e "$tempdir/$test_name/save_entry.dry_exec.000000" ]
	[ ! -e "$tempdir/$test_name/save_entry.dry_exec.000002" ]
}

test_dry_mark() {
	# Test the -d flag.  It implements a dry run from the persistence
	# perspective, not flagging new entries as seen.
	local test_name=test_dry_mark

	mkdir -p "$tempdir/$test_name"
	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1

		feed ${test_name}
			url file://${PWD}/assets/feed.rss
			handler print
			format %n\\n
	EOF

	# Run it twice with persistence disabled, and twice with
	# persistence enabled.
	{
		crossbow -d
		crossbow -d
		crossbow
		crossbow
	} >"$tempdir/stdout"

	# The two items of assets/feed.rss are seen three times.
	# The last invocation will see nothing, as it both entries are already
	# marked as seen.
	diff -u "$tempdir/stdout" - <<-'EOF'
	000000
	000001
	000000
	000001
	000000
	000001
	EOF
}

test_processing_failure
test_dry_exec
test_dry_mark
