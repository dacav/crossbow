#!/bin/sh
#
# Testing quality aspects, e.g. robustness to huge input,
# or to uncommon corner cases seen in the wild.
set -e

atexit() {
	local ex="$?"

	[ -z "$tempdir" ] || rm -rf "$tempdir"
	exit "$ex"
}
trap atexit EXIT

tempdir="$(mktemp -d)"
export HOME="$tempdir"

gen_long_feed() {
	local size

	bs="${1:?missing bs}"
	count="${2:?missing count}"

	printf '<?xml version="1.0" encoding="utf8"?>\n'
	printf '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">\n'
	printf '<channel>\n'
	printf '<item>\n'
	printf '<guid>foobar</guid>\n'
	printf '<description>'
	dd status=none if=/dev/zero bs="$bs" count="$count" | tr '\000' x
	printf '</description>\n'
	printf '</item>\n'
	printf '</channel>\n'
	printf '</rss>\n'
}

cat >~/.crossbow.conf <<-EOF
	compat 1

	feed long_feed_exec
		url file://$tempdir/feed.xml
		handler exec
		command echo %c

	feed long_feed_pipe
		url file://$tempdir/feed.xml
		handler pipe
		command hexdump -C

	feed duplicated_items
		url file://$tempdir/feed.xml
		handler exec
		command echo %c
EOF

: "
Test robustness against ridiculously long feeds
"
{
	gen_long_feed 1024 1024 >"$tempdir/feed.xml"

	if crossbow -v long_feed_exec 2>"$tempdir/error"; then exit 1; fi
	grep -q -e ofmt_fail_too_long_evaluation "$tempdir/error"

	crossbow -v long_feed_pipe >"$tempdir/stdout"
	diff -u "$tempdir/stdout" - <<-EOF
	00000000  78 78 78 78 78 78 78 78  78 78 78 78 78 78 78 78  |xxxxxxxxxxxxxxxx|
	*
	00100000
	EOF
}

: "
Test robustness against feed having duplicate item ids
"
{
	# Crossbow will identify repeated items by hashing the concatenation of
	# some of the item fields.  This will allow feeds to violate the uniqueness
	# of the item id.
	: Step 1
	cat >"$tempdir/feed.xml" <<-EOF
	<?xml version="1.0" encoding="utf8"?>
	<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
			<item><guid>Surprise</guid><title>1</title><description>one</description></item>
			<item><guid>Surprise</guid><title>2</title><description>two</description></item>
		</channel>
	</rss>
	EOF

	# No duplication.
	crossbow -v duplicated_items >"$tempdir/stdout" 2>"$tempdir/stderr"
	diff -u "$tempdir/stdout" - <<-EOF
	one
	two
	EOF
	diff -u "$tempdir/stderr" - <<-EOF
	crossbow: scheduling fetch for duplicated_items
	crossbow: processing duplicated_items
	crossbow: feed duplicated_items: old_news=0 new_news=2
	EOF

	# New items that duplicate an existing one are not handled,
	# as one would expect.
	# Duplicated instances of the same new item are individually handled, but a
	# warning is emitted on stderr.
	: Step 2
	cat >"$tempdir/feed.xml" <<-EOF
	<?xml version="1.0" encoding="utf8"?>
	<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
			<item><guid>Surprise</guid><title>2</title><description>two</description></item>
			<item><guid>Surprise</guid><title>3</title><description>three</description></item>
			<item><guid>Surprise</guid><title>3</title><description>three</description></item>
		</channel>
	</rss>
	EOF

	crossbow -v duplicated_items >"$tempdir/stdout" 2>"$tempdir/stderr"
	diff -u "$tempdir/stdout" - <<-EOF
	three
	three
	EOF
	diff -u "$tempdir/stderr" - <<-EOF
	crossbow: scheduling fetch for duplicated_items
	crossbow: processing duplicated_items
	crossbow: Duplicated item in feed duplicated_items: 10b731c7ef867e7eb986cbf928af92898104d6
	crossbow: feed duplicated_items: old_news=1 new_news=2
	EOF
}
