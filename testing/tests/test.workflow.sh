#!/bin/sh
#
# Testing the regular workflow with local parsing.

set -e

atexit() {
	local ex="$?"

	[ -z "$tempdir" ] || rm -rf "$tempdir"
	exit "$ex"
}
trap atexit EXIT

tempdir="$(mktemp -d)"
export HOME="$tempdir"

gen_feed() {
	cat <<-EOF
	<?xml version="1.0" encoding="utf8"?>
	<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
	EOF

	seq "$@" | while read -r item_id; do
		cat <<-EOF
			<item><guid isPermaLink="true">item_${item_id}.guid</guid></item>
		EOF
	done

	cat <<-EOF
		</channel>
	</rss>
	EOF
}

# Note how 'testfeed' and 'toastfeed' have different output formats.
cat >~/.crossbow.conf <<-EOF
	compat 1

	feed testfeed
		url file://$tempdir/feed.xml
		handler print
		format %fi %n %g\\n

	feed toastfeed
		url file://$tempdir/feed.xml
		handler print
		format %fi %n %g\\n
EOF

: Generate a feed with two entries, from 1 to 2.
: The two feed should distinctly report both of them.
{
	gen_feed 1 2 >"$tempdir/feed.xml"

	crossbow testfeed >"$tempdir/stdout"
	diff -u "$tempdir/stdout" - <<-EOF
		testfeed 000000 item_1.guid
		testfeed 000001 item_2.guid
	EOF

	crossbow >"$tempdir/stdout"
	diff -u "$tempdir/stdout" - <<-EOF
		toastfeed 000000 item_1.guid
		toastfeed 000001 item_2.guid
	EOF
}

: "
Add one entry  The two feeds should detect it, and skip the first two,(as
they have been marked as seen).
"
{
	gen_feed 1 3 >"$tempdir/feed.xml"
	{
		crossbow testfeed
		crossbow toastfeed
	} >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
		testfeed 000002 item_3.guid
		toastfeed 000002 item_3.guid
	EOF
}

: "
Drop the first entry.  The two feeds should not react, as there's no new entry.
The unseen side effect is that the first entry is forgotten.
"
{
	gen_feed 2 3 >"$tempdir/feed.xml"
	{
		crossbow testfeed
		crossbow toastfeed
	} >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
	EOF
}

: "
Add the first entry back.  Since it was forgotten on the previous step, it
will be detected, and associated with another incremental identifier.
"
{
	gen_feed 1 3 >"$tempdir/feed.xml"
	{
		crossbow testfeed
		crossbow toastfeed
	} >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
		testfeed 000003 item_1.guid
		toastfeed 000003 item_1.guid
	EOF
}

: "
Add entry 4 and remove entry 1 again.  Then process it with the catch-up
flag -c.  We are expected to see no output (due to -c), yet the entry
identifier should be incremented of 1 (because we introduced entry 4).
Adding entry 1 back will result in it being detected, but assinged with
the identifier 000005 (one was skipped).
"
{
	gen_feed 2 4 >"$tempdir/feed.xml"
	crossbow -c testfeed >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
	EOF

	gen_feed 1 4 >"$tempdir/feed.xml"
	crossbow testfeed >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
		testfeed 000005 item_1.guid
	EOF
}

: "
In the previous step we only worked with 'testfeed'.  Since 'toastfeed' was
not touched, it will see the cumulative effect of the XML updates, that is
only the appearance of entry 4.
"
{
	crossbow toastfeed >"$tempdir/stdout"

	diff -u "$tempdir/stdout" - <<-EOF
		toastfeed 000004 item_4.guid
	EOF
}
