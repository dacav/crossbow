#!/bin/sh
#
# Testing various network-related situations.
#
# Note that the XML file retrieval is handled by libcurl: the code in this
# testing module is meant to exercise only the reaction to adverse network
# conditions.  Testing the business logic (e.g. how the feed is handled) does
# not belong here.

http_serve() {
	local file="${workdir:?}/assets/feed.rss"
	local size

	size=$(wc -c <"$file") || return

	printf "HTTP/1.1 200 OK\r\n"
	printf "Content-Type: application/x-rss+xml\r\n"
	printf "Content-Length: %d\r\n" "$size"
	printf "\r\n"
	cat "$file"
}

http_serve_hangup() {
	printf "HTTP/1.1 200 OK\r\n"
	printf "Content-Type: application/x-rss+xml\r\n"
	printf "Content-Length: 1337\r\n"
	printf "\r\n"
}

atexit() {
	local ex="$?"

	[ -z "$srvpid" ] || kill -TERM "$srvpid"
	[ -z "$tempdir" ] || rm -rf "$tempdir"
	exit "$ex"
}
trap atexit EXIT
trap exit TERM

tempdir="$(mktemp -d)"

export HOME="$tempdir"
cat >~/.crossbow.conf <<EOF
	compat 1
	feed testfeed
	url http://127.0.0.1:8000/feed.rss
	handler print
	format %t %g %l %c\\n
EOF


# == Testing regular situation ==

http_serve | nc -Nl 8000 &
srvpid="$!"

cat >"$tempdir/expected" <<EOF
item1.title item1.guid item1.link item1.description
item2.title item2.guid item2.link item2.description
EOF

crossbow -vv >"$tempdir/effective"
srvpid=
diff -u "$tempdir/expected" "$tempdir/effective"


# == Testing server that hangs up before sending data ==

http_serve_hangup | nc -Nl 8000 &
srvpid="$!"

if crossbow -vv 2>"$tempdir/effective"; then exit 1; fi
srvpid=

# Assuming that the message won't change in future versions of libcurl.
# If it will (why, anyway?) the test needs to be updated.
cat >&2 "$tempdir/effective"
grep -q \
	-e 'failed to download testfeed:.* 1337 bytes' \
	"$tempdir/effective"
