#!/bin/sh
#
# The goal of this test is to ensure that the execution of crossbow(1) does not
# leak file descriptors into the subprocess.

set -e

atexit() {
	local x="$?"

	[ -z "$tempdir" ] || rm -r "$tempdir"
	exit "$x"
}
trap atexit EXIT
trap exit INT

tempdir="$(mktemp -d)"

# We construct a temporary program that loops over all the allowed file
# descriptors beyond 2 (stderr) and up to rlimit.  The program closes them all,
# expecting a EBADF failure.
#
# If we manage to close a file descriptor, it means that the program leaked it.
cat >"$tempdir/mysubproc.c" <<-'EOF'
	#include <err.h>
	#include <errno.h>
	#include <fcntl.h>
	#include <sys/resource.h>
	#include <sys/stat.h>
	#include <sys/time.h>
	#include <sys/types.h>
	#include <unistd.h>

	int main(int argc, char **argv)
	{
		struct rlimit rlim;
		int fd;

		if (getrlimit(RLIMIT_NOFILE, &rlim))
			err(1, "getrlimit");

		for (fd = 3; fd < rlim.rlim_cur; fd ++) {
			if (close(fd) == 0)
				errx(1, "!!! %d was a valid file descriptor !!!", fd);

			if (errno != EBADF)
				err(fd, "close %d", fd);

			if (fd % 100 == 0)
				warnx("all good up to %d...", fd);
		}

		warnx("all good up to %d (rlimit)", fd - 1);
		return 0;
	}
EOF

mkdir "$tempdir/bin"
cc -Wall -Werror "$tempdir/mysubproc.c" -o "$tempdir/bin/mysubproc"
chmod +x "$tempdir/bin/mysubproc"
PATH="$tempdir/bin:$PATH"

export HOME="$tempdir"
cat >~/.crossbow.conf <<EOF
compat 1
feed testfeed
	url file://${PWD}/assets/feed.rss
	handler pipe
	command mysubproc
EOF

crossbow -vv
