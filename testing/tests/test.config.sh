#!/bin/sh
#
# This test ensures various properties of consistency between the configuration,
# the behaviour of crossbow, and the persistence file.

set -e

atexit() {
	local x="$?"

	[ -z "$tempdir" ] || rm -rf "$tempdir"
	exit "$x"
}
trap atexit EXIT
trap exit INT

tempdir="$(mktemp -d)"
export HOME="$tempdir"

test_missing_config() {
	# Crossbow fails if the configuration file does not exits.
	if crossbow -vv 2>"$tempdir/stderr"; then exit 1; fi
	diff -u "$tempdir/stderr" - <<-EOF
	crossbow: no configuration file available
	EOF
}

test_no_feeds() {
	# If the existing configuration defines no feed, Crossbow succeeds but
	# casts a warning.

	printf "compat 1\n" >"$HOME/.crossbow.conf"
	crossbow 2>"$tempdir/stderr"

	diff -u "$tempdir/stderr" - <<-EOF
		crossbow: no feeds defined
	EOF
}

test_persist_unlinked() {
	# The persistence file is created when the feed is defined, and gets
	# unlink(2)-ed when the feed is removed from the configuration.
	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1
		feed test1
		url file://${PWD}/assets/feed.rss
		feed test2
		url file://${PWD}/assets/feed.rss
	EOF

	crossbow -vv
	test -e "$HOME/.crossbow/test1"
	test -e "$HOME/.crossbow/test2"

	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1
		feed test1
		url file://${PWD}/assets/feed.rss
	EOF

	crossbow -vv
	test -e "$HOME/.crossbow/test1"
	test \! -e "$HOME/.crossbow/test2"
}

test_chdir() {
	# Testing chdir to work properly

	mkdir -p "$HOME/x/y/z"
	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1
		feed test_chdir
		url file://${PWD}/assets/feed.rss
		handler exec
		chdir ~/x/y/z
		command sh -c echo\\ hi\\ from\\ \$PWD
	EOF

	crossbow -vv >"$HOME/output"

	# NOTE: Repeated twice because the feed has two items
	diff -u "$HOME/output" - <<-EOF
	hi from $HOME/x/y/z
	hi from $HOME/x/y/z
	EOF
}

test_feed_name_unique() {
	# Mutliple feeds having the same name are forbidden: the process is
	# terminated before processing.

	cat >"$HOME/.crossbow.conf" <<-EOF
		compat 1

		feed foobar
		url file://${PWD}/assets/feed.rss

		feed foobar
		url file://${PWD}/assets/feed.rss
	EOF

	if crossbow -vv 2>"$HOME/stderr"; then exit 1; fi
	grep -Fqe 'Duplicated feed name in configuration: foobar' "$HOME/stderr"
}

test_missing_config
test_no_feeds
test_persist_unlinked
test_chdir
test_feed_name_unique
