== Automated testing ==

This directory is devoted to automated testing

./assets                        Auxiliary files for testing

./container_test/README.txt     How to run ./container_test

./container_test/run            Run the test suite against the current
                                source tree

./logs                          A directory where logs end up when running
                                either ./container_test or ./vm_test

./tests                         The test suite. A collection of scripts
                                verifying the software behaviour, used both
                                by ./container_test and ./vm_test.

./vm_test/README.txt            How to run ./vm_test

./vm_test/run                   Run the test suite against a given tarball on a
                                given virtual machine. 
