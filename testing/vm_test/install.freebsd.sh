#!/bin/sh

set -e

. "$(dirname "$0")/common.sh"
: "${tarball:?}"

# Making sure that the system time is correct.
# This is needed for TLS to work properly
if [ "$sync_nntp" ]; then
	ntpd -gq
fi

export ASSUME_ALWAYS_YES=yes
pkg bootstrap
pkg install -y cmake curl uthash pkgconf mxml

export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
export CPPFLAGS="-I/usr/local/include"
export LDFLAGS="-L/usr/local/lib"
export PKG_CONFIG_PATH="/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH"

src_install_cmake \
  -T \
  -n libeccio \
  -u 'https://dacav.org/projects/libeccio/releases/1.0.0/libeccio-1.0.0.tar.gz'

src_install_cmake -T -f "$tarball" -n crossbow -D UT_SAN=ON
