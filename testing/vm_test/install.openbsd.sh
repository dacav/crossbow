#!/bin/sh

set -e

. "$(dirname "$0")/common.sh"
: "${tarball:?}"

# Making sure that the system time is correct.
# This is needed for TLS to work properly
if [ "$sync_nntp" ]; then
	rdate pool.ntp.org
fi

export LD_LIBRARY_PATH="/usr/local/lib:$LD_LIBRARY_PATH"
export CPPFLAGS="-I/usr/local/include"
export LDFLAGS="-L/usr/local/lib"

add_dep() {
	local dep_name="${1:?}"
	set -x

	shift
	"$@" && echo "$dep_name: OK" || pkg_add "$dep_name"

	# pkg_add exits 0 on failure.  Testing dependency again
	"$@" && echo "$dep_name: OK" || exit 1
} >&2

add_dep uthash  test -e /usr/local/include/uthash.h
add_dep curl    pkg-config --exists libcurl
add_dep mxml    pkg-config --exists mxml
add_dep cmake   command -v cmake

src_install_cmake \
  -T \
  -n libeccio \
  -u 'https://dacav.org/projects/libeccio/releases/1.0.0/libeccio-1.0.0.tar.gz'

CC=clang src_install_cmake -T -f "$tarball" -n crossbow -D UT_SAN=OFF
