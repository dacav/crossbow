cache=/var/cache/crossbow

extract() {
	local cwd=.
	local tarball=
	local url=

	OPTIND=1; while getopts C:t:u: opt; do
		case "$opt" in
		C)	cwd="$OPTARG";;
		t)	tarball="$OPTARG";;
		u)	url="$OPTARG";;
		*)	return 1;;
		esac
	done

	if [ -n "$url" ]; then
		tarball="$(basename "$url")"
		curl -sSf -L "$url" -o "$cwd/$tarball"
		tarball="$cwd/$tarball"
	fi

	tar -C "$cwd" -xzf "${tarball:?}"
}

src_install_configure() (
	local file=
	local name=
	local test=
	local unittest=
	local url=

	local configure
	local src
	local workdir

	set -e

	OPTIND=1; while getopts f:n:Tt:u: opt; do
		case "$opt" in
		f)	file="$OPTARG";;
		n)	name="$OPTARG";;
		t)	test="$OPTARG";;
		u)	url="$OPTARG";;
		T)	unittest=1;;
		*)	return 1;;
		esac
	done

	: "${name:?}"

	if [ -e "$test" ]; then
		printf >&2 "skip src_install %s: %s exists\n" "$name" "$test"
		return 0
	fi

	mkdir -p "${cache:?}"
	workdir="$(mktemp -d "${cache:?}/install_XXXXXXXXX")"
	extract -C "$workdir" ${file:+-t "$file"} ${url:+-u "$url"}

	configure="$(find "$workdir" -type f -name configure)"
	src="$(dirname "${configure:?}")"

	cd "$src"
	./configure
	make V=1
	[ -z "$unittest" ] || make check
	exec make install
)

src_install_cmake() (
	local file=
	local name=
	local cmake_opts=
	local test=
	local unittest=
	local url=

	local cmakefile
	local src

	set -e

	OPTIND=1; while getopts D:f:n:Tt:u: opt; do
		case "$opt" in
		D)	cmake_opts="$cmake_opts -D$OPTARG";;
		f)	file="$OPTARG";;
		n)	name="$OPTARG";;
		t)	test="$OPTARG";;
		u)	url="$OPTARG";;
		T)	unittest=1;;
		*)	return 1;;
		esac
	done

	: "${name:?}"

	if [ -e "$test" ]; then
		printf >&2 "skip src_install %s: %s exists\n" "$name" "$test"
		return 0
	fi

	mkdir -p "${cache:?}" || return
	workdir="$(mktemp -d "${cache:?}/install_XXXXXXXXX")" || return
	extract -C "$workdir" ${file:+-t "$file"} ${url:+-u "$url"}

	cmakefile="$(find "$workdir" -type f -name CMakeLists.txt | head -1)"
	src="$(dirname "${cmakefile:?}")"

	cmake ${cmake_opts:+$cmake_opts} -B "${name}_build" -S "$src"
	make -C "${name}_build" VERBOSE=1
	[ -z "$unittest" ] || (cd "$src" && ctest)
	exec make -C "${name}_build" VERBOSE=1 install
)

seq() {
	# Invoke seq(1) when available (GNU coreutils and FreeBSD).
	# Invoke jot(1) to achieve the same result under OpenBSD.

	case "$(uname -a)" in
	*OpenBSD*)
		if [ -z "$2" ]; then
			jot "$1" 1
		else
			jot $(($2 - $1 + 1)) "$1"
		fi
		;;

	*FreeBSD* | *GNU*)
		# Assuming seq is available.  TODO: Darwin?
		command seq "$@"
		;;

	*)	echo 2>& "Not sure how to 'seq' in $(uname -a)"
		return 1;;
	esac
}
