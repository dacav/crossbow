NAME

   run - vm-based black-box testing system for Crossbow

SYNOPSIS

   run -H host [-l login] -o ostype [-p ssh_port] [-T] [-t tarball]

DESCRIPTION

   Run a black-box test suite on the supplied remote host.

OPTIONS

   -H host      Specify a host where to run tests.

   -l           Specify a remote login name (default: root).

   -o ostype    The operationg-system specific functions are in a file named
                testing/install.${ostype}.sh

   -p ssh_port  Specify a ssh port (default: 22).

   -T           If enabled, synchronize the system time of the testing host
                using NTP.

   -t           Specify a tarball to install.
                If not specified the installation process will be skipped,
                and the test run assuming that some version of crossbow(1) is
                already installed.  The tests might of course fail if they are
                not compatbile with it.

CHEATSHEET

    This section has not much to do with testing crossbow, but it is useful as
    personal reference.

  Determine os variant

      osinfo-query --fields=name,short-id,version os |
      grep -e openbsd -e freebsd

  Misc tips:

    - The install iso must be in a place where libvirt has access
      permissions.  I like to use /usr/local/share/iso.

    - Network not working?
      Ensure all packages are installed.
      Ensure the host network has sensible settings.
      Might help: ip link delete virbrX + systemctl restart libvirtd

    - Snapshot right after installation, to speed up system restorations.

  OpenBSD Virtual Machine

    - Download the latest install iso, e.g. install-76.iso

    - Create new VM

      virt-install \
        --os-variant "${os_variant:?}" \
        --name obsd-76 \
        --memory 1024 \
        --vcpus=2,maxvcpus=2 \
        --cpu host \
        --cdrom /usr/local/share/iso/install76.iso \
        --disk format=qcow2,size=4 \
        --clock hpet_present=yes \
      ;

  FreeBSD Virtual Machine

    - Download the latest qcow2 image, e.g FreeBSD-XX.Y-RELEASE-amd64-qcow2.xz

    - Uncompress (xzcat), extend (qemu-img resize ./FreeBSD*qcow2 +10G)

    - Create new VM

      virt-install \
        --os-variant "${os_variant:?}" \
        --name freebsd-13 \
        --memory 1024 \
        --vcpus=2,maxvcpus=2 \
        --cpu=host \
        --import \
        --disk=/usr/local/share/iso/FreeBSD-13.2-RELEASE-amd64.qcow2 \
      ;
