.Dd September 30, 2021
.Dt CROSSBOW-FORMAT 5 URM
.Os \&
.\" ---------------------------------------------------------------------
.Sh NAME
.Nm crossbow-format
.Nd format string reference
.\" ---------------------------------------------------------------------
.Sh DESCRIPTION
The
.Xr crossbow 1
feed aggregator processes each new entry by means of a handler, according
to the feed settings in the
.Xr crossbow.conf 5
configuration file.
.Pp
The
.Cm print
handler prints a textual representation of an entry on
.Xr stdout 3 .
The optional
.Cm format
setting can define a template to be used in place of the default.
The
.Cm exec
and
.Cm pipe
handlers process individual entries by invoking an external program.
They both require the definition of a
.Cm command
setting, to expand into the argument vector of the invoked subprocess.
.Pp
The values of
.Cm format
and
.Cm command
are interpreted as format strings: they are allowed to contain
placeholders that are replaced with the fields of the processed entry.
All the available placeholders are listed below (see
.Sx Supported placeholders Ns ).
.Pp
The placeholders syntax resembles the
.Xr printf 3
function, with some differences and simplifications:
.Bl -bullet
.It
The
.Ql %
character can only be followed by one or more alphabetic characters.
There is no support for field width or other modifiers.
.It
A raw
.Ql %
character is represented with
.Ql \e%
instead of
.Ql %% .
.It
The backslash character
.Ql \e
escapes the subsequent character:
.Bl -dash
.It
Some escape sequences are recognized as in the C programming language:
.Ql \en
(new line),
.Ql \er
(carriage return),
.Ql \et
(horizontal tab) and
.Ql \e\e
(literal backslash).
.It
If the handler is
.Cm exec
or
.Cm pipe ,
the
.Ql \e\ \&
string is interpreted as space character, as opposed to a non-escaped
white-space that is interpreted as argument separator.
Escaping white-spaces has no effect if the handler is
.Cm print ,
since in this case they do not have any special meaning.
.It
The
.Ql \e:
string is interpreted as a zero width break
(see
.Sx Zero width break
below).
.El
.El
.\" ---------------------------------------------------------------------
.Ss Argument vector expansion
In the context of the
.Cm exec
or
.Cm pipe
handlers,
the argument vector of a subprocess is constructed on the
.Cm command
setting.
.Pp
The value is split on white-spaces into an intermediate array of
tokens.
When a new entry is processed, each element of the intermediate array is
further evaluated for placeholder expansion.
The obtained array is then used as argument vector for the subprocess.
The first of its elements determines the command to execute.
.Pp
Details:
.Bl -bullet
.It
Whites-spaces are intended as in
.Xr isspace 3 ;
.It
Since the tokenization happens before the placeholder expansion, an
expanded field can never be part of two arguments, even if it contains
white-spaces.
.It
Multiple white-spaces are considered part of the same delimiter.
In case it is needed, it is still possible to pass around an empty
argument by using a zero width break (See
.Sx Zero width break
below).
.It
The subprocess is invoked via
.Xr execvp 3 .
The
.Ev PATH
environment variable is honoured.
.It
It is not possible to violate the convention by which
the value of
.Ql argv[0]
corresponds to the name of the executable.
.It
For security reasons the command line is
.Em not
parsed by a shell
interpreter (see
.Sx Security
below).
Special operators, such as input redirection, are therefore not available.
.El
.\" ---------------------------------------------------------------------
.Ss Zero width break
The language recognized by the output format parser allows
the placeholders to be composed by multiple characters.
While this feature makes it easier to have mnemonic placeholders
(such as
.Ql %a
for
.Qq Author
and
.Ql %am
for
.Qq Author eMail Ns
),
it introduces some additional edge cases.
.Pp
The zero width break sequence
.Ql \e:
has been introduced to cover a case of ambiguity which can
be easily explained by means of an example.
.Pp
Let
.Ql %x
and
.Ql %xn
be valid placeholders.
In such case obtaining the expansion of
.Ql %x
followed by a literal
.Ql n
would be impossible, as the sequence
.Ql %x\en
would be rendered as the expansion of
.Ql %x
followed by a new-line, while
.Ql %xn
would be rendered as the expansion of
.Ql %xn .
Using the backslash would have worked if
.Ql \en
wasn't a recognized escape sequence.
.Pp
The zero width break can be used to force the termination of an escape
sequence, so that whatever follows can be interpreted independently of it.
.Pp
The behaviour is summarized by the following table, where
.Fn expand x
expresses the expansion of the
.Ql %x
placeholder into the string representation of the
corresponding
.Em field ,
and the
.Qo . Qc
operator expresses string concatenation.
.Bl -column -offset indent xxxxxx yyyyyyyyyyyyyy z
.It Em Format Ta Em Expansion Ta Em Notes
.It %x Ta Fn expand x Ta
.It %xn Ta Fn expand xn Ta
.It %x\em Ta Fn expand x No \&. Qo m Qc Ta
Works because
.Ql \em
is not a recognized escape sequence.
.It %x\en Ta Fn expand x No \&. Qo \en Qc Ta
.It %x\e:n Ta Fn expand x No \&. Qo n Qc Ta
.El
.Pp
Additionally, although not originally intended for this purpose, the
zero width break can be used to pass empty strings as subprocess
arguments.
This is demonstrated in the following example, where the
configuration prints the entry author, followed by a space character, and
by the entry title:
.Bd -literal -offset indent
feed foobar
  url https://example.conf/feed.xml
  handler exec
  command printf \e%s\e%s\e%s\e\en %a \e: %t
.Ed
.Pp
.Em Note :
the literal
.Ql %s
is intended to be interpreted by the
.Xr printf 1
command.
The corresponding percent character is escaped, since it is meant to be a
literal percent, and not to be expanded by
.Xr crossbow 1 .
.\" ---------------------------------------------------------------------
.Ss Supported placeholders
The following table shows the supported placeholders and the corresponding
entry properties for the RSS and Atom feed formats.
.Pp
Depending on the feed format, some placeholders may refer to unavailable
entry properties.
If this is the case they are expanded with an empty string, with
some exceptions (see Notes).
.Bl -column -offset indent pholder RSS________________ Atom___
.It Em P.holder Ta Em RSS       Ta Em Atom
.It %a  Ta -                    Ta author.name
.It %am Ta author               Ta author.email
.It %au Ta -                    Ta author.uri
.It %ca Ta category[0] (1)      Ta -
.It %co Ta comments             Ta -
.It %c  Ta content:encoded (2)  Ta content
.It %g  Ta guid                 Ta id
.It %gp Ta guid_isPermaLink (3) Ta - (4)
.It %l  Ta link                 Ta link[0] (1)
.It %pd Ta pubDate              Ta published
.It %s  Ta description          Ta summary
.It %t  Ta title                Ta title
.El
.Pp
.Em Notes :
.Bl -tag -width XXX -offset indent -compact
.It (1)
Some of the entry properties can have multiple values.
Only the first one is currently available.
This limitation might be lifted with future updates.
.It (2)
The
.Ql <content:encoded>
element of RSS is an extension provided by the
.Lk http://purl.org/rss/1.0/modules/content/
XML namespace.
If such namespace is not enabled for the feed, the
.Ql %c
placeholder will be
expanded with the regular
.Ql <content>
tag of RSS.
.It (3)
Boolean, represented with
.Ql 0
or
.Ql 1 .
.It (4)
According to the Atom definition, the
.Ql <id>
element conveys a permanent, universally unique identifier.
.Ql %gp
is therefore undefined, but always expanded as
.Ql 1 .
.El
.Pp
Some additional placeholders, not referring to any entry field, are also
available:
.Bl -column -offset indent pholder Description
.It Em P.holder Ta Em Description
.It %fi Ta
The unique name of the feed.
.It %ft Ta
The feed title
.It %n Ta
A per-feed zero-padded six digits incremental number.
.El
.Pp
The incremental number expanded in place of
.Ql %n
is initialized to zero for new feeds, and gets incremented for
every new feed entry.
This is an important security feature: the value of this number is not
controlled by the feed content, thus it can be used safely as filename.
The value is persisted across executions, and incremented even if the item
was not successfully processed, so that the same value is never used twice.
.\" ---------------------------------------------------------------------
.Ss Security
Since the
.Cm exec
and
.Cm pipe
handlers process entries by passing parameters to a subprocess, it is
important to keep security in mind when configuring the corresponding
.Cm command
setting.
.Bl -enum -offset XXX
.It
Inserting an interpreted code snippet within a command template is
strongly discouraged, as it might constitute an easy target for code
injection.
.Pp
Consider, for example, the following configuration:
.Bd -literal -offset indent
feed foobar
  url https://example.conf/feed.xml
  handler exec
  command sh -c echo\\ "%t"\\ |\\ wc\\ -c
.Ed
.Pp
The provided command is dangerous in that the entry title, expanded in
place of
.Ql %t ,
might be exploited by a malicious XML like the following:
.Bd -literal -offset indent
<?xml version="1.0" encoding="utf8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
 <channel>
  <item>
    <guid>inject</guid>
    <title>"; echo pwned; echo "</title>
  </item>
 </channel>
</rss>
.Ed
.Pp
The correct way of achieving the same result consists in moving the code
into a shell script, and making the entry properties available to it by
means of a safe, uninterpreted, parameter passing:
.Bd -literal -offset indent
feed foobar
  url https://example.conf/feed.xml
  handler exec
  command /usr/local/bin/count_bytes %t
.Ed
.It
Extra care should be taken when the expansion of a placeholder directly
determines the name of a file.
Consider, for example, the following configuration:
.Bd -literal -offset indent
feed foobar
  url https://example.conf/feed.xml
  handler pipe
  command sed -n w%t
.Ed
.Pp
This is an effective (yet dangerous) way of dumping the entry contents into
a file named after the entry title.
A specially crafted XML can exploit a similar configuration to attempt the
replacement of sensitive files:
.Bd -literal -offset indent
<?xml version="1.0" encoding="utf8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
 <channel>
  <item>
    <guid>shenanigans</guid>
    <title>.bashrc</title>
    <description>echo pwned</description>
  </item>
  <item>
    <guid>shenanigans_1</guid>
    <title>../.bashrc</title>
    <description>echo pwned</description>
  </item>
  <item>
    <guid>shenanigan's_2</guid>
    <title>../../.bashrc</title>
    <description>echo pwned</description>
  </item>
  ...
 </channel>
</rss>
.Ed
.Pp
The correct way of achieving the same result consists in using the
.Ql %n
placeholder in place of
.Ql %t ,
obtaining a safer (although admittedly less descriptive) file naming.
.\" .It
.\" If a feed is configured with the
.\" .Cm pipe
.\" handler, the subprocess
.\" .Xr stdin 3
.\" must be taken in account as attack surface.
.\" Special care should be taken when interpreting the content with tools that
.\" are meant to be used by a trusted user.
.\" .Pp
.\" As a noteworthy example of such behaviour, consider the
.\" .Sy tilde escapes
.\" features, supported by some implementations of the
.\" .Xr mail 1
.\" program.
.\" XXX explain.
.\".Pp
.\"See
.\".Xr crossbow-cookbook 7
.\"for a detailed example.
.El
.\" ---------------------------------------------------------------------
.Sh EXAMPLES
See
.Xr crossbow-cookbook 7 .
.\" ---------------------------------------------------------------------
.Sh SEE ALSO
.Xr crossbow 1 ,
.Xr crossbow-conf 5 ,
.Xr crossbow-cookbook 7
.\" ---------------------------------------------------------------------
.Sh AUTHORS
.An Giovanni Simoni Aq Mt dacav@fastmail.com
