.Dd October 20, 2021
.Dt CROSSBOW.CONF 5 URM
.Os \&
.\" ---------------------------------------------------------------------
.Sh NAME
.Nm crossbow.conf
.Nd configuration file for
.Xr crossbow 1
.\" ---------------------------------------------------------------------
.Sh DESCRIPTION
.Xr crossbow 1
attempts to load the configuration from
.Pa ~/.crossbow.conf ,
.Ev $XDG_CONFIG_HOME Ns
.Pa /crossbow.conf ,
or
.Pa ~/.config/crossbow.conf
in that order.
The configuration file is case sensitive.
Lines can be indented by any number of white-spaces (according to the
.Xr isspace 3
definition).
Empty lines, and lines starting with
.Ql #
are ignored.
.Pp
Settings are defined by statements in the form
.Bd -literal -offset xxxx
key value
.Ed
.Pp
A single white-space character separates the key from the value.
Everything that follows the separator, up to the next line-feed character
is considered part of the value, including any surrounding white-space.
.Pp
The configuration can define global settings and multiple feed sections,
one for each monitored feed.
Global settings are defined in the beginning of the configuration
file.
The first encountered
.Cm feed
key introduces the first feed section.
All the subsequent settings, up to the next occurrence of the
.Cm feed
key or the end of the file, affect the declared feed.
.Pp
Setting whose value represents a path on the filesystem are subject to
tilde expansion: if the value is prefixed with
.Ql ~/ ,
the
.Sl ~
character is expanded
with the content of the
.Ev $HOME
environment variable.
Relative paths are interpreted relative to the directory where
.Xr crossbow 1
is invoked.
.Ss Global settings
.Bl -tag -width x
.It Cm jobs Ar N
Define the maximum number of concurrent feed XML retrievals.
.Pp
Defaults to 10.
.It Cm persist_dir Ar path
Define the path where
.Xr crossbow 1
will persist the seen
entry identifiers for each monitored feed.
.Pp
Defaults to
.Pa ~/.crossbow .
.It Cm compat Ar v
Declare the compatibility value of the configuration file.
.Pp
The software execution will fail if the configuration
file is found to be marked with a compat value different from the
expected, or if the setting is missing.
If this happens, the expected compat value is printed on
.Xr stderr 3 .
The purpose of this setting is to avoid surprising behaviours
after the upgrade to a new release of
.Xr crossbow 1 .
See
.Sx Compatibility Migration
for information on how to migrate the configuration to be
compatible with the current release.
.El
.Ss Feed settings
.Bl -tag -width x
.It Cm chdir Ar path
If the feed handler is
.Cm exec
or
.Cm pipe ,
execute the subprocess in the given directory.
.Pp
Defaults to
.Pa ./
.It Cm command Ar format-string
If the feed handler is
.Cm exec
or
.Cm pipe ,
use
.Ar format-string
as a command template, expanding it into the argument vector of the
subprocess.
.Pp
The template is split on white-space, with the first item being the
command to execute and the subsequent items being the arguments.
The accepted syntax allows to use
.Xr printf 3 Ns -like
placeholders that are substituted with the
fields of the processed entry, so that they can be made available to the
subprocess.
.Pp
.Em Security remark :
Doing proper shell escaping against untrusted input is really hard which is why
the template is not parsed by means of a shell interpreter.
Constructing an interpreted code snippet within a command template
(e.g. by invoking
.Ql sh -c )
is strongly discouraged, as it might result in accidental or malicious
code injection.
.Pp
See
.Xr crossbow-format 5 .
.It Cm feed Ar name
Declare a new feed.
.Pp
This setting is special in that it marks the beginning of a feed
section in the configuration file.
Every settings that follows, up to the next feed section or to the end of
file (whichever comes first) will be applied to the declared feed.
.Pp
The provided name identifies the feed.
All feeds must have distinct names.
Feed names are forbidden to contain the
.Ql /
character.
.It Cm format Ar format-string
If the feed handler is
.Cm print ,
use
.Ar format-string
as a template for the textual representation of each
handled feed entry.
The template uses the same
.Xr printf 3 Ns -like
syntax as the
.Cm command
setting.
See
.Xr crossbow-format 5 .
.Pp
This setting is optional: if not provided, the print handler will use the
default representation.
.It Cm handler Ar handler
Use the selected
.Ar handler
to process new feed entries.
The
.Ar handler
argument must be one of the following:
.Bl -tag -width x
.It Cm print
Print a textual representation of each new entry to
.Xr stdout 3 .
A custom template can optionally be defined by specifying a
.Cm format
setting.
.Pp
This is the default handler for feeds that do not define one.
.It Cm exec
Handle new entries by means of a subprocess.
This handler requires the
.Cm command
setting to be defined.
.It Cm pipe
Like
.Cm exec ,
but the content (corresponding to the
.Ql %c
placeholder, see
.Xr crossbow-format 5 Ns )
is also made available to the subprocess by means of a
.Xr pipe 2 ,
This handler requires the
.Cm command
setting to be defined.
.El
.It Cm url Ar URL
Define the URL of the feed XML.
.Pp
The allowed remote protocols are HTTP, HTTPS, and Gopher (using the
.Ql http:// ,
.Ql https://
and
.Ql gopher://
URL schemas respectively).
Local file-system access (using
.Ql file:// Ns )
is possible too.
HTTPS is the default protocol when no schema is specified.
.Pp
This setting must be defined for each feed.
.El
.\" ---------------------------------------------------------------------
.Ss Compatibility Migration
This section explains how to migrate the configuration from
compat value 0 to 1.
By compat value 0 here we mean a configuration compatible with
.Xr crossbow 1
3.0.0, although versions before 4.0.0 do not recognize any versioning
information in the configuration file.
.Pp
Concerning the format strings
(See
.Xr crossbow-format 5 Ns ):
.Bl -bullet
.It
The following placeholders are no longer available:
.Ql %cm
.No ( Ns
.Sq contributor email Ns
),
.Ql %cr
.No ( Ns
.Sq rights Ns
),
.Ql %ct
.No ( Ns
.Sq rights type Ns
),
.Ql %cu
.No ( Ns
.Sq contributor uri Ns
),
.Ql %d
.No ( Ns
.Sq summary
/
.Sq content Ns
),
.Ql %dt
.No ( Ns
.Sq summary type
/
.Sq content type Ns
),
.Ql %el
.No ( Ns
.Sq enclosure length Ns
),
.Ql %en
.No ( Ns
.Sq enclosure Ns
),
.Ql %et
.No ( Ns
.Sq enclosure type Ns
),
.Ql %eu
.No ( Ns
.Sq enclosure url Ns
),
.Ql %sr
.No ( Ns
.Sq source Ns
),
.Ql %su
.No ( Ns
.Sq source url Ns
),
.Ql %tt
.No ( Ns
.Sq title type Ns
).
.Pp
.It
.Ql %c
.No ( Sq contributor name )
has been redefined as
.Sq content .
.It
The following placeholders have been added:
.Ql %ca
.No ( Ns
.Sq category Ns
),
.Ql %s
.No ( Ns
.Sq summary Ns
).
.El
.Pp
Furthermore, care should be taken concerning the use of the
.Cm pipe
handler, since the new implementation takes advantage of the
.Ql <content:encoded>
element (extension of RSS).
.Ql <content:encoded>
is preferable to
.Ql <content>
because it is more likely to contain full article, but as a
drawback it might turn out to be HTML-encoded.
.\" ---------------------------------------------------------------------
.Sh SEE ALSO
.Xr crossbow 1 ,
.Xr crossbow-format 5 ,
.Xr crossbow-cookbook 7
.\" ---------------------------------------------------------------------
.Sh AUTHORS
.An Giovanni Simoni Aq Mt dacav@fastmail.com
