.Dd October  9, 2021
.Dt CROSSBOW 7 URM
.Os \&
.\" ---------------------------------------------------------------------
.Sh NAME
.Nm crossbow-cookbook
.Nd
cookbookish examples of
.Xr crossbow 1
usage
.\" ---------------------------------------------------------------------
.Sh DESCRIPTION
This manual page contains short recipes demonstrating how to use the
.Nm crossbow
feed aggregator.
.Ss Table of contents
.Bl -enum -compact
.It
Simple local mail notification
.It
Incremental files collection
.It
Download the full article
.It
One mail per entry
.It
Maintain a local multimedia collection
.El
.\" ---------------------------------------------------------------------
.Sh EXAMPLES
.Ss Simple local mail notification
We want a periodic notification, via local mail, of the availability of
new stories on a site.
.Pp
The configuration in
.Xr crossbow.conf 5
would look like this:
.Bd -literal -offset indent
feed debian_micro
  url https://micronews.debian.org/feeds/feed.rss
  format %ft: %l\en
.Ed
.Pp
The invocation of
.Xr crossbow 1
will emit on
.Xr stdout 3
a line like the following for each new item:
.Bd -literal -offset indent
Debian micronews: https://micronews.debian.org/....html
.Ed
.Pp
By placing the following string in a
.Xr crontab 5 ,
a check for updates will be run automatically every two hours:
.Bd -literal -offset xxxx
0 0-23/2 * * * crossbow
.Ed
.Pp
Assuming that local mail delivery is enabled, and since the output of a
cronjob is mailed to the owner of the
.Xr crontab 5 ,
the user will receive a mail with one line for each entry that appeared in
the last two hours.
.Ss Incremental files collection
Let's consider a feed whose XML reports the whole article for each entry.
We want to store individual articles in a separate file, under a specific
directory on the filesystem.
.Pp
The configuration in
.Xr crossbow.conf 5
would look like this:
.Bd -literal -offset indent
feed cosmic.voyage
  url gopher://cosmic.voyage:70/0/atom.xml
  handler pipe
  command sed -n w%n.txt
  chdir ~/scifi_stories/cosmic.voyage/
.Ed
.Pp
The invocation of
.Xr crossbow 1
will spawn one
.Xr sed 1
process for each new entry.
The content, corresponding to the
.Cm %d
placeholder, will be piped to the subprocess.
This in turn will write it on the specified file
.Pq Cm w No command ,
but not on
.Xr stdout 3
.Pq Fl n No flag .
.Pp
As a result, the
.Pa ~/scifi_stories/cosmic.voyage
directory will be populated with files named
.Pa 000000.txt ,
.Pa 000001.txt ,
.Pa 000002.txt ,
\&...etc, since
.Cm %n
is expanded with an incremental numeric value.
See
.Xr crossbow-format 5 .
.Pp
.Em Security remark :
unless the feed is trusted, it is strongly discouraged to name filesystem
paths after entry properties others than
.Cm %n .
Consider for example the case where
.Cm %t
is used as a file name, and the title of a post is something like
.Pa ../../.profile .
.Cm %n
is safe to use, since its value is not dependent on the feed content.
.Ss Download the full article
This scenario is similar to the previous one, but it tackles the situation
where the feed entry does not contain the full content, while the entry's
link field contains a valid URL, which is intended to be reached by means
of a web browser.
.Pp
In this case we can leverage
.Xr curl 1
to do the retrieval:
.Bd -literal -offset indent
feed debian_micro
  url https://micronews.debian.org/feeds/feed.rss
  handler exec
  command curl -o %n.html %l
  chdir ~/debian_micronews/
.Ed
.Pp
The
.Qq %n
and
.Qq %l
placeholders do not need to be quoted: they are handled safely even when
their expansions contain white spaces.
See
.Xr crossbow-format 5 .
.Pp
It is of course possible to use any non-interactive download manager in
place of
.Xr curl 1 ,
or maybe a specialized script that fetches the entry link and scrapes the
content out of it.
.\" --- example
.Ss One mail per entry
We want to turn individual feed entries into plain (HTML-free) text
messages, and deliver them via email.
.Pp
Our goal can be achieved by means of a generic shell script like
the following:
.Bd -literal -offset indent
#!/bin/sh

set -e

feed_title="$1"
post_title="$2"
link="$3"

lynx "${link:--stdin}" -dump -force_html |
    sed "s/^~/~~/" |    # Escape dangerous tilde expressions
    mail -s "${feed_title:+${feed_title}: }${post_title:-...}" "${USER:?}"
.Ed
.Pp
The script can be installed in the
.Ev PATH ,
e.g. as
.Pa /usr/local/bin/crossbow-to-mail ,
and then integrated in
.Xr crossbow 1
as follows:
.Bl -bullet
.It
If the tracked feed encloses the whole content in the XML:
.Bd -literal -offset indent
feed debian_micro
  url https://micronews.debian.org/feeds/feed.rss
  handler pipe
  command crossbow-to-mail %ft %t
.Ed
.It
If the feed entries only relay the link to the article:
.Bd -literal -offset indent
feed lobsters.c
  url https://lobste.rs/t/c.rss
  handler exec
  command crossbow-to-mail %ft %t %l
.Ed
.El
.Pp
.Em Note :
The
.Pa crossbow-to-mail
script leverages
.Xr lynx 1
to download and parse the HTML into textual form.
Any other
.Pp
.Em Security remark :
The
.Qq s/^~/~~/
.Xr sed 1
regex prevents accidental or malicious
.Em tilde escapes
from being interpreted by the
.Xr mail 1
program.
The
.Xr mutt 1
mail user agent, if available, can be used as a safer drop-in replacement.
.\" --- example
.Ss Maintain a local multimedia collection
Many sites specialized in multimedia delivery can be scraped using tools
such as
.Xr youtube-dl 1 .
If the web site allows the subscription of a feed,
.Xr crossbow 1
can be combined with these tools in order to maintain incrementally a
local collection of files.
.Pp
For example, YouTube provides feeds for users, channels and playlists.
Each of these entities is assigned with a unique identifier, which can be
easily figured by looking at the web URL.
.Bl -bullet
.It
Given a user identifier
.Ar UID ,
the feed is
.Sy https://youtube.com/feeds/videos.xml?user= Ns Ar UID
.It
Given a channel identifier
.Ar CID ,
the feed is
.Sy https://youtube.com/feeds/videos.xml?channel_id= Ns Ar CID
.It
Given a playlist identifier
.Ar PID ,
the feed is
.Sy https://youtube.com/feeds/videos.xml?playlist_id= Ns Ar PID
.El
.Pp
What follows is a convenient wrapper script that ensures proper file
naming (although it is always wiser to use
.Cm %n Ns
, as explained above):
.Bd -literal -offset indent
#!/bin/sh

link="${1:?missing link}"
incremental_id="${2:?missing incremental id}"
format="$3"

# Transform a title in a reasonably safe 'slug'
slugify() {
    tr -d \e\en |                     # explicitly drop new-lines
    tr /[:punct:][:space:] . |      # turn all sly chars into dots
    tr -cs [:alnum:]                # squeeze repetitions
}

fname="$(
    youtube-dl \e
        --get-filename \e
        -o "%(id)s_%(title)s.%(ext)s" \e
        "$link"
)" || exit 1

youtube-dl \e
    ${format:+-f "$format"} \e
    -o "$(printf %s_%s "$incremental_id" "$fname" | slugify)" \e
    --no-progress \e
    "$link"
.Ed
.Pp
Once again, the script can be installed in the
.Ev PATH ,
e.g. as
.Pa /usr/local/bin/crossbow-ytdl ,
and then integrated in
.Xr crossbow 1
as follows:
.Bl -bullet
.It
To save each published video:
.Bd -literal -offset indent
feed computerophile
  url https://youtube.com/feeds/videos.xml?user=Computerphile
  handler exec
  command crossbow-ytdl %l %n
.Ed
.It
To save only the audio of each published video:
.Bd -literal -offset indent
feed nodumb
  url https://youtube.com/feeds/videos.xml?channel_id=UCVnIvJuTZqM5nnwGFpA57_Q
  handler exec
  command crossbow-ytdl %l %n
.Ed
.El
.\" ---------------------------------------------------------------------
.Sh SEE ALSO
.Xr crossbow 1 ,
.Xr lynx 1 ,
.Xr sed 1 ,
.Xr youtube-dl 1 ,
.Xr crontab 5 ,
.Xr cron 8
.\" ---------------------------------------------------------------------
.Sh AUTHORS
.An Giovanni Simoni Aq Mt dacav@fastmail.com
