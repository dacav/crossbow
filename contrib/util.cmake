function(crossbow_load_project_version OutVersion OutVersionStr)
    set(VersionFile ${CMAKE_SOURCE_DIR}/.versionstr)
    set(GitDir ${CMAKE_SOURCE_DIR}/.git)

    if (EXISTS ${VersionFile})
        file(STRINGS ${VersionFile} VersionStr)
    elseif (IS_DIRECTORY ${GitDir})
        execute_process(
            COMMAND git;describe;--always;--dirty
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE VersionStr
            ECHO_ERROR_VARIABLE
            OUTPUT_STRIP_TRAILING_WHITESPACE
        )
    else()
        message(FATAL_ERROR
          "Could not determine version, see project README for more info."
        )
    endif()
    set(${OutVersionStr} ${VersionStr} PARENT_SCOPE)

    string(REGEX REPLACE "^([0-9]+\\.[0-9]+\\.[0-9]+).*" "\\1" Version ${VersionStr})
    set(${OutVersion} ${Version} PARENT_SCOPE)
endfunction()

function(enable_san Target)
    target_compile_options(${Target} BEFORE
        PRIVATE -fsanitize=address
        PRIVATE -fsanitize=undefined
    )
    target_link_libraries(${Target} PRIVATE
        -fsanitize=address
        -fsanitize=undefined
    )
endfunction()
