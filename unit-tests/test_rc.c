#include "unittest.h"

#define COMPAT "compat 1\n"

#include <err.h>
#include <stdbool.h>

#include "rc.h"
#include "test_counters.h"

static struct rc *load_(const char *str, size_t len)
{
    struct rc *rc;

    rc = rc_new();
    if (!rc)
        return NULL;

    if (rc_parse_buffer(rc, str, len)) {
        warnx("rc_parse_str failed");
        rc_free(rc);
        return NULL;
    }

    return rc;
}

#define load(str) load_((str), sizeof(str) - 1)

static int same_string(const char *s1, const char *s2)
{
    return (s1 == NULL) == (s2 == NULL) && (!s1 || strcmp(s1, s2) == 0);
}

static int test_compat(struct util_ctx *ctx, intptr_t opaque)
{
    struct rc *rc;
    int e = -1;

    rc = load("feed boola\n"
              "url file://localhost/etc/etc\n");
    EXPECT_OR_EXIT(!rc);

    rc = load(COMPAT "feed boola\n"
              "url file://localhost/etc/etc\n");
    EXPECT_OR_EXIT(rc);

    e = 0;
exit:
    rc_free(rc);
    return e;
}

static int test_whole(struct util_ctx *ctx, intptr_t opaque)
{
    struct rc *rc;
    int e = -1;

    const struct rc_feed expected[] = {
        {
            .name = "dacav",
            .url = "https://dacav.roundhousecode.com/feed.rss",
            .handler = rc_hdl_pipe,
            .ofmt.spec = "aaa",
            .ofmt.len = 3,
        },
        {
            .name = "lobsters.unix ",
            .url = "https://lobste.rs/t/unix.rss",
            .handler = rc_hdl_print,
            .ofmt.spec = "bbb",
            .ofmt.len = 3,
        },
        {
            .name = "lobsters.security",
            .url = "https://lobste.rs/t/security.rss",
            .handler = rc_hdl_print,
            .chdir = "/home/xxx/yyy",
        },
        {
            .name = "lobsters.c",
            .url = "https://lobste.rs/t/c.rss",
            .handler = rc_hdl_exec,
            .ofmt.spec = "ccc",
            .ofmt.len = 3,
        },
        {
            .name = "all_defaults",
            .url = "all_defaults",
            .handler = rc_hdl_print,
        },
        {
            .name = "auto_handler_1",
            .url = "auto_handler_1",
            .handler = rc_hdl_print,
            .ofmt.spec = "ddd",
            .ofmt.len = 3,
        },
        {
            .name = "auto_handler_2",
            .url = "auto_handler_2",
            .handler = rc_hdl_exec,
            .ofmt.spec = "eee",
            .ofmt.len = 3,
        }};

    test_reset_counters();

    rc = load(COMPAT
              "jobs 1337\n"
              "persist_dir /home/xxx/.feeds\n"
              "feed dacav\n"
              "url https://dacav.roundhousecode.com/feed.rss\n"
              "handler pipe\n"
              "command aaa\n"
              "# a lonely comment\n"
              "feed lobsters.unix \n" /* testing space in feed name */
              "url https://lobste.rs/t/unix.rss\n"
              "handler print\n"
              "format bbb\n"
              "  \n"
              "# a longer comment\n"
              "# on multiple lines\n"
              "feed lobsters.security\n"
              "url https://lobste.rs/t/security.rss\n"
              "handler print\n"
              "chdir /home/xxx/yyy\n"
              "\n"
              "feed lobsters.c\n"
              "  # indentation is fine for comment,\n"
              "  # and for other things too.\n"
              "  url https://lobste.rs/t/c.rss\n"
              "  handler exec\n"
              "  command ccc\n"
              "\n"
              "feed all_defaults\n"
              "  # if the handler is not defined, it defines to print\n"
              "  url all_defaults\n"
              "\n"
              "feed auto_handler_1\n"
              "  # if format is defined, the print handler is implied\n"
              "  url auto_handler_1\n"
              "  format ddd\n"
              "\n"
              "feed auto_handler_2\n"
              "  # if command is defined, the exec handler is implied\n"
              "  url auto_handler_2\n"
              "  command eee\n"
              "\n");
    EXPECT_OR_EXIT(rc);

    const unsigned N = rc_n_feeds(rc);
    EXPECT(test_read_counter("count_finalized_feeds") == N);

    for (unsigned i = 0; i < N; i++) {
        const struct rc_feed *rcf = rc_get_feed(rc, i);

        EXPECT_OR_EXIT(i < sizeof(expected) / sizeof(*expected));
        EXPECT_OR_EXIT(same_string(expected[i].name, rcf->name));
        EXPECT_OR_EXIT(same_string(expected[i].url, rcf->url));
        EXPECT_OR_EXIT(expected[i].handler == rcf->handler);
        EXPECT_OR_EXIT(same_string(expected[i].ofmt.spec, rcf->ofmt.spec));
        EXPECT_OR_EXIT(expected[i].ofmt.len == rcf->ofmt.len);
        EXPECT_OR_EXIT(same_string(expected[i].chdir, rcf->chdir));
    }
    EXPECT_OR_EXIT(rc_get_global(rc)->jobs == 1337);
    EXPECT_OR_EXIT(
        same_string(rc_get_global(rc)->persist_dir, "/home/xxx/.feeds"));

    e = 0;
exit:
    rc_free(rc);
    return e;
}

static bool verify(struct rc *rc, const char *str, size_t len, bool should_fail)
{
    if (should_fail == !!rc_parse_buffer(rc, str, len))
        return true;

    warnx("expected %sto fail, but does not: [[\n%.*s\n",
          should_fail ? "" : "not ",
          (int)len,
          str);
    warnx("]]. follows dump");
    rc_dump(rc);
    return false;
}

#define is_bad(rc, str) verify(rc, COMPAT str, sizeof(COMPAT str) - 1, true)
#define is_good(rc, str) verify(rc, COMPAT str, sizeof(COMPAT str) - 1, false)

static int test_error_checking(struct util_ctx *ctx, intptr_t opaque)
{
    struct rc *rc;

    rc = rc_new();
    EXPECT(rc);

    test_reset_counters();

    EXPECT_OR_EXIT(is_bad(rc, "feed bu/la\n"));
    EXPECT_OR_EXIT(test_read_counter("bad_feed_name") == 1);

    EXPECT_OR_EXIT(is_bad(rc, "feed bula\n"));
    EXPECT(test_read_counter("missing_url") == 1);

    /* it is ok, the handler defaults to print, that doesn't need ofmt_spec */
    EXPECT_OR_EXIT(is_good(rc,
                           "feed boola\n"
                           "url file://localhost/etc/etc\n"))

    EXPECT_OR_EXIT(is_bad(rc,
                          "feed bula\n"
                          "url file://localhost/etc/etc\n"
                          "handler exec\n"));
    EXPECT(test_read_counter("missing_ofmt_spec_exec") == 1);

    EXPECT_OR_EXIT(is_good(rc,
                           "feed bula\n"
                           "url file://localhost/etc/etc\n"
                           "handler exec\n"
                           "command echo %a %b %c\n"));

    EXPECT_OR_EXIT(is_bad(rc,
                          "feed bula\n"
                          "url file://localhost/etc/etc\n"
                          "handler pipe\n"));
    EXPECT(test_read_counter("missing_ofmt_spec_pipe") == 1);

    EXPECT_OR_EXIT(is_good(rc,
                           "feed bula\n"
                           "url file://localhost/etc/etc\n"
                           "handler pipe\n"
                           "command echo %a %b %c\n"));

    EXPECT_OR_EXIT(is_bad(rc,
                          "jobs 1337\n"
                          "feed leet\n"
                          "url file://example.com/feed.xml\n"
                          "jobs 1338\n" /* this is global, will be rejected */
                          ));
    EXPECT(test_read_counter("fail_unmatched_feed_opt") == 1);

    EXPECT_OR_EXIT(is_bad(rc,
                          "jobs 1337\n"
                          "handler print\n" /* this is feed, will be rejected */
                          "feed leet\n"
                          "jobs 1338\n"));
    EXPECT(test_read_counter("fail_unmatched_global_opt") == 1);

    EXPECT_OR_EXIT(
        is_bad(rc,
               "feed not_guessing\n"
               "  # Both command and format are defined, but no handler is\n"
               "  # specified\n"
               "  command xxx\n"
               "  format yyy\n"));
    EXPECT(test_read_counter("fail_both_command_and_format") == 1);

exit:
    rc_free(rc);
    return 0;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_compat, NULL),
                                  TEST(1, test_whole, NULL),
                                  TEST(1, test_error_checking, NULL),
                                  END_TESTS};
    return tests;
}
