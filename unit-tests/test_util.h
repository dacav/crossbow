#pragma once

#include <sys/types.h>

void util_test_setup(const char *test_name);
void util_test_teardown();
