#include "digest.h"

#include <stdlib.h>

#include "unittest.h"
#include "util.h"

// shasum </dev/null | xxd -p -r - | xxd -i -c4
const uint8_t sha1_empty[] = {0xda, 0x39, 0xa3, 0xee, 0x5e, 0x6b, 0x4b,
                              0x0d, 0x32, 0x55, 0xbf, 0xef, 0x95, 0x60,
                              0x18, 0x90, 0xaf, 0xd8, 0x07, 0x09};

// printf 'hello world' | shasum | xxd -p -r - | xxd -i -c4
const uint8_t sha1_hello_world[] = {0x2a, 0xae, 0x6c, 0x35, 0xc9, 0x4f, 0xcf,
                                    0xb4, 0x15, 0xdb, 0xe9, 0x5f, 0x40, 0x8b,
                                    0x9c, 0xe9, 0x1e, 0xe8, 0x46, 0xed};

#if 0
static void print_hex(uint8_t c)
{
    const char digits[] = "0123456789abcdef";
    fputc(digits[(c & 0xf0) >> 4], stderr);
    fputc(digits[(c & 0x0f) >> 0], stderr);
}

static void print_hexstr(const uint8_t *c, size_t n)
{
    for (size_t i = 0; i < n; i++)
        print_hex(c[i]);
    fputc('\n', stderr);
}
#endif

static_assert(digest_buflen == 20,
              "digest_buflen must reflect the digest algorithm");

static int test_base(struct util_ctx *ctx, intptr_t opaque)
{
    struct digest *digest;
    uint8_t buffer[digest_buflen];
    int e = -1;

    digest = digest_new();
    EXPECT_OR_EXIT(digest);

    EXPECT_OR_EXIT(digest_finish(digest, buffer, sizeof buffer) == 0);
    EXPECT_OR_EXIT(memcmp(buffer, sha1_empty, sizeof buffer) == 0);

    /* Reset is required, so a new call to finish will fail. */
    EXPECT_OR_EXIT(digest_finish(digest, buffer, sizeof buffer) != 0);

    /* After reset we can start over. */
    EXPECT_OR_EXIT(digest_reset(digest) == 0);
    EXPECT_OR_EXIT(digest_finish(digest, buffer, sizeof buffer) == 0);
    EXPECT_OR_EXIT(memcmp(buffer, sha1_empty, sizeof buffer) == 0);

    e = 0;
exit:
    digest_del(digest);
    return e;
}

static int test_incremental(struct util_ctx *ctx, intptr_t opaque)
{
    struct digest *digest;
    uint8_t buffer[digest_buflen];
    int e = -1;

    digest = digest_new();
    EXPECT_OR_EXIT(digest);

    /* Incremental addition to the checksum */
    EXPECT_OR_EXIT(
        digest_add(digest, (const uint8_t *)"hell", strlen("hell")) == 0);
    EXPECT_OR_EXIT(
        digest_add(digest, (const uint8_t *)"o world", strlen("o world")) == 0);
    EXPECT_OR_EXIT(digest_finish(digest, buffer, sizeof buffer) == 0);
    EXPECT_OR_EXIT(memcmp(buffer, sha1_hello_world, sizeof buffer) == 0);

    e = 0;
exit:
    digest_del(digest);
    return e;
}

static int test_robust(struct util_ctx *ctx, intptr_t opaque)
{
    /* Testing robustness of the API.
     * Reset right after creation (note: the tests are supposed to be run with
     * -fsanitize=address, so a this test is expected to cover memory leaks
     */

    struct digest *digest;
    int e = -1;

    digest = digest_new();
    EXPECT_OR_EXIT(digest);

    /* Reset right after creation */
    EXPECT_OR_EXIT(digest_reset(digest) == 0);

    /* Reset after addition, before finish */
    EXPECT_OR_EXIT(
        digest_add(digest, (const uint8_t *)"hello", strlen("hello")) == 0);
    EXPECT_OR_EXIT(digest_reset(digest) == 0);

    e = 0;
exit:
    digest_del(digest);
    return e;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_base, NULL),
                                  TEST(1, test_incremental, NULL),
                                  TEST(1, test_robust, NULL),
                                  END_TESTS};
    return tests;
}
