#include "text.h"
#include "unittest.h"

#include <err.h>
#include <string.h>

#define EXPECT_TEXT_SHORT(expected, actual)                                    \
    do {                                                                       \
        static unsigned short test_cnt;                                        \
        const char *result = (actual);                                         \
                                                                               \
        warnx("%hu: [%s] VS [%s]", test_cnt++, expected, result);              \
        EXPECT(strcmp(expected, actual) == 0);                                 \
    } while (0)

static int test_text_short(struct util_ctx *ctx, intptr_t opaque)
{
    const char src[] = "abcdef";
    char dst[7];

    (void)ctx;

    // Handling of bad parameters.
    EXPECT_TEXT_SHORT("", text_short(NULL, sizeof(dst), src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, 0, src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, sizeof(dst), NULL, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, sizeof(dst), src, 0));

    EXPECT_TEXT_SHORT("abcdef", text_short(dst, sizeof(dst), src, text_whole));
    EXPECT_TEXT_SHORT("ab...",
                      text_short(dst, sizeof(dst) - 1, src, text_whole));
    EXPECT_TEXT_SHORT("a...",
                      text_short(dst, sizeof(dst) - 2, src, text_whole));
    EXPECT_TEXT_SHORT("...", text_short(dst, sizeof(dst) - 3, src, text_whole));
    EXPECT_TEXT_SHORT("..", text_short(dst, sizeof(dst) - 4, src, text_whole));
    EXPECT_TEXT_SHORT(".", text_short(dst, sizeof(dst) - 5, src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, sizeof(dst) - 6, src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, 0, src, text_whole));

    EXPECT_TEXT_SHORT("abcdef", text_short(dst, sizeof(dst), src, text_whole));
    EXPECT_TEXT_SHORT("ab...",
                      text_short(dst, sizeof(dst) - 1, src, text_whole));
    EXPECT_TEXT_SHORT("a...",
                      text_short(dst, sizeof(dst) - 2, src, text_whole));
    EXPECT_TEXT_SHORT("...", text_short(dst, sizeof(dst) - 3, src, text_whole));
    EXPECT_TEXT_SHORT("..", text_short(dst, sizeof(dst) - 4, src, text_whole));
    EXPECT_TEXT_SHORT(".", text_short(dst, sizeof(dst) - 5, src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, sizeof(dst) - 6, src, text_whole));
    EXPECT_TEXT_SHORT("", text_short(dst, 0, src, text_whole));

    return 0;
}

static int test_text_join_argv(struct util_ctx *ctx, intptr_t opaque)
{
    const char *argv[] = {"sed", "-n", "s/foo/bar/p", NULL};
    char to_buf[19];
    const char *result;

    result = text_join_argv(to_buf, sizeof(to_buf), argv);
    warnx("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo/bar/p") == 0);

    result = text_join_argv(to_buf, sizeof(to_buf) - 1, argv);
    warnx("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo/b...") == 0);

    result = text_join_argv(to_buf, sizeof(to_buf) - 8, argv);
    warnx("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n ...") == 0);

    return 0;
}

static int test_text_join_argv_spaces(struct util_ctx *ctx, intptr_t opaque)
{
    const char *argv[] = {"sed", "-n", "s/foo /bar/p", NULL};
    char to_buf[21];
    const char *result;

    result = text_join_argv(to_buf, sizeof(to_buf), argv);
    warnx("-> [%s]", result);
    EXPECT(strcmp(result, "sed -n s/foo\\ /bar/p") == 0);

    return 0;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_text_short, NULL),
                                  TEST(1, test_text_join_argv, NULL),
                                  TEST(1, test_text_join_argv_spaces, NULL),
                                  END_TESTS};
    return tests;
}
