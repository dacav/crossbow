#include "outfmt.h"

#include <err.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/wait.h>
#include <unistd.h>

#include "unittest.h"
#include "util.h"

#if 0
#define DEBUG_P(fmt, ...) warnx(__FILE__ ":%d " fmt, __LINE__, __VA_ARGS__)
#else
#define DEBUG_P(...)
#endif

#define OFMT_COMPILE_OK(ofmt, mode, spec)                                      \
    EXPECT_OR(ofmt_compile((ofmt), (mode), (spec), sizeof(spec) - 1) == 0,     \
              ofmt_print_error(ofmt));

#define OFMT_COMPILE_FAIL(ofmt, mode, spec)                                    \
    EXPECT_OR(ofmt_compile((ofmt), (mode), (spec), sizeof(spec) - 1) != 0,     \
              ofmt_print_error(ofmt));

struct test_params {
    const char *spec;
    size_t spec_len;
    const char *expected;
    size_t expected_len;
};

struct test_setup {
    struct ofmt *ofmt;
    int pipe_r, pipe_w;

    const char *resolved_pipe;
    const char *resolved_t;
    const char *resolved_a;
    const char *resolved_am;
    const char *resolved_x;
    const char *resolved_xn;
};

static const char *resolve_pipe(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_pipe;
}

static const char *resolve_t(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_t;
}

static const char *resolve_a(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_a;
}

static const char *resolve_am(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_am;
}

static const char *resolve_x(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_x;
}

static const char *resolve_xn(const void *item, void *opaque)
{
    const struct test_setup *ts = item;
    return ts->resolved_xn;
}

static int test_reject_unresolved(struct util_ctx *ctx, intptr_t opaque)
{
    struct ofmt *ofmt;

    enum ofmt_mode mode = (enum ofmt_mode)opaque;

    /* Note: %x is not a placeholder, as it is escpaed */
    const char spec[] = "hello \\%x %t world %a\\n";

    EXPECT(ofmt = ofmt_new());

    OFMT_COMPILE_FAIL(ofmt, mode, spec);
    EXPECT(ofmt_get_error(ofmt)->reason == ofmt_fail_undefined_resolver);

    EXPECT(ofmt_set_resolver(ofmt, "t", resolve_t) == 0);
    OFMT_COMPILE_FAIL(ofmt, mode, spec);
    EXPECT(ofmt_get_error(ofmt)->reason == ofmt_fail_undefined_resolver);

    EXPECT(ofmt_set_resolver(ofmt, "a", resolve_a) == 0);
    OFMT_COMPILE_OK(ofmt, mode, spec);

    ofmt_del(ofmt);
    return 0;
}

#define T(s, e)                                                                \
    (struct test_params)                                                       \
    {                                                                          \
        .spec = (s), .spec_len = sizeof s - 1, .expected = (e),                \
        .expected_len = sizeof e - 1,                                          \
    }

static int pipe_read_compare(const struct test_setup *ts,
                             const char *expected,
                             size_t len)
{
    char buffer[1024];
    ssize_t n;

    DEBUG_P("%s", __func__);
    EXPECT_OR(sizeof buffer >= len, warnx("static size is not enough!"));

    n = read(ts->pipe_r, buffer, sizeof buffer);
    DEBUG_P("  read -> (%.*s) %zd, expected (%.*s) %zu",
            (int)n,
            buffer,
            n,
            (int)len,
            expected,
            len);
    EXPECT((size_t)n == len);
    EXPECT(memcmp(expected, buffer, len) == 0);

    return 0;
}

static void teardown(struct test_setup *tsetup)
{
    if (tsetup->pipe_r != -1)
        close(tsetup->pipe_r);
    if (tsetup->pipe_w != -1)
        close(tsetup->pipe_w);
    ofmt_del(tsetup->ofmt);
}

static int setup(struct test_setup *out_ts, enum ofmt_mode mode)
{
    struct test_setup ts = {
        .pipe_r = -1,
        .pipe_w = -1,
        .resolved_t = "RESOLVED TITLE",
        .resolved_a = "RESOLVED AUTHOR",
        .resolved_am = "RESOLVED AUTHOR MAIL",
        .resolved_x = "RESOLVED X",
        .resolved_xn = "RESOLVED XN",
    };
    int pipefd[2];

    ts.ofmt = ofmt_new();
    if (!ts.ofmt) {
        warn("ofmt_new failed");
        goto fail;
    }

    EXPECT(ofmt_set_resolver(ts.ofmt, "a", resolve_a) == 0);
    EXPECT(ofmt_set_resolver(ts.ofmt, "t", resolve_t) == 0);
    EXPECT(ofmt_set_resolver(ts.ofmt, "am", resolve_am) == 0);
    EXPECT(ofmt_set_resolver(ts.ofmt, "x", resolve_x) == 0);
    EXPECT(ofmt_set_resolver(ts.ofmt, "xn", resolve_xn) == 0);

    if (pipe(pipefd) == -1) {
        warn("pipe failed");
        goto fail;
    }

    ts.pipe_r = pipefd[0];
    ts.pipe_w = pipefd[1];

    if (mode == ofmt_mode_print) {
        /* in print-mode we don't rely on external processes: this process
         * supplies both the write end and read end.  We prevent accidental
         * lockage by using non-blocking pipes. */
        if (fcntl(pipefd[0], F_SETFL, O_NONBLOCK) == -1)
            goto fail;
        if (fcntl(pipefd[1], F_SETFL, O_NONBLOCK) == -1)
            goto fail;
    }

    *out_ts = ts;
    return 0;

fail:
    teardown(&ts);
    FAIL("unable to setup");
}

static int evaluate(const struct test_setup *ts)
{
    return ofmt_evaluate(ts->ofmt,
                         &(const struct ofmt_evaluate_params){
                             .item = (void *)ts,
                             .opt_print_fd = &ts->pipe_w,
                         });
}

static int test_mode_print(struct util_ctx *ctx, intptr_t opaque)
{
    const struct test_params *tp = (const struct test_params *)opaque;
    struct test_setup ts;
    enum ofmt_mode mode = ofmt_mode_print;

    EXPECT(setup(&ts, mode) == 0);

    EXPECT(ofmt_compile(ts.ofmt, mode, tp->spec, tp->spec_len) != -1);
    EXPECT(evaluate(&ts) != -1);

    EXPECT(pipe_read_compare(&ts, tp->expected, tp->expected_len) != -1);
    teardown(&ts);
    return 0;
}

static int run(const struct test_setup *ts)
{
    int save_stdout;

    save_stdout = dup(STDOUT_FILENO);
    EXPECT(save_stdout != -1);
    EXPECT(dup2(ts->pipe_w, STDOUT_FILENO) == STDOUT_FILENO);

    if (evaluate(ts) == -1)
        FAIL(ofmt_fail_str(ofmt_get_error(ts->ofmt)->reason));

    EXPECT(dup2(save_stdout, STDOUT_FILENO) == STDOUT_FILENO);
    close(save_stdout);

    return 0;
}

static int test_mode_subprocess(struct util_ctx *ctx, intptr_t opaque)
{
    const struct test_params *tp = (const struct test_params *)opaque;
    struct test_setup ts;
    enum ofmt_mode mode = ofmt_mode_exec;

    EXPECT(setup(&ts, mode) == 0);

    EXPECT(ofmt_compile(ts.ofmt, mode, tp->spec, tp->spec_len) != -1);
    EXPECT(run(&ts) == 0);
    EXPECT(pipe_read_compare(&ts, tp->expected, tp->expected_len) != -1);
    teardown(&ts);
    return 0;
}

/* This test verifies the existence of a proper boundary for the amount of
 * segments used for subprocess launching ('max_segments'). */
static int test_limits_subprocess_1(struct util_ctx *ctx, intptr_t opaque)
{
    enum {
        /* the value of this constant must match the value
         * of the corresponding one in outfmt.c, otherwise
         * the test will fail */
        max_segments = 3 * 32,

        sizeof_cmd = 12,
        sizeof_arg = 2,
        accepted_args = max_segments - 2,
    };

    struct test_setup ts;
    char input[sizeof_cmd + sizeof_arg * (accepted_args + 1)];

    strcpy(input, "printf \\%s\\ ");
    for (int i = sizeof_cmd; i < sizeof(input); i += 2) {
        input[i] = ' ';
        input[i + 1] = '0' + (i % 10);
    }

    enum ofmt_mode mode = ofmt_mode_exec;

    EXPECT(setup(&ts, mode) == 0);

    EXPECT(ofmt_compile(ts.ofmt,
                        mode,
                        input,
                        sizeof_cmd + sizeof_arg * (accepted_args + 1)) == -1);
    EXPECT(ofmt_get_error(ts.ofmt)->reason == ofmt_fail_too_many_args);

    EXPECT(ofmt_compile(
               ts.ofmt, mode, input, sizeof_cmd + sizeof_arg * accepted_args) ==
           0);
    EXPECT(run(&ts) != -1);

    enum {
        start = sizeof_cmd + 1,
        length = sizeof_arg * accepted_args,
    };
    EXPECT(pipe_read_compare(&ts, input + start, length) != -1);

    teardown(&ts);
    return 0;
}

/* This test exercies the boundary checking for the maximum evaluation
 * buffer ('max_eval_buflen'). */
static int test_limits_subprocess_2(struct util_ctx *ctx, intptr_t opaque)
{
    enum ofmt_mode mode = ofmt_mode_exec;

    /* the value of this constant must match the value of
     * the corresponding one in outfmt.c, otherwise the test
     * will fail */
    enum {
        max_eval_buflen = 1024,
    };

    char buffer[max_eval_buflen];
    struct test_setup ts;

    for (int i = 0; i < sizeof(buffer) - 1; ++i) {
        int j = i % 16;
        if (j < 10)
            buffer[i] = '0' + j;
        else
            buffer[i] = 'A' + j;
    }
    buffer[max_eval_buflen - 1] = 0;

    /* first setup, then override some resolved values */
    EXPECT(setup(&ts, mode) == 0);
    ts.resolved_t = buffer; /* resolve(%t) -> buffer */
    ts.resolved_a = "X";    /* resolve(%a) -> 1-byte string */

    OFMT_COMPILE_OK(ts.ofmt, mode, "printf \\%s %t");
    EXPECT(run(&ts) == 0);
    EXPECT(pipe_read_compare(&ts, ts.resolved_t, strlen(ts.resolved_t)) != -1);

    OFMT_COMPILE_OK(ts.ofmt, mode, "printf \\%s %t%a");
    EXPECT(evaluate(&ts) == -1);
    EXPECT(ofmt_get_error(ts.ofmt)->reason == ofmt_fail_too_long_evaluation);

    teardown(&ts);
    return 0;
}

static int test_pipe_resolver(struct util_ctx *ctx, intptr_t opaque)
{
    struct test_setup ts;
    const struct ofmt_err *e;
    enum ofmt_mode mode = ofmt_mode_pipe;

    EXPECT(setup(&ts, mode) == 0);
    EXPECT(ofmt_set_pipe_resolver(ts.ofmt, resolve_pipe) == 0);

    ts.resolved_pipe = "oprasta\neterasta\n";
    OFMT_COMPILE_OK(ts.ofmt, mode, "./aux_test_launcher -cope");
    EXPECT_OR(evaluate(&ts) != 0, ofmt_print_error(ts.ofmt));
    e = ofmt_get_error(ts.ofmt);
    EXPECT(e->reason == ofmt_fail_subproc);
    EXPECT(WIFEXITED(e->subproc.exit_status));
    EXPECT(WEXITSTATUS(e->subproc.exit_status) == 4);

    teardown(&ts);
    return 0;
}

const struct test *list_test(void)
{
    static const struct test_params print_cases[] = {
        T("New post, title: %t\\n", "New post, title: RESOLVED TITLE\n"),
        T("Title: %t\\\\Author: %a\\n",
          "Title: RESOLVED TITLE\\Author: RESOLVED AUTHOR\n"),
        T("No placeholder should obviously work",
          "No placeholder should obviously work"),
        T("%x", "RESOLVED X"),
        T("%xn", "RESOLVED XN"),
        T("%x\\n", "RESOLVED X\n"),
        T("%x\\:n", "RESOLVED Xn")};

    static const struct test_params subprocess_cases[] = {
        T("printf %t", "RESOLVED TITLE"),
        T("printf %t\n", "RESOLVED TITLE"),
        T("printf %t\\n", "RESOLVED TITLE\n"),
        T("printf [%t%a]\\n", "[RESOLVED TITLERESOLVED AUTHOR]\n"),
        T("printf [%t][%a]\\n", "[RESOLVED TITLE][RESOLVED AUTHOR]\n"),
    };

    static struct test tests[] = {
        TEST(1, test_reject_unresolved, ofmt_mode_print),
        TEST(1, test_reject_unresolved, ofmt_mode_exec),
        TEST(1, test_reject_unresolved, ofmt_mode_pipe),
        TEST(1, test_mode_print, (intptr_t)&print_cases[0]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[1]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[2]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[3]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[4]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[5]),
        TEST(1, test_mode_print, (intptr_t)&print_cases[6]),
        TEST(1, test_mode_subprocess, (intptr_t)&subprocess_cases[0]),
        TEST(1, test_mode_subprocess, (intptr_t)&subprocess_cases[1]),
        TEST(1, test_mode_subprocess, (intptr_t)&subprocess_cases[2]),
        TEST(1, test_mode_subprocess, (intptr_t)&subprocess_cases[3]),
        TEST(1, test_mode_subprocess, (intptr_t)&subprocess_cases[4]),
        TEST(1, test_limits_subprocess_1, NULL),
        TEST(1, test_limits_subprocess_2, NULL),
        TEST(1, test_pipe_resolver, NULL),
        END_TESTS};
    return tests;
}
