#include "unittest.h"
#include "util.h"

#include <err.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#include "itemset.h"
#include "persist_dir.h"
#include "persist_file.h"

enum {
    max_items = 4096, /* matching the constant in persist_file.c */
};

static struct persist_dir *temp_persist_dir(struct util_ctx *ctx)
{
    int tempdir;
    struct persist_dir *pd;

    tempdir = util_get_tempdir(ctx);
    if (tempdir == -1)
        return NULL;

    pd = persist_dir_new(tempdir);
    if (!pd)
        close(tempdir);
    return pd;
}

static int write_persist(struct persist_dir *pd,
                         const struct iovec *iov,
                         int iovlen,
                         const char *fname)
{
    int fd, e;

    fd = openat(
        persist_dir_fd(pd), fname, O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU);

    if (fd == -1) {
        warn("openat %d test_file_1", persist_dir_fd(pd));
        return -1;
    }

    e = writev(fd, iov, iovlen) == -1 ? -1 : 0;
    if (fd != -1 && close(fd))
        warn("close");
    return e;
}

static int items_are(struct persist_file *pf, unsigned n, ...)
{
    va_list ap;
    int e = -1;
    const struct itemset *items;

    va_start(ap, n);

    items = persist_file_get_items(pf);
    EXPECT_OR_EXIT(items);

    EXPECT_OR_EXIT(itemset_get_size(items) == n);
    for (unsigned i = 0; i < n; i++) {
        const char *expected = va_arg(ap, const char *);
        const uint8_t *data = (const uint8_t *)expected;
        uint16_t data_len = strlen(expected);

        EXPECT_OR_EXIT(itemset_exists(items, data, data_len));
    }

    e = 0;
exit:
    va_end(ap);
    return e;
}

static int test_persist_items(struct util_ctx *ctx, intptr_t opaque)
{
    struct itemset *items;
    int e = -1;
    int n_items = 111;
    const void *aux = NULL;
    int *vals = reallocarray(NULL, n_items, sizeof(*vals));

    EXPECT(vals);

    items = itemset_new();
    EXPECT(items);

    for (int i = 0; i < n_items; i++) {
        vals[i] = i;
        e = itemset_add(items, (const uint8_t *)&vals[i], sizeof(*vals));
        EXPECT_OR_EXIT(e == itemset_eok);
    }

    for (int i = 0; i < n_items; i++) {
        const uint8_t *val_ptr;
        uint16_t data_len;

        itemset_iter(items, &aux, &val_ptr, &data_len);

        EXPECT_OR_EXIT(data_len == sizeof(*vals));
        EXPECT_OR_EXIT(memcmp(val_ptr, &vals[i], sizeof(*vals)) == 0);
    }

    e = 0;
exit:
    itemset_del(items);
    free(vals);
    return e;
}

static int test_file_status_detection(struct util_ctx *ctx, intptr_t opaque)
{
    struct persist_dir *pd;
    struct persist_file *pf;
    int e = -1;
    const char *fname = "testfile";

    /* this data type must kept in sync with the corresponding type in
     * persist_file.c */
    const size_t N = sizeof(short);

    /* This structure is initially configured to be a working persist
     * file, with two items stored ("hello" and "world"). */
    unsigned char magic_revision = 1;
    unsigned short items_count = 2;
    const struct iovec iovec[] = {
        {.iov_len = 8, .iov_base = "crossbow"},
        {.iov_len = 1, .iov_base = &magic_revision},
        {.iov_len = N, .iov_base = &(unsigned short){0}},
        {.iov_len = N, .iov_base = &items_count},
        {.iov_len = N, .iov_base = &(unsigned short){5}},
        {.iov_len = 5, .iov_base = "hello"},
        {.iov_len = N, .iov_base = &(unsigned short){5}},
        {.iov_len = 5, .iov_base = "world"},
    };

    const unsigned whole = sizeof(iovec) / sizeof(*iovec);

    pd = temp_persist_dir(ctx);
    EXPECT(pd);
    pf = persist_file_new(pd, fname);
    EXPECT_OR_EXIT(pf);

    EXPECT_OR_EXIT(write_persist(pd, iovec, whole, fname) == 0);
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_regular);
    EXPECT_OR_EXIT(items_are(pf, 2, "hello", "world") == 0);

    /* Recognized but wrong version. */
    magic_revision = 2;
    EXPECT_OR_EXIT(write_persist(pd, iovec, 2, fname) == 0);
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_incompat);
    magic_revision = 1;

    /* Good version, file too short, detected as corrupt. */
    for (int i = 2; i < whole; ++i) {
        EXPECT_OR_EXIT(write_persist(pd, iovec, i, fname) == 0);
        EXPECT_OR_EXIT(persist_file_load(pf) == 0);
        EXPECT_OR_EXIT(persist_file_status(pf) == pfs_corrupt);
    }

    /* Good version, but items count does not reflect the effective number
     * of items: the file is corrupt */
    items_count = 3;
    EXPECT_OR_EXIT(write_persist(pd, iovec, whole, fname) == 0);
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_corrupt);
    items_count = 2;

    /* Good version, although items count is smaller than what effectively
     * serialized: the file is loaded, but less items are detected */
    items_count = 1;
    EXPECT_OR_EXIT(write_persist(pd, iovec, whole, fname) == 0);
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_regular);
    EXPECT_OR_EXIT(items_are(pf, 1, "hello") == 0);
    items_count = 2;

    e = 0;
exit:
    persist_dir_del(pd);
    persist_file_del(pf);
    return e;
}

static int populate(struct persist_dir *pd,
                    const char *name,
                    unsigned n_items,
                    ...)
{
    struct persist_file *pf;
    struct itemset *items;
    int e = -1;
    va_list ap;

    va_start(ap, n_items);

    pf = persist_file_new(pd, name);
    EXPECT(pf);

    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_unknown);

    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_noent);
    EXPECT_OR_EXIT(persist_file_get_items(pf) == NULL);

    items = itemset_new();

    for (unsigned i = 0; i < n_items; ++i) {
        const char *item;
        itemset_error e;

        item = va_arg(ap, const char *);
        e = itemset_add(items, (const uint8_t *)item, strlen(item));
        EXPECT_OR_EXIT(e == itemset_eok);
    }

    EXPECT_OR_EXIT(persist_file_swap_items(pf, items) == NULL);
    persist_file_set_incrid(pf, n_items);
    EXPECT_OR_EXIT(persist_file_write(pf) == 0);

    e = 0;
exit:
    va_end(ap);
    persist_file_del(pf);
    return e;
}

static int test_workflow(struct util_ctx *ctx, intptr_t opaque)
{
    struct persist_dir *pd;
    struct persist_file *pf = NULL;
    struct itemset *items = NULL;
    int e = -1;
    const char *fname = "feed1";

    pd = temp_persist_dir(ctx);
    EXPECT(pd);

    /* Pre-populate a file. */
    EXPECT_OR_EXIT(populate(pd, fname, 1, "one") == 0);

    /* Get a file, and load it. */
    pf = persist_file_new(pd, fname);
    EXPECT_OR_EXIT(pf);
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_regular);
    EXPECT_OR_EXIT(items_are(pf, 1, "one") == 0);
    EXPECT_OR_EXIT(persist_file_get_incrid(pf) == 1);

    /* Add one item and write. */
    items = itemset_new();
    EXPECT_OR_EXIT(itemset_add(items, (const uint8_t *)"two", 3) ==
                   itemset_eok);
    EXPECT_OR_EXIT(itemset_add(items, (const uint8_t *)"three", 5) ==
                   itemset_eok);
    persist_file_set_incrid(pf, 3);
    items = persist_file_swap_items(pf, items);
    EXPECT_OR_EXIT(persist_file_write(pf) == 0);

    /* Reload: find the new item. */
    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_regular);
    EXPECT_OR_EXIT(items_are(pf, 2, "two", "three") == 0);
    EXPECT_OR_EXIT(persist_file_get_incrid(pf) == 3);

    e = 0;
exit:
    itemset_del(items);
    persist_file_del(pf);
    persist_dir_del(pd);
    return e;
}

static int test_max_items(struct util_ctx *ctx, intptr_t opaque)
{
    int e = -1;
    struct itemset *items;
    uint16_t *vals = NULL;
    enum {
        max_size = 10000,
    };

    items = itemset_new();
    EXPECT_OR_EXIT(items);

    vals = calloc(1 + max_size, sizeof(uint16_t));
    for (uint16_t i = 0; i <= max_size; i++)
        vals[i] = i;

    EXPECT_OR_EXIT(vals);

    for (int i = 0; i < max_size; i++)
        EXPECT_OR_EXIT(itemset_add(items,
                                   (const uint8_t *)&vals[i],
                                   sizeof(uint16_t)) == itemset_eok);
    EXPECT_OR_EXIT(itemset_add(items,
                               (const uint8_t *)&vals[max_size],
                               sizeof(uint16_t)) == itemset_emax);

    e = 0;
exit:
    itemset_del(items);
    free(vals);
    return e;
}

static int test_arbitrary_item_name(struct util_ctx *ctx, intptr_t opaque)
{
    uint8_t buffer[256];
    struct persist_dir *pd;
    struct persist_file *pf = NULL;
    struct itemset *items = NULL;
    int e = -1;
    const char *fname = "feed1";

    pd = temp_persist_dir(ctx);
    EXPECT_OR_EXIT(pd);
    pf = persist_file_new(pd, fname);
    EXPECT_OR_EXIT(pf);

    /* Any byte, arbitrary order.  The rationale is: every feed might define an
     * arbitrary item identifier, and we shouldn't be affected by weird choices
     * (e.g. nil-character). */
    for (int i = 0; i < sizeof(buffer); ++i)
        buffer[i] = (127 + i) % 256;

    items = itemset_new();
    EXPECT_OR_EXIT(items);

    EXPECT_OR_EXIT(itemset_add(items, buffer, sizeof(buffer)) == 0);
    EXPECT_OR_EXIT(itemset_get_size(items) == 1);

    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_noent);
    EXPECT_OR_EXIT(persist_file_get_items(pf) == NULL);

    items = persist_file_swap_items(pf, items);
    EXPECT_OR_EXIT(!items);
    EXPECT_OR_EXIT(persist_file_write(pf) == 0);

    EXPECT_OR_EXIT(persist_file_load(pf) == 0);
    EXPECT_OR_EXIT(persist_file_status(pf) == pfs_regular);

    items = persist_file_swap_items(pf, items);
    EXPECT_OR_EXIT(!!items);
    EXPECT_OR_EXIT(itemset_get_size(items) == 1);
    EXPECT_OR_EXIT(itemset_exists(items, buffer, sizeof(buffer)));

    e = 0;
exit:
    itemset_del(items);
    persist_file_del(pf);
    persist_dir_del(pd);
    return e;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_persist_items, NULL),
                                  TEST(1, test_file_status_detection, NULL),
                                  TEST(1, test_workflow, NULL),
                                  TEST(1, test_max_items, NULL),
                                  TEST(1, test_arbitrary_item_name, NULL),
                                  END_TESTS};
    return tests;
}
