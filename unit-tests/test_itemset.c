#include "itemset.h"

#include <stdlib.h>

#include "unittest.h"
#include "util.h"

static int test_duplication(struct util_ctx *ctx, intptr_t opaque)
{
    struct itemset *itemset = NULL;
    int e = -1;
    void *allocated_key = NULL;

    itemset = itemset_new();
    EXPECT_OR_EXIT(itemset);

    allocated_key = strdup("zeppola");

    EXPECT_OR_EXIT(itemset_add(itemset,
                               (const uint8_t *)"zeppola",
                               strlen("zeppola")) == itemset_eok);

    EXPECT_OR_EXIT(itemset_add(itemset, allocated_key, strlen(allocated_key)) ==
                   itemset_edup);

    const void *aux = NULL;
    const uint8_t *data;
    uint16_t data_len;
    while (itemset_iter(itemset, &aux, &data, &data_len)) {
        EXPECT_OR_EXIT(data_len == strlen("zeppola"));
        EXPECT_OR_EXIT(memcmp(data, "zeppola", data_len) == 0);
    }

    e = 0;
exit:
    free(allocated_key);
    itemset_del(itemset);
    return e;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_duplication, NULL), END_TESTS};
    return tests;
}
