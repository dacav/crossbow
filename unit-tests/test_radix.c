#include <stdint.h>

#include "radix.h"

#include "unittest.h"
#include "util.h"

#if 1
#include <err.h>
#define DEBUG(...) warnx(__VA_ARGS__);
#endif

static int test_create(struct util_ctx *ctx, intptr_t opaque)
{
    struct radix *r;
    const char key[] = "senap";
    size_t keylen = sizeof key - 1;
    void *item = NULL;

    EXPECT(r = radix_new());
    EXPECT(radix_register(r, key, (void *)9001) == 0);

    EXPECT(radix_lookup(r, key, keylen, &item) == keylen);
    EXPECT(item == (void *)9001);

    radix_del(r);
    return 0;
}

static int test_multi(struct util_ctx *ctx, intptr_t opaque)
{
    static const char *sequence[] = {
        "c",  /* new in root */
        "cc", /* new in existing but empty level 1 */
        "ca", /* new in existing level 1, beforre existing sibling */
        "d",  /* new in root, after existing sibling */
        "eb", /* new in root, new in level 1 */
        "cb", /* new in existing level 1, between existing siblings */
        "a",  /* new in root, before existing sibling */
        "ac", /* new in existing level 1, after existing sibling */
        "ba", /* new in non-existing level 1 */
        "b",  /* base of existing level 1 */
        NULL};

    static const struct {
        const char *lookup;
        int expected_idx;
    } lookups[] = {
        {"conz", 0},
        {"car", 2},
        {"ca", 2},
        {"ccl", 1},
        {"be", 9},
        {"cba", 5},
        {NULL, 0},
    };

    struct radix *r;

    EXPECT(r = radix_new());

    for (intptr_t i = 0; sequence[i]; ++i)
        EXPECT(radix_register(r, sequence[i], (void *)sequence[i]) == 0);

    for (intptr_t i = 0; sequence[i]; ++i) {
        void *item = NULL;

        EXPECT(radix_lookup(r, sequence[i], strlen(sequence[i]), &item) != -1);
        EXPECT(item && strcmp(item, sequence[i]) == 0);
    }

    for (int i = 0; lookups[i].lookup; ++i) {
        void *item = NULL;
        int depth;
        int exp_idx;

        depth = radix_lookup(
            r, lookups[i].lookup, strlen(lookups[i].lookup), &item);
        EXPECT(item != NULL);

        exp_idx = lookups[i].expected_idx;
        EXPECT(item && strcmp(sequence[exp_idx], item) == 0);
        EXPECT(depth == strlen(sequence[exp_idx]));
    }

    radix_del(r);
    return 0;
}

static int test_corner(struct util_ctx *ctx, intptr_t opaque)
{
    struct radix *r;

    EXPECT(r = radix_new());
    EXPECT(radix_register(r, "", NULL) == -1 && errno == EINVAL);
    EXPECT(radix_register(r, "", NULL) == -1 && errno == EINVAL);
    radix_del(r);

    return 0;
}

const struct test *list_test(void)
{
    static struct test tests[] = {TEST(1, test_create, NULL),
                                  TEST(1, test_corner, NULL),
                                  TEST(1, test_multi, NULL),
                                  END_TESTS};
    return tests;
}
