#include "util.h"

#include "test_counters.h"

#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

#define TEMPDIR_SUFFIX "-XXXXXX"

struct util_ctx {
    struct {
        char *path;
        int fd;
    } tempdir;

    const char *test_name;
};

#define EMPTY                                                                  \
    (struct util_ctx)                                                          \
    {                                                                          \
        .tempdir = {.path = NULL, .fd = -1}, .test_name = NULL,                \
    }

static struct util_ctx *util_ctx_new(void)
{
    struct util_ctx *ctx;

    ctx = malloc(sizeof(*ctx));
    if (!ctx)
        return NULL;

    *ctx = EMPTY;
    return ctx;
}

static void cleardir(int dirfd)
{
    DIR *dir;
    struct dirent *dirent;

    dir = fdopendir(dirfd);
    if (!dir) {
        warn("fdopendir %d", dirfd);
        return;
    }

    while (errno = 0, dirent = readdir(dir), dirent != NULL) {
        int fd, unlinkat_flags = 0;

        if (strcmp(dirent->d_name, ".") == 0)
            continue;
        if (strcmp(dirent->d_name, "..") == 0)
            continue;

        fd = openat(dirfd, dirent->d_name, O_DIRECTORY);
        if (fd != -1) {
            cleardir(fd);
            unlinkat_flags = AT_REMOVEDIR;
        } else if (errno != ENOTDIR) {
            warn("openat %d %s (skip)", dirfd, dirent->d_name);
            continue;
        }

        if (unlinkat(dirfd, dirent->d_name, unlinkat_flags) == -1)
            warn("unlink %d %s %d (skip)",
                 dirfd,
                 dirent->d_name,
                 unlinkat_flags);
    }

    if (errno)
        warn("readdir");

    closedir(dir);
}

static void rmtempdir(struct util_ctx *ctx)
{
    if (ctx->tempdir.fd != -1) {
        cleardir(ctx->tempdir.fd);
        ctx->tempdir.fd = -1;
    }

    if (ctx->tempdir.path) {
        if (rmdir(ctx->tempdir.path) == -1)
            warn("rmdir %s", ctx->tempdir.path);
        free(ctx->tempdir.path);
        ctx->tempdir.path = NULL;
    }
}

static int mktempdir(struct util_ctx *ctx)
{
    char *dirname;
    size_t n;
    int dirfd;

    if (ctx->tempdir.fd != -1)
        return 0;

    n = strlen(ctx->test_name) + sizeof(TEMPDIR_SUFFIX);
    dirname = malloc(n);
    if (!dirname) {
        warn("malloc");
        goto fail;
    }

    snprintf(dirname, n, "%s" TEMPDIR_SUFFIX, ctx->test_name);
    if (mkdtemp(dirname) == NULL) {
        warn("mkdtemp %s", dirname);
        goto fail;
    }

    dirfd = open(dirname, O_DIRECTORY);
    if (dirfd == -1) {
        warn("open %s, O_DIRECTORY", dirname);
        goto fail;
    }

    ctx->tempdir.path = dirname;
    ctx->tempdir.fd = dirfd;
    return 0;

fail:
    free(dirname);
    return -1;
}

static void util_ctx_del(struct util_ctx *ctx)
{
    if (ctx == NULL)
        return;

    rmtempdir(ctx);
    free(ctx);
}

struct util_ctx *util_test_setup(const char *test_name)
{
    struct util_ctx *ctx;

    ctx = util_ctx_new();
    if (!ctx)
        return NULL;

    ctx->test_name = test_name;
    return ctx;
}

void util_test_teardown(struct util_ctx *ctx)
{
    util_ctx_del(ctx);
    test_reset_counters();
}

int util_get_tempdir(struct util_ctx *ctx)
{
    int d;

    if (mktempdir(ctx) == -1)
        return -1;

    /* basically like dup.  We need to keep our own file descriptor in
     * order to properly clean up, later. */
    d = openat(ctx->tempdir.fd, ".", O_DIRECTORY);
    if (d == -1)
        warn("openat(%s . O_DIRECTORY)", ctx->tempdir.path);
    return d;
}
