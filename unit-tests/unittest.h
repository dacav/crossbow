#pragma once

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifdef NDEBUG
#undef NDEBUG
#include <assert.h>
#define NDEBUG
#else
#include <assert.h>
#endif

#include "util.h"

#define FAIL(reason)                                                           \
    do {                                                                       \
        fprintf(stderr,                                                        \
                __FILE__ ":%d: %s errno=%d (%s)\n",                            \
                __LINE__,                                                      \
                (reason),                                                      \
                errno,                                                         \
                strerror(errno));                                              \
        return -1;                                                             \
    } while (0)

#define EXPECT_OR(condition, otherwise)                                        \
    do {                                                                       \
        errno = 0;                                                             \
        if (!(condition)) {                                                    \
            fprintf(stderr,                                                    \
                    __FILE__ ":%d: error: ![%.40s%s]%s%s\n",                   \
                    __LINE__,                                                  \
                    #condition,                                                \
                    sizeof(#condition) > 40 ? "..." : "",                      \
                    errno ? " errno=" : "",                                    \
                    errno ? strerror(errno) : "");                             \
            {                                                                  \
                otherwise;                                                     \
            }                                                                  \
            return -1;                                                         \
        }                                                                      \
    } while (0)

#define EXPECT(condition) EXPECT_OR(condition, (void)0);

#define EXPECT_OR_EXIT(condition) EXPECT_OR(condition, goto exit);

typedef int (*test_callback)(struct util_ctx *, intptr_t opaque);

struct test {
    int enabled;
    const char *name;
    test_callback callback;
    intptr_t opaque;
    const char *opaque_repr;
};

const struct test *list_test();

#define TEST(enable, callback, opaque)                                         \
    {                                                                          \
        (enable), #callback, (callback), (intptr_t)(opaque), #opaque           \
    }
#define END_TESTS                                                              \
    {                                                                          \
        0, NULL, NULL, 0                                                       \
    }
