#pragma once

#include <sys/types.h>

struct util_ctx;

struct util_ctx *util_test_setup(const char *test_name);
void util_test_teardown(struct util_ctx *);

int util_get_tempdir(struct util_ctx *);
