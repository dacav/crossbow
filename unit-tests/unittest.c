#include "unittest.h"
#include "util.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

enum {
    WIDTH = 60,
};

int main(int argc, char **argv)
{
    int exit_status;
    const struct test *test;

    exit_status = EXIT_SUCCESS;
    for (test = list_test(); test->name; ++test) {
        struct util_ctx *ctx;
        int result;
        char test_repr[WIDTH];

        if (!test->enabled) {
            fprintf(stderr, "%-*s [SKIP]\n", WIDTH, test->name);
            continue;
        }

        ctx = util_test_setup(test->name);
        if (!ctx)
            err(1, "util_test_setup");

        snprintf(test_repr,
                 sizeof(test_repr),
                 test->opaque ? "%s(%s)" : "%s",
                 test->name,
                 test->opaque_repr);

        result = test->callback(ctx, test->opaque);
        if (result == 0) {
            fprintf(stderr, "%-*s [ OK ]\n", WIDTH, test_repr);
        } else {
            fprintf(stderr, "%-*s [FAIL, %d]\n", WIDTH, test_repr, result);
            exit_status = EXIT_FAILURE;
        }
        util_test_teardown(ctx);
    }

    return exit_status;
}
